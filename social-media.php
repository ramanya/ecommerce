<!-- facebook plugin script -->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v13.0" nonce="2qdPjdQt"></script>


<!-- Start Most Popular -->
<div class="product-area most-popular section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h2>Trending Items</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel popular-slider">

    <?php
    require_once'admin/library/config.php';

    
    $root ="SELECT * FROM product  ORDER BY product_createdon DESC LIMIT 5";
    $res1 = pg_query($conn, $root);

    if ($res1) {
            // output data of each row
        $num = 1;
        while($row1 = pg_fetch_assoc($res1)) {
            (string) $id = $row1['product_code'];
            (string) $product_code = $row1['product_code'];
            (string) $product_photo = $row1['product_photo'];
            (string) $product_description = $row1['product_description'];
            (string) $product_name = $row1['product_name'];
        
            if (!empty($product_code)){
                ?>
					<!-- Start Single Product -->
					  <?php
                                    if (!empty($product_photo)){
                                        ?>
                                      	<div class="single-product">
						<div class="product-img">
							<a href="shop/">
								<img class="default-img" src="admin/products/images/<?php echo $product_photo; ?>" alt="#">
								<img class="hover-img" src="admin/products/images/<?php echo $product_photo; ?>" alt="#">
								<span class="out-of-stock">Hot</span>
							</a>
							
							</div>
							<div class="product-content">
								<h3><a href="shop/"><?php echo $product_name ?></a></h3>
						
							</div>
						</div>
                                    <?php } ?>
				
						<!-- End Single Product -->
					<?php }
					}
					} ?>
		      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Most Popular Area -->


    <!-- Slider Area -->
    <section class="section free-version-banner">
    	<div class="container">
    		<div class="section18">
    			<div class="row">
    				<div class="col-12">
    					<div class="section-title">
    						<h2>About Christ Kenya Investments Ltd</h2>
    					</div>
    				</div>
    				<div class="col-md-8 col-lg-8 wow fadeInUp">
    					<div class="textcont">
    						<h3>Who we are</h3>
    						<p>A company that empowers a girl child in acquiring vocational education. We pride ourselves in imparting 21st century life skills. CKIL has a production & sales arm where value addition on cow horn and cleaning detergents are produced and introduced to the world.</p>
    						<h4>What we do</h4>
    						<p>We have a HYBRID model of profit and social enterprise. We provide debt free access to skills and capital for the entrepreneurs, under the poverty line, to start their own businesses through joint contribution.</p>
    						<br>
    						<p>While on the program, the likeminded entrepreneurs work together creating our Collection of banana fibre, baskets, bags and Ankole cow Horn wares ranging from décor, kitchen and bathroom, jewelry among others, sold locally, Nigeria and the UK	.</p>
    					</div>  
    				</div>
    				<div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s">
    					<div class="section-18-img">
    						<img src="images/ourstory.jpeg"  class="img-responsive" alt=""/>
    						 <div class="default-social">
          <h4 class="share-now">visit our pages:</h4>
          <ul>
            <li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
            <li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>
            
          </ul>
        </div>
    					</div>
    				</div>
    			</div>
    			
    		</div>  
    	</div>
    </section>
    <!--/ End Slider Area -->

    <!-- Slider Area -->
    <section class="section free-version-banner">
    	<div class="container">
    		<div class="section18">
    			<div class="row">
    				<div class="col-12">
    					<div class="section-title">
    						<h2>Our story</h2>
    					</div>
    				</div>
    				<div class="col-md-8 col-lg-8 wow fadeInUp">
    					<div class="blog-detail">
    					<div class="content">
    						
    						<p>Hannah Mellon Kenyangi a CEO and founder launched CKIL in 2018 with a dream of
    							creating employment to the youth, women and girls majorly in areas of skilling. She is a
    							master’s degree holder in Marketing and management though her foundation is Education.
    							Her education did not come easily as a last born of many and an orphan. Hannah grew up
    							with her brother in the country side and joined Uganda’s capital Kampala in her youth and
    						fought her way through alone..</p>
    						<br>
    						
    						<p>“It was a struggle to find a job, later on house to call home in a highly competitive city”
    							This changed her to dream bigger for those struggling with education by providing
    						livelihood training skills.</p>
    						<br>

    						<p>This desire saw the launch of CKIL where value addition on Ankole cow horn was born,
    							sourcing and training artisans in different skills (paper jewelry, beading, kneading, and use
    						of recycled material within to save the environment.)</p>
    						<br>
    						<p>She is passionate about Ankole Horn and born products for re-known Indigenous Ankole Long Horned Cow is a beautiful friend in her life and culture. Hannah hopes that the cow’s legacy can stand the taste of time with the generations. These cattle played a great role in relationship strengthening amongst families and we hope we shall have a lasting relationship with you as we bring a unique and rare touch to your space.  </p>
    						<br>
    						<p>In 2021, several partners came on board to see this dream come true, creating
    							employment and earning descent income even when considered vulnerable. Today, Ckil 
    							is a growing business in Uganda that creates dignified work for
    							over 20 Artisans in their community – and they are not alone. We currently looking
    							forward to partnering more Artisan skilling Businesses in other cities and countries.
    							Together we are creating opportunity for more than 20 Artisans, impacting over 40 family
    						members.</p>


    					</div>  
    					</div>
    				</div>
    				<div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s">
    					<div class="section-18-img">
    						<img src="images/story.jpg"  class="img-responsive" alt=""/>
    						 <div class="default-social">
          <h4 class="share-now">visit our pages:</h4>
          <ul>
            <li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
            <li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>
            
          </ul>
        </div>
    					</div>
    						<div class="section-18-img">
    						<img src="images/story-b.jpg"  class="img-responsive" alt=""/>
    						 <div class="default-social">
          <h4 class="share-now">visit our pages:</h4>
          <ul>
            <li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
            <li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>
            
          </ul>
        </div>
    					</div>
    				</div>
    			</div>
    			
    		</div>  
    	</div>
    </section>
    <!--/ End Slider Area -->



    <!-- Start Midium Banner  -->
    <section class="midium-banner" >
    	<div class="container">
    		<div class="row" >

    			<div class="col-12">
    				<div class="section-title">
    					<h2>Social Media Feeds</h2>
    				</div>
    			</div>
    			
    			<!-- Single Banner  -->
    			<div class="col-lg-6 col-md-6 col-12" style="height:500px; overflow-y:scroll;">
    				 <a class="twitter-timeline" href="https://twitter.com/ckiluganda?ref_src=twsrc%5Etfw">Tweets by ckiluganda</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    			</div>
    			<!-- /End Single Banner  -->

       
    			<!-- Single Banner  -->
    			<div class="col-lg-6 col-md-6 col-12" style="height:500px; overflow-y:scroll;" >
    	
            <div class="embedsocial-hashtag" data-ref="3dc87a623a0326a1f3303ffe46a409e81eb13515"> <a class="feed-powered-by-es feed-powered-by-es-feed-new" href="https://embedsocial.com/social-media-aggregator/" target="_blank" title="Widget by EmbedSocial"> Widget by EmbedSocial<span>→</span> </a> </div> <script> (function(d, s, id) { var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/cdn/ht.js"; d.getElementsByTagName("head")[0].appendChild(js); }(document, "script", "EmbedSocialHashtagScript")); </script>
    			</div>


    			<!-- /End Single Banner  -->
    		</div>
    	</div>
    </section>
    <!-- End Midium Banner -->













