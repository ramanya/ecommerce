  <!-- Preloader -->
  <div class="preloader">
    <div class="preloader-inner">
      <div class="preloader-icon">
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- End Preloader -->


  <!-- Slider Area -->
  <section class="section free-version-banner">
    <div class="container">
      <div class="section18">
        <div class="row">
         <div class="col-12">
          <div class="section-title">
           <h2>Skilling</h2>
         </div>
       </div>
       <div class="col-md-8 col-lg-8 wow fadeInUp">
        <div class="textcont">
          <h3>Horn & Bone</h3>
          <p>Ankole Cow horn is one of the most sustainable and ethically sourced products that we carry. Our
            artisans collect the discarded Ankole horns and bones, then take them to our modest workshops/
            training stations where they heat the horns to transform them into beautiful bowls, servers, and
            jewelry components. Each horn is unique in color and texture, so you know that you are getting a
          one-of-a kind design where no two pieces are alike.</p>
          <h4>Art& Craft | Back cloth</h4>
          <p>We give young girls, women and youth access to affordable training opportunities to help in
            paving way towards economic justice and independence. There your order helps in making
            an impact and more power to train and reach more as they all strive to have a decent
          income.</p>



        </div>  
      </div>
      <div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s">
        <div class="section-18-img">
          <img src="../images/back.jfif"  class="img-responsive" alt=""/>
        </div>
      </div>
    </div>

  </div>  
</div>
</section>
<!--/ End Slider Area -->


<!-- Slider Area -->
<section class="section free-version-banner">
  <div class="container">
    <div class="section18">
      <div class="row">

        <div class="col-md-8 col-lg-8 wow fadeInUp">
          <div class="textcont">
            <h3>Social Media marketing</h3>
            <p>Being a third world country, Uganda still lags behind in the field Social media marketing which is the
              use of social media platforms and websites to promote a product or service. We tailor our modules
            depending on the client’s needs.</p>
            <h4>STEM Education</h4>
            <p>Uganda largely dominated with unemployed yet educated youth, we discovered that the theoretical
              education plays a role in increasing the unemployment numbers high as the demand is too small
            compared to supply of skilled labor.</p>
            <p>We partnered with African School of Innovations, science and technology to provide affordable
              weekend lessons to children from 8years into the field of STEM. STEM is an acronym for science,
              technology, engineering, and math. These four fields share an emphasis on innovation,
              problem-solving, and critical thinking. And together they make up a popular and fast-growing
            industry. Most STEM workers use computers and other technology in their day-to-day jobs.</p>

          </div>  
        </div>
        <div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s">
          <div class="section-18-img">
            <img src="../images/girl.jpeg"  class="img-responsive" alt=""/>
          </div>
        </div>
      </div>

    </div>  
  </div>
</section>
  <!--/ End Slider Area -->