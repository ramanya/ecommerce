  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> User Account Management </h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

                <?php
                        if(strlen($error) > 0){
                                echo '
                                        <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" 
                                        aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i> Alert!</h4>' .$error . '
                                        </div>';
                        }
                ?>



		<table id="cci" class="table table-bordered ">
              		<tr>
			<td>
			<?php
			   $tab_root = $_GET["tab_root"];
			   $tab_priv = $_GET["tab_priv"];
			   $tab_role = $_GET["tab_role"];
			   $tab_user = $_GET["tab_user"];
			?>
			<button class="tablink" onclick="openPage('root', this, 'red')" <?php if ($tab_root == '1'){ echo 'id="defaultOpen"';} ?>> 
			   <i class="fa fa-user"></i>  Root User </button>
			<button class="tablink" onclick="openPage('Home', this, '#04812F')" <?php if ($tab_priv == '1'){ echo 'id="defaultOpen"';} ?>>
			   <i class="fa fa-user-secret"></i> Privileges </button>
			<button class="tablink" onclick="openPage('News', this, '#04812F')" <?php if ($tab_role == '1'){ echo 'id="defaultOpen"';} ?>> 
			   <i class="fa fa-sun-o"></i> User Roles </button>
			<button class="tablink" onclick="openPage('Contact', this, '#04812F')" 
			<?php if ($tab_user == '1'){ echo 'id="defaultOpen"';}else{echo 'id="defaultOpen"';} ?>> <i class="fa fa-users"> </i> Users </button>
			<button class="tablink" onclick="openPage('About', this, '#04812F')"> System Logs </button>

                        <div id="root" class="tabcontent">
                           <h3>Root Users 
   	                      <button class='button button2' data-toggle='modal' data-target='#modal-root'>
                	         <i class='fa fa-plus'></i>Add Root User</button>
			   </h3>

              		   <table id="example1" class="table table-bordered table-striped">
 			           <thead>
                	      <tr>
               	         <th>Id</th>
                		 <th>Username</th>
                		 <th>Name </th>
                		 <th>Email </th>
                		 <th>Last Loggin </th>
				 <th align='center'> Action </th>
                	       </tr>
                	       </thead>
                	       <tbody>
  			       <?php	
			          $root = "SELECT * FROM company WHERE companyusertype = 'root'";
				  $res1 = pg_query($conn, $root);
				  if ($res1) {
    				     // output data of each row
				     $number = 1;
    				     while($row1 = pg_fetch_assoc($res1)) {
                                        (string) $id = $row1['companyurl'];
                                        (string) $trash = $row1['company_trash'];
                                        (string) $is_active = $row1['companyuserisactive'];
                                        (string) $companyurl = $row1['companyurl'];
       					echo "<tr class='text-sm'>";
       					echo "<td>" . $number . "</td>";

					$status = $row1['companyuserisactive'];
					$online = $row1['isonline'];
					if (($status == 'f') && ($online == 'f')){
                   			   echo " <td> 
						     <i class='fa fa-circle text-danger'></i> " . " " ."
						     <i class='fa fa-user-times'> </i> "."- " 
						     . $row1['companyaccountname'] . "</td>";
					}elseif (($status == 't') && ($online == 't')){
                                       	   echo " <td> 
						     <i class='fa fa-circle text-success'></i> " . " " ."
						     <i class='fa fa-user'> </i> " . "- " 
                                                     . $row1['companyaccountname'] . "</td>";
					}else{
                                           echo "<td> 
                                                    <i class='fa fa-circle text-danger'></i> " . " " ."
                                                    <i class='fa fa-user'> </i> " . "- "  
                                                    . $row1['companyaccountname'] . "</td>";
					}

					echo "<td>" . $row1['companyfullname'] . "</td>";
		               		echo "<td>" . $row1['companyemail'] . "</td>";
					if (!empty($row1['last_loggin'])){
					   echo "<td>" . $row1['last_loggin'] . "</td>";
					}else{
					   echo "<td> <font color='red'> Never logged in </font> </td>";
					}

					echo '<td>';
                                        echo ' <a class="button button" href="#edit'.$companyurl.'" data-toggle="modal">
                                               <i class="fa fa-edit"></i> Edit</a>';

					if ($is_active == 't'){
					   echo ' <button class="button button2" title="Disable account!" 
					 	  onclick="window.location.href ='."'".'disable.php?error='.$id."';".'"> 
						  <i class="fa fa-user"> </i> Disable </button> ';
					}else{
                                           echo ' <button class="button button4" title="Enable account!" 
                                                  onclick="window.location.href ='."'".'enable.php?error='.$id."';".'"> 
                                                  <i class="fa fa-user-times"> </i> Enable </button> ';
				       }

                                      if ($trash == 'f'){
                                         echo ' <button class="button button5" title="Trash record!" 
                                                onclick="window.location.href ='."'".'root_trash.php?error='.$id."';".'"> 
                                                <i class="fa fa-trash"> </i> Trash </button> ';
                                      }else{
                                ?>
                                        <a href="root_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }

                                      include('edit_root.php');
                                      echo '</td>';
				      $number++;
                                      echo '</tr>';
                                    }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> There user types configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  	       }
                	      ?>
                	      </tbody>
                	      <tfoot>
                	      <tr>
              		      <tr>
                                 <th>Id</th>
                        	 <th>Username</th>
                		 <th>Name </th>
                        	 <th>Email</th>
                        	 <th>Last Loggin</th>
                        	 <th align='center'> Action </th>
			      </tr>
                	      </tr>
                	      </tfoot>
              		   </table>
                        </div>

			<div id="Home" class="tabcontent">
                           <h3>User Privileges 
	                      <button class='button button2' data-toggle='modal' data-target='#modal-privilege'>
                	         <i class='fa fa-user-secret'></i>Add Privilege</button>
			   </h3>
              		   <table id="privilege" class="table table-bordered table-striped">
                	      <thead>
              		         <tr>
                		    <th>Id</th>
                		    <th>User privilege name</th>
                		    <th>Created By</th>
                		    <th>Created On</th>
				    <th align='center'> Action </th>
                		 </tr>
                		 </thead>
                		 <tbody>
  				 <?php	
				    $pr = "SELECT * FROM access_rights ORDER BY right_name ASC";
				    $rs = pg_query($conn, $pr);
				    if ($rs) {
    				       // output data of each row
				       $number = 1;
    				       	while($rw = pg_fetch_assoc($rs)) {
                                           (string) $id = $rw['access_url'];
                                           (string) $access_url = $rw['access_url'];
                                           (string) $trash = $rw['access_trash'];
       					   echo "<tr class='text-sm'>";
                                           echo "<td>" . $number . "</td>";
					   echo "<td>" . $rw['right_name'] . "</td>";
					   echo "<td>" . $rw['access_created_by'] . "</td>";
					   echo "<td>" . $rw['access_created_on'] . "</td>";
					   echo "<td width='25%'>";
                                           echo ' <a class="button button" href="#edit'.$access_url.'" data-toggle="modal">
                                                  <i class="fa fa-edit"></i> Edit</a>';

					   if ($is_active == 't'){
                                              echo ' <button class="button button2" title="Disable account!" 
					             onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						     <i class="fa fa-user"> </i> Disable </button> ';
					   }else{
                                              echo ' <button class="button button4" title="Enable account!" 
                                                     onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                     <i class="fa fa-user-times"> </i> Enable </button> ';
					  }

                                          if ($trash == 'f'){
                                             echo ' <button class="button button5" title="Trash record!" 
                                                    onclick="window.location.href ='."'".'priv_trash.php?error='.$id."';".'"> 
                                                    <i class="fa fa-trash"> </i> Trash </button> ';
                                          }else{
                                ?>
                                        <a href="priv_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }
                                         include('edit_privilege.php');
                                         echo '</td>';

                                         $number++;
                                         echo '</tr>';
                                      }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> There user privilege configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  		}
                		?>
                		</tbody>
                		<tfoot>
                		<tr>
                		<tr>
                        	   <th>Id</th>
                		   <th>User privilege name</th>
                                   <th>Created By</th>
                                   <th>Created On</th>
                        	   <th align='center'> Action </th>
                                </tr>
                	        </tr>
                	        </tfoot>
              		     </table>
			</div>


			<div id="News" class="tabcontent">

                           <h3>User Roles / Profile 
			      <button class='button button2' data-toggle='modal' data-target='#modal-role'>
                	         <i class='fa fa-user-secret'></i>Add User Role</button>
			   </h3>

			   <table id="role" class="table table-bordered table-striped">
                	      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Role name</th>
                		 <th>Privileges</th>
                		 <th>Created By</th>
				 <th align='center'> Action </th>
                	      </tr>
                	      </thead>
                	      <tbody>

			      <?php	
			         $roles = "SELECT * FROM user_roles ORDER BY role_name ASC";
				 $result_role = pg_query($conn, $roles);
				 if ($result_role) {
    				   // output data of each row
				   $number = 1;
    				   while($row_role = pg_fetch_assoc($result_role)) {
                                      (string) $id = $row_role['role_url_link'];
                                      (string) $role_url_link = $row_role['role_url_link'];
                                      (string) $trash = $row_role['role_trash'];

                                      echo "<tr class='text-sm'>";
                                      echo "<td>" . $number . "</td>";
                                      echo "<td>" . $row_role['role_name'] . "</td>";
				      echo "<td>" . $row_role['role_access_rights'] . "</td>";
				      echo "<td>" . $row_role['role_createdby'] . "</td>";

				      echo "<td width='27%'>";
                                      echo  ' <a class="button button" href="#edit'.$role_url_link.'" data-toggle="modal">
                                              <i class="fa fa-edit"></i> Edit</a>';

               			      if ($is_active == 't'){
                                         echo ' <button class="button button2" title="Disable account!" 
						onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						<i class="fa fa-user"> </i> Disable </button> ';
				      }else{
                                         echo ' <button class="button button4" title="Enable account!" 
                                                onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                <i class="fa fa-user-times"> </i> Enable </button> ';
				     }

                                     if ($trash == 'f'){
                                        echo ' <button class="button button5" title="Trash record!" 
                                               onclick="window.location.href ='."'".'role_trash.php?error='.$id."';".'"> 
                                               <i class="fa fa-trash"> </i> Trash </button> ';
                                     }else{
                                ?>
                                        <a href="role_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }
				    
                                     include('button.php');
				     echo '</td>';
				     $number++;
                                     echo '</tr>';
                                  }
                               } else {
                                  echo "<tr>";
                                  echo "<td colspan='6'> There user privilege configured in the system yet! </td>";
                                  echo "</tr>";
                                  echo "<tr>";
                                  echo "<td colspan='6' align='right'>";
                                  echo "</td>";
                                  echo "</tr>";
                  	       }
                	    ?>
                	    </tbody>
                	    <tfoot>
                	    <tr>
                	    <tr>
                               <th>Id</th>
                	       <th>Role name</th>
                	       <th>Privileges</th>
                               <th>Created By</th>
                               <th align='center'> Action </th>
                	    </tr>
                	    </tr>
                	    </tfoot>
              		 </table>
	              </div>
		
		      <div id="Contact" class="tabcontent">

                           <h3>User accounts 
   	                      <button class='button button2' data-toggle='modal' data-target='#modal-user'>
                	         <i class='fa fa-plus'></i>Add User Account</button>
			   </h3>

              		   <table id="users" class="table table-bordered table-striped">
 			      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Username</th>
                		 <th>User Role </th>
                		 <th>Last Loggin </th>
				 <th align='center'> Action </th>
                	       </tr>
                	       </thead>
                	       <tbody class="text-xs">
  			       <?php	
			          $root = "SELECT * FROM company,user_roles 
					   WHERE companyusertype != 'root' 
					   AND company.companyusertype = user_roles.role_name
					   ORDER BY companyaccountname ASC";
				  $res1 = pg_query($conn, $root);
				  if ($res1) {
    				     // output data of each row
				     $number = 1;
    				     while($row1 = pg_fetch_assoc($res1)) {
                                        (string) $id = $row1['companyurl'];
                                        (string) $trash = $row1['company_trash'];
                                        (string) $is_active = $row1['companyuserisactive'];
                                        (string) $companyurl = $row1['companyurl'];
                                        (string) $account = $row1['companyaccountname'];
       					echo "<tr class='text-sm'>";
       					echo "<td width='3%'>" . $number . "</td>";

					$status = $row1['companyuserisactive'];
					$online = $row1['isonline'];
					if (($status == 'f') && ($online == 'f')){

                   			   	echo " <td title='".$row1['companyfullname']."'> 
						     <i class='fa fa-circle text-danger'></i> " . " " ."
						     <i class='fa fa-user-times'> </i> "."- " 
						     . $row1['companyaccountname'] . "</td>";

					}elseif (($status == 't') && ($online == 't')){

                                       	   	echo " <td title='".$row1['companyfullname']."'> 
						     <i class='fa fa-circle text-success'></i> " . " " ."
						     <i class='fa fa-user'> </i> " . "- " 
                                                     . $row1['companyaccountname'] . "</td>";

					}else{

                                           	echo "<td title='".$row1['companyfullname']."'> 
                                                    <i class='fa fa-circle text-danger'></i> " . " " ."
                                                    <i class='fa fa-user'> </i> " . "- "  
                                                    . $row1['companyaccountname'] . "</td>";
					}

		               			echo "	<td title='" . $row1['role_access_rights'] . "'>" . $row1['role_name'] . "</td>";
					if (!empty($row1['last_loggin'])){

					   	echo "	<td>" . date("F j, Y, g:i a", strtotime($rows['last_loggin'])). "</td>";

					}else{
					   	echo "	<td> <font color='red'> Never logged in </font> </td>";
					}

						echo '	<td width="35%">';
                                        	echo ' 	<a class="button button" href="#edit'.$companyurl.'" data-toggle="modal">
                                               		<i class="fa fa-edit"></i> Edit</a>';

                        			echo '  <a class="button button2" href="#changepwd'.$companyurl.'" data-toggle="modal">
                                        		<i class="fa fa-lock"></i> Change Pin </a>';

					if ($is_active == 't'){
					   	echo ' 	<button class="button button2" title="Disable account!" 
					 	  	onclick="window.location.href ='."'".'disable.php?error='.$id."';".'"> 
						  	<i class="fa fa-user"> </i> Disable </button> ';
					}else{
                                           	echo ' 	<button class="button button4" title="Enable account!" 
                                                  	onclick="window.location.href ='."'".'enable.php?error='.$id."';".'"> 
                                                  	<i class="fa fa-user-times"> </i> Enable </button> ';
				       }

                                      if ($trash == 'f'){
                                         	echo ' 	<button class="button button5" title="Trash record!" 
                                                	onclick="window.location.href ='."'".'user_trash.php?error='.$id."';".'"> 
                                                	<i class="fa fa-trash"> </i> Trash </button> ';
                                      }else{
				?>
                                        	<a href="user_delete.php?error=<?php echo $id;?>" 
						onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
				<?php


                                      }

                                      include('edit_user.php');
                                      echo '</td>';
				      $number++;
                                      echo '</tr>';
                                    }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> There user types configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  	       }
                	      ?>
                	      </tbody>
                	      <tfoot>
                	      <tr>
              		      <tr>
                                 <th>Id</th>
                        	 <th>Username</th>
                        	 <th>User Role</th>
                        	 <th>Last Loggin</th>
                        	 <th align='center'> Action </th>
			      </tr>
                	      </tr>
                	      </tfoot>
              		   </table>


		      </div>

             <div id="About" class="tabcontent">
                    <br> 
             		<table id="syslog" class="table table-bordered table-striped">
                	   <thead>
                	   <tr>
                  	      <th>LogId</th>
                  	      <th>IP</th>
                  	      <th>Log message</th>
                  	      <th>User</th>
                  	      <th>Date</th>
                	   </tr>
                	   </thead>
                	   <tbody>
                	   <?php
                              $sl = "SELECT * FROM syslogs 
				     WHERE syslogs_messages !=''	
				     ORDER BY syslogs_id DESC limit 200";
                              $rt = pg_query($conn, $sl);
                              if ($rt){
                                 $number = 1;
                                 while($sys = pg_fetch_assoc($rt)) {
			   ?>
                	   <tr class='text-sm'>
                  	      <td> <?php echo $sys['syslogs_id']; ?>  </td>
                  	      <td> <?php echo $sys['syslogs_connection_from'];?> </td>
                  	      <td> <?php echo $sys['syslogs_messages']; ?> </td>
                  	      <td>  <?php echo $sys['syslogs_created_by']; ?> </td>
                  	      <td width="20%"> <?php echo $sys['syslogs_day_created']; ?> </td>
                	   </tr>
			   <?php
			         $number++;
			      }
			   }  
			  ?>
			  <tr>	
                  	     <th>LogId</th>
                  	     <th>IP</th>
                  	     <th>Log message</th>
                  	     <th>User</th>
                  	     <th>Date</th>
			  </tr>
            	       </table> 
		    </div>
		</td>
             </tr>
         </table>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          User Account Management
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




      <!-- for root -->
      <div class="modal fade" id="modal-user">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Create a user account </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../account-management/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Username :</label>
                        <input type="text" name="username" class="form-control" placeholder="Enter username" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Full Name :</label>
                        <input type="text" name="fullName" class="form-control" placeholder="Enter user's name" required>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Email Address :</label>
                        <input type="email" name="email" class="form-control" placeholder="Enter user's email address" required>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Phone Contact :</label>
                        <input type="text" name="phone" class="form-control" placeholder="Enter user's phone" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select user role : </label>
			<select name="companyusertype" class="form-control select2" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM user_roles ORDER BY role_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $id = $row['role_id']; 
                                        $name = $row['role_name']; 
                                        $rights = $row['role_access_rights']; ?>
                                        <option value="<?php echo $name; ?> "<?php
                                        if ($rmon == $row['name'])
                                                { echo 'selected'; } ?> title=<?php echo $rights; ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
		    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="register" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->





      <!-- for root -->
      <div class="modal fade" id="modal-root">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Create a root user account </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="index.php" method="post">
            <div class="modal-body">
              
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Enter username" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Full Name</label>
                        <input type="text" name="fullName" class="form-control" placeholder="Enter user's name" required>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Email Address</label>
                        <input type="email" name="email" class="form-control" placeholder="Enter user's email address" required>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Phone Contact</label>
                        <input type="text" name="phone" class="form-control" placeholder="Enter user's phone" required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="user" class="btn btn-primary">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->




  <!-- /.content-wrapper -->

        <!-- Begin form for adding privilege -->
        <div class="modal fade" id="modal-privilege">
          <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-info disabled color-palette">
                   <h4 class="modal-title">Add user privilege </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

                <form action="index.php" method="post">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Privilege Name : </label>
                        <input type="text" name="right_name" class="form-control" placeholder="Enter Privilege name e.g. EDIT " required>
                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="privilege" class="btn btn-primary">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user privilege -->
	<!-- Create role -->
      <div class="modal fade" id="modal-role">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title"> Create user role </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="index.php" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Role name :</label>
                        <input type="text" name="rolename" class="form-control" placeholder="Enter role name E.g Admin." required>
                    </div>

                    <div class="form-group">
                    <label> Role Privileges :  </label>
                    <select name="privilege[]" class="select2" multiple="multiple" data-placeholder="Select a State" data-dropdown-css-class="select2-purple" style="width: 100%;">
                    
                    <?php


                        $ovcdArr = explode(",", $role_access_rights);
                        $sql = "SELECT * FROM access_rights ORDER BY right_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $id = $row['right_id'];
                                        $name = $row['right_name'];
                                        ?>
                                        <option value="<?php echo $name; ?>" 
                                        <?php if (in_array($name, $ovcdArr) ){echo 'selected';}else{ echo " ";} ?> >
                                        <?php echo $name ?> </option>
                        <?php
                                }
                        }
                     ?>

                    </select>

                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="role" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->













        <!-- Begin form for adding user -->
        <div class="modal fade" id="modal-rolee">
          <div class="modal-dialog ">
            <div class="modal-content">
              <div class="modal-header bg-info disabled color-palette">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create user role / profile </h4>
              </div>
              <div class="modal-body">

                <form action="index.php" method="post">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Role name:</label>
                        <input type="text" name="rolename" class="form-control" placeholder="Enter role name E.g Admin." required>
                    </div>

                    <div class="form-group">
                    <label> Role Privileges :  </label>
                    <select name="privilege[]" id="privilege" class="form-control select2" 
                        multiple="multiple" data-placeholder="Click to Select"
                        style="width: 100%;">
                    <?php
			
			$role_access_rights="";
                        $ovcdArr = explode(",", $role_access_rights);
                        $sql = "SELECT * FROM access_rights ORDER BY right_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $id = $row['right_id'];
                                        $name = $row['right_name'];
                                        ?>
                                        <option value="<?php echo $name; ?>" 
                                        <?php if (in_array($name, $ovcdArr) ){echo 'selected';}else{ echo " ";} ?> >
                                        <?php echo $name ?> </option>
                        <?php
                                }
                        }
                     ?>
                    </select>

                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="role" class="btn btn-info">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->




<!-- Java Scripts for tabs on enrollment   -->
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#privilege").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#role").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#syslog").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#users").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>






