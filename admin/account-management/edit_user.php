
        <!-- Begin form for adding user -->
        <div class="modal fade" id="edit<?php echo $companyurl; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
            	<div class="modal-header bg-success disabled color-palette">
              	   <h4 class="modal-title">Editing user account </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM company WHERE companyurl ='".$companyurl."'");
                        $urow=pg_fetch_array($edit);
			(string) $GLOBALS['user_rights'] = $urow['companyusertype'];
           	?>


                <form method="POST" action="../account-management/">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Username :</label>

                        <input  type="hidden" name="companyurl" class="form-control" value="<?php echo $companyurl; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                        <input 	type="text" name="username" class="form-control" value="<?php echo $urow['companyaccountname']; ?>" 
				id="inputdefault" style="width: 100%;"  readonly>
                    </div>
                    <div class="form-group">
                    <label> Full Name :  </label>
                        <input  type="text" name="companyfullname" class="form-control" value="<?php echo $urow['companyfullname']; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                    </div>

                    <div class="form-group">
                    <label> Email Address :  </label>
                        <input  type="text" name="companyemail" class="form-control" value="<?php echo $urow['companyemail']; ?>" 
                                id="inputdefault" style="width: 100%;"  readonly>

                    </div>

                    <div class="form-group">
                    <label> Phone :  </label>
                        <input  type="text" name="companyphone" class="form-control" value="<?php echo $urow['companyphone']; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select user role : </label>
                        <select name="companyusertype" class="form-control select2" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM user_roles ORDER BY role_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $id = $row['role_id'];
                                        $name = $row['role_name'];
                                        (string) $rights = $row['role_access_rights']; ?>
                                        <option value="<?php echo $name; ?> "<?php
                                        if ($user_rights == $name)
                                                { echo 'selected'; } ?> title=<?php echo $rights; ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>


                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="update_user" class="btn btn-success">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->



      <!-- for transfering a member to another Religious Institute -->
        <div class="modal fade" id="changepwd<?php echo $companyurl; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">

              <h4 class="modal-title"> <i class="fa fa-lock"></i>  Change Password  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../account-management/" method="post">
            <div class="modal-body">

                        <div class="form-group">
                                <input type="hidden" name="account" value="<?php echo $companyurl; ?>"
                                class="form-control" >
                        </div>

                        <div class="form-group">
                                <input minlength="4" maxlength="15" type="password" name="new_passcode" class="form-control" placeholder="Enter New Password" required>
                        </div>

                        <div class="form-group">
                                <input minlength="4" maxlength="15" type="password" name="cfm_passcode" class="form-control"  placeholder="Confirm New Password" required>
                        </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="change_password" class="btn btn-info"> Continue </button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
