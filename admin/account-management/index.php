<?php
	session_start();
	require_once'../library/config.php';
 	logout_session($conn); //Check wheather user logged in
	auto_logout($conn); //Logout iddle user
        $GLOBALS['app'] = "settings";
        $GLOBALS['nav'] = "account";
        $GLOBALS['error'] = "";

	$ip = ip_add();

        # Change password 
        if(isset($_POST['change_password'])){
                $account = test_input($_POST['account']);
                $new_passcode = test_input($_POST['new_passcode']);
                $cfm_passcode = test_input($_POST['cfm_passcode']);

                if ($new_passcode != $cfm_passcode){
                        $GLOBALS['error'] = "Invalid! New Password did not match";
                        $msg = $error;
                        $type = "-- warning --";
                        syslogs($conn,$ip,$msg,$user,$type);
		
                }else{
                	$sql = "SELECT companyemail FROM company 
                                WHERE companyurl = '$account'";
                        // If result matched $myusername and $mypassword, table row must be 1 row
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                        // output data of each row
                        	while($row = pg_fetch_assoc($result)) {
                                        $email = $row['companyemail'];
                                        $ln = $row['company_domain'];
                                        $msg = "Your new password is: ";

                                        #Update the password
                                        $code = sha1($new_passcode);
                                        $pwd = "UPDATE company SET passcode = '$code',
                                                companyneverloggedin = false,
                                                cfmpasscode = '$code',
                                                company_last_updated_on = now() 
                                                WHERE companyemail = '$email'";
                                        $res = pg_query($conn,$pwd);

                                        #Calling the email function
                                        $type = "-- normal --";
                                        $user = $email;
                                        syslogs($conn,$ip,$msg,$type,$user);

                                        $lnk = $ln;
                                        $sub = "Password Recovery: ";
                                        $to = $email;
                                        $phone = $phone;
                                        $new_pwd = $new_passcode;
                                        $msg = "Your new password is : ". $new_pwd ."\n The link is ".$lnk;
                                        //Sending email 
                                        $from = 'noreply@gemcs.biz';
                                        send_email($conn,$sub,$msg,$to,$from);
                                	$GLOBALS['error'] = "New Password has been sent to your email.";
                                }
                        }else{
                        	$GLOBALS['error'] = "Invalid! Old Password";
                        	$msg = $error;
                        	$type = "-- warning --";
                        	syslogs($conn,$ip,$msg,$user,$type);

				#Login again
				session_destroy();
			}
                }
        }



        # Processing Privilege
        if(isset($_POST['role'])){
                $rolename = test_input($_POST['rolename']);
                $link = sha1($rolename);
                $user = test_input($_SESSION['user']);
		$privilege = $_POST['privilege'];
		$privilege =  implode(",", $privilege );
		
                $sql = "INSERT INTO user_roles (role_name,role_access_rights,role_createdby,role_url_link) 
                        VALUES ('$rolename','$privilege','$user','$link')";
                $result = pg_query($conn,$sql);
		
		$tab = '1';
                if ($result){
                        $ip = ip_add();
                        $type = "-- normal --";
                        $GLOBALS['error'] = "User role has been added by  " .$user. ".";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../account-management/?tab_role='.$tab.'"</script>';
                }else{
                        $ip = ip_add();
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! Adding user role has failed. ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../account-management/?tab_role='.$tab.'"</script>';
                }
        }

        # Processing Role
        if(isset($_POST['update_role'])){
                $role_url_link = test_input($_POST['role_url_link']);
                $rolename = test_input($_POST['rolename']);
                $user = test_input($_SESSION['user']);
                $privilege = $_POST['privilege'];
                $privilege =  implode(",", $privilege );
                $application = $_POST['application'];
                $application =  implode(",", $application );

                $sql = "UPDATE user_roles SET role_name = '$rolename',
			role_access_rights = '$privilege',
			role_modules = '$application',
			role_last_updated_on = now(),
			role_last_updated_by = '$user'
			WHERE role_url_link = '$role_url_link'";
                $result = pg_query($conn,$sql);

		$tab = '1';
                if ($result){

                        $ip = ip_add();
                        $type = "-- normal --";
                        $GLOBALS['error'] = "User role has been modified by  " .$user. ".";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../account-management/?tab_role='.$tab.'"</script>';
                }else{
                        $ip = ip_add();
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! Modifying user role has failed. ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../account-management/?tab_role='.$tab.'"</script>';
                }
        }

	
	# Processing Privilege
        if(isset($_POST['privilege'])){
                $privilege = test_input($_POST['right_name']);
		$link = sha1($privilege);
                $user = test_input($_SESSION['user']);
		
		if (!empty($privilege)){
			$sql = "INSERT INTO access_rights (right_name,access_created_by,access_url) 
				VALUES ('$privilege','$user','$link')";
		}
		$result = pg_query($conn,$sql);
		$tab = '1';	
		if ($result){

                	$ip = ip_add();
                	$type = "-- normal --";
                	$GLOBALS['error'] = "User privilege has been added by  " .$user. ".";
                	$msg = $error;
                	syslogs($conn,$ip,$msg,$user,$type);
                	echo '<script>window.location="../account-management/?tab_priv='.$tab.'"</script>';
		}else{
                        $ip = ip_add();
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! Adding user privilege has failed. ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../account-management/?tab_priv='.$tab.'"</script>';
		}
	}

        #Editing root acount
        if (isset($_POST['update_privilege'])) {
                $access_url = test_input($_POST['access_url']);
                $right_name = test_input($_POST['right_name']);
                $user = test_input($_SESSION['user']);

                $tab = '1';
                $sql = "UPDATE access_rights SET
                        right_name = '$right_name',
                        access_lastupdated_by = '$user'
                        WHERE access_url = '$access_url'";
                $results = pg_query($conn,$sql);

                $ip = ip_add();
                $type = "-- normal --";
                $GLOBALS['error'] = "User privileges " .$right_name. " updated.";
                $msg = $error;
                $user = $user_check;
                syslogs($conn,$ip,$msg,$user,$type);
                echo '<script>window.location="../account-management/?tab_priv='.$tab.'"</script>';
        }

        // Registering root account
        if(isset($_POST['register'])){
                $username = test_input($_POST['username']);
                $fname = test_input($_POST['fullName']);
                $usertype = test_input($_POST['companyusertype']);
                $email = test_input($_POST['email']);
                $phone = test_input($_POST['phone']);
                $passCode = randomString(); 
                $cfmPassCode = $passCode;
                $connecting_IP = test_input($_SERVER['REMOTE_ADDR']);
		$user = test_input($_SESSION['login_user']);

                if (empty($passCode) ){
                        $GLOBALS['error'] = "Password is required";
                        $msg = "Trying to create an account without a password";
                        syslogs($conn,$connecting_IP,$msg);
                }
                if (empty($cfmPassCode) ){
                        $GLOBALS['error'] = "Confirm Password.";
                        $msg = "Trying to create an account without a password";
                        syslogs($conn,$connecting_IP,$msg);
                }
                if (empty($passCode) && empty($cfmPassCode) && empty($email) && empty($phone) && empty($username)) {
                        $GLOBALS['error'] = "Password and username are required";
                        $msg = "Please try again...enter your password.";
                        syslogs($conn,$connecting_IP,$msg);
                }else {
                        if (($passCode == $cfmPassCode) && !empty($phone) && !empty($username)){
                        $passCode = $passCode;
                        }else{
                                $GLOBALS['error'] = "Password did not match. Try again";
                                $msg = "Password did not match. Try again.";
                                syslogs($conn,$connecting_IP,$msg);
                      	}
                }

               if (($passCode == $cfmPassCode) && !empty($phone) && !empty($username) && !empty($email)){
                        $sql = "SELECT * FROM company  WHERE (companyaccountname = '$username') OR 
				(companyemail = '$email') OR (companyphone = '$phone')";
                        $result = pg_query($conn,$sql);
                        $row = pg_fetch_array($result);
                        $count = pg_num_rows($result);
                        // If result matched $myusername and $mypassword, table row must be 1 row
                        if($count > 0) {
                                $GLOBALS['error'] = "Invalid! Check username ".$username .", " . $email . " or " . $phone ." already exists.";
                        }else{
				
				$ip = ip_add();
                		$link = sha1($email);
                		$pass = sha1($passCode);
                		$pass_cfm = $pass;
                		$create_user = "INSERT INTO Company (CompanyAccountName,CompanyFullName,CompanyUserType,
                				CompanyEmail,CompanyPhone,passCode,cfmPassCode,CompanyNameCreatedBy,CompanyUrl)
                				VALUES('$username','$fname','$usertype','$email','$phone','$pass',
                				'$pass_cfm','$user','$link')";
                		$res = pg_query($conn,$create_user);

                		$type = "-- normal --";
                		$msg = "User account for " . $username . " " . $email . "has been created using online portal by " . $user . ".";
                		syslogs($conn,$ip,$msg,$type,$user);

				$lnk = "http://estate.gemcs.biz/";
                		$sub = "Account Creation";
                		$email = $email;
                		$phone = $phone;
                		$new_pwd = $passCode;
                		$msg = "Account has been created\n Username is : ". $email . " password is : "
					.$new_pwd. "\n The link is ".$lnk;
                		//Sending email to the new staff added
				sendemail($conn,$sub,$msg,$email);
							$GLOBALS['error'] = "Account successfuly created.";
							echo '<script>window.location="../account-management/?tab_priv='.$tab.'"</script>';
                	}
        	}
	}


        // Registering root account
        if(isset($_POST['user'])){
                $username = test_input($_POST['username']);
                $fname = test_input($_POST['fullName']);
                $usertype = "root";
                $email = test_input($_POST['email']);
                $phone = test_input($_POST['phone']);
                $passCode = randomString(); 
                $cfmPassCode = $passCode;
                $connecting_IP = test_input($_SERVER['REMOTE_ADDR']);
		$user = test_input($_SESSION['login_user']);

                if (empty($passCode) ){
                        $GLOBALS['error'] = "Password is required";
                        $msg = "Trying to create an account without a password";
                        syslogs($conn,$connecting_IP,$msg);
                }
                if (empty($cfmPassCode) ){
                        $GLOBALS['error'] = "Confirm Password.";
                        $msg = "Trying to create an account without a password";
                        syslogs($conn,$connecting_IP,$msg);
                }
                if (empty($passCode) && empty($cfmPassCode) && empty($email) && empty($phone) && empty($username)) {
                        $GLOBALS['error'] = "Password and username are required";
                        $msg = "Please try again...enter your password.";
                        syslogs($conn,$connecting_IP,$msg);
                }else {
                        if (($passCode == $cfmPassCode) && !empty($phone) && !empty($username)){
                        $passCode = $passCode;
                        }else{
                                $GLOBALS['error'] = "Password did not match. Try again";
                                $msg = "Password did not match. Try again.";
                                syslogs($conn,$connecting_IP,$msg);
                      	}
                }

               if (($passCode == $cfmPassCode) && !empty($phone) && !empty($username) && !empty($email)){
                        $sql = "SELECT * FROM Company  WHERE CompanyAccountName = '$username' AND 
				CompanyEmail = '$email' AND CompanyPhone = '$phone'";
                        $result = pg_query($conn,$sql);
                        $row = pg_fetch_array($result,MYSQLI_ASSOC);
                        $count = pg_num_rows($result);
                        // If result matched $myusername and $mypassword, table row must be 1 row
                        if($count > 0) {
                                $GLOBALS['error'] = $username ." " . $email . " already exists.";
                        }else{
				
				$ip = ip_add();
                		$link = sha1($email);
                		$pass = sha1($passCode);
                		$pass_cfm = $pass;
                		$create_user = "INSERT INTO Company (CompanyAccountName,CompanyFullName,CompanyUserType,
                				CompanyEmail,CompanyPhone,passCode,cfmPassCode,CompanyNameCreatedBy,CompanyUrl)
                				VALUES('$username','$fname','$usertype','$email','$phone','$pass',
                				'$pass_cfm','$user','$link')";
                		$res = pg_query($conn,$create_user);

                		$type = "-- normal --";
				$user = test_input($_SESSION['login_user']);
                		$msg = "User account for " . $username . " " . $email . "has been created using online portal by " . $user . ".";
                		syslogs($conn,$ip,$msg,$type,$user);

				$lnk = "http://aru.gemcs.biz/";
                		$sub = "Account Creation";
                		$email = $email;
                		$phone = $phone;
                		$new_pwd = $passCode;
                		$msg = "Account has been created\n Username is : ". $email . " password is : "
					.$new_pwd. "\n The link is ".$lnk;
                		//Sending email to the new staff added
				sendemail($conn,$sub,$msg,$email);
				$GLOBALS['error'] = "Account successfuly created.";
                	}
        	}
	}

	#Editing root acount
        if (isset($_POST['update_root'])) {
                $companyurl = test_input($_POST['companyurl']);
                $companyaccountname = test_input($_POST['username']);
                $companyfullname = test_input($_POST['companyfullname']);
                $companyemail = test_input($_POST['companyemail']);
                $companyphone = test_input($_POST['companyphone']);
                $user = $_SESSION['user'];

		$tab = '1';
                $sql = "UPDATE company SET
                       	companyfullname = '$companyfullname',
                       	companyphone = '$companyphone',
                       	company_last_update_by = '$user'
                       	WHERE companyurl = '$companyurl'";
                $results = pg_query($conn,$sql);

                $ip = ip_add();
                $type = "-- normal --";
                $GLOBALS['error'] = "Details for the user " .$companyfullname. " have been updated.";
                $msg = $error;
                $user = $user_check;
                syslogs($conn,$ip,$msg,$user,$type);
		echo '<script>window.location="../account-management/?tab_root='.$tab.'"</script>';
        }

        #Editing root acount
        if (isset($_POST['update_user'])) {
                $companyurl = test_input($_POST['companyurl']);
                $companyaccountname = test_input($_POST['username']);
                $companyfullname = test_input($_POST['companyfullname']);
                $companyemail = test_input($_POST['companyemail']);
                $companyphone = test_input($_POST['companyphone']);
                $companyusertype = test_input($_POST['companyusertype']);
                $user = $_SESSION['user'];

                $tab = '1';
                $sql = "UPDATE company SET
                        companyfullname = '$companyfullname',
                        companyphone = '$companyphone',
                        company_last_update_by = '$user',
			company_last_updated_on = now(),
			companyusertype = '$companyusertype'
                        WHERE companyurl = '$companyurl'";
                $results = pg_query($conn,$sql);

                $ip = ip_add();
                $type = "-- normal --";
                $GLOBALS['error'] = "Details for the user " .$companyfullname. " have been updated.";
                $msg = $error;
                $user = $user_check;
                syslogs($conn,$ip,$msg,$user,$type);
                echo '<script>window.location="../account-management/?tab_user='.$tab.'"</script>';
        }







        // End Registration company membership 
        # Processing user type changes
        if(isset($_POST['edituser'])){
                $username = test_input($_POST['username']);
                $url = test_input($_POST['url']);
                $fname = test_input($_POST['fname']);
                $level = test_input($_POST['level']);
                $new_passcode = test_input($_POST['passCode']);
		$new_pwd = $new_passcode;
		$new_passcode = sha1($new_passcode);
                $cfm_passcode = test_input($_POST['cfmPassCode']);
		$cfm_passcode = sha1($cfm_passcode);
                $user = test_input($_SESSION['login_user']);

                if (empty($username) ){
                        $GLOBALS['error'] = "Name is required";
                        $msg = "User trying to create an account without account name";
                        syslogs($conn,$connecting_IP,$msg);
                }
                if (empty($passCode) ){
                        $GLOBALS['error'] = "Password is required";
                        $msg = "Trying to create an account without a password";
                        syslogs($conn,$connecting_IP,$msg);
                }
                if (empty($cfmPassCode) ){
                        $GLOBALS['error'] = "Confirm Password.";
                        $msg = "Trying to create an account without a password";
                        syslogs($conn,$connecting_IP,$msg);
                }

                if (!empty($url) && !empty($new_passcode)){

                        $sql = "SELECT * FROM Company 
                                WHERE CompanyUrl = '$url'
				AND CompanyAccountName = '$username'";

                        $result = pg_query($conn,$sql);
                        $row = pg_fetch_array($result,MYSQLI_ASSOC);
                        $count = pg_num_rows($result);
                        // If result matched 
                        if($count > 0) {

                                if ($new_passcode == $cfm_passcode){

                                        $update = "UPDATE Company SET 
                                                        passCode = '$new_passcode',
                                                        cfmPassCode = '$cfmPassCode',
							CompanyFullName = '$fname',
							CompanyUserType = '$level',
							CompanyNameCreatedBy = '$user'
                                                   WHERE CompanyUrl = '$url'";

                                        $result = pg_query($conn,$update);

                                        //Sending email notification
                                        $sub = "New Password";
                                        $email = $row['CompanyEmail'];
                                        $phone= $row['CompanyPhone'];
                                        $msg = "Your new password is: ";
                                        emailing($conn,$sub,$msg,$email,$phone,$new_pwd);

                                        //Logging the transactions
                                        $GLOBALS['error'] = "Password for " . $fname . " has been updated!";
                                        $msg = $error;
                                        $type = "--warning--";
                                        syslogs($conn,$connecting_IP,$msg,$type,$user);

                                }else{
                                        $GLOBALS['error'] = "Invalid! New password did not match.";
                                        $msg = $error;
                                        $type = "--warning--";
                                        syslogs($conn,$connecting_IP,$msg,$type,$user);

                                }

                        }else{
                        $GLOBALS['error'] = "Invalid! Current password did not match.";
                        $msg = $error;
                        syslogs($conn,$connecting_IP,$msg);
                        }
                }
        }

 	require_once'../header.php';
 	require_once'../navigation.php';
 	include('body.php');
 	require_once'../footer.php';
?>

