  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Morris charts -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">  
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <!-- Ichecker -->
  <link rel="stylesheet" href="../plugins/iCheck/all.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../bower_components/select2/dist/css/select2.min.css">
  <!-- JS for pie charts -->
  <script src="../code/highcharts.js"></script>
  <script src="../code/modules/exporting.js"></script>
  <!-- Theme style -->
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">


<!-- for pop up modal -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="_https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<?php 
	session_start(); 
        require_once'../library/config.php';
        logout_session($conn); //Check wheather user logged in
        auto_logout($conn); //Logout iddle user
        $GLOBALS['error'] = "";

	#Get Enroollment token
    	$role_url_link = $_GET['role_url_link'];                                                                                       
    	if (isset($_POST['submit'])) {                                                                                                    
        	$role_url_link = test_input($_POST['role_url_link']);
                $role_name = test_input($_POST['role_name']);
                $privilege = $_POST['privilege'];
                $privilege =  implode(",", $privilege );
		$user = $_SESSION['user'];

		$sql = "UPDATE user_roles SET
			role_name = '$role_name',
			role_access_rights = '$privilege',
			role_last_updated_by = '$user'
			WHERE role_url_link = '$role_url_link'";
		$results = pg_query($conn,$sql);

                $ip = ip_add();
                $type = "-- warning --";
                $GLOBALS['error'] = "User privileges  " .$right_name. " has been updated.";
                $msg = $error;
                syslogs($conn,$ip,$msg,$user,$type);
                                                                                           
		echo '<script>window.location="../userPrivileges/"</script>'; 
    	}

	#Get data for the form to edit
 	$sql = "SELECT * FROM user_roles 
        	WHERE role_url_link = '$role_url_link' LIMIT 1";
    	$result = pg_query($conn, $sql);
    	if ($result) {
    		// output data of each row
    		while($mem = pg_fetch_assoc($result)) {
?>
<form method="post" action="editprivilege.php" role="form">
        <div class="modal-body">
                <div class="form-group">

                        <input type="hidden" class="form-control" id="role_url_link" name="role_url_link" 
				value="<?php echo $mem['role_url_link'];?>" readonly="true"/>
                </div>
                <div class="form-group">
                        <label for="name">Role name : </label>
                        <input type="text" class="form-control" id="role_name" name="role_name" maxlength="15" 
				value="<?php echo $mem['role_name'];?>" readonly/>
                </div>

	        <div class="form-group">
                    <label> Role Privileges :  </label>
                    <select name="privilege[]" id="privilege" class="form-control select2" 
                        multiple="multiple" data-placeholder="Click to Select"
                        style="width: 100%;">
                    <?php
			(string) $rights = $mem['role_access_rights'];
                        $ovcdArr = explode(",", $rights);			
                        $sql = "SELECT * FROM access_rights ORDER BY right_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $id = $row['right_id'];
                                        $name = $row['right_name'];
                                        ?>
                                        <option value="<?php echo $name; ?>" 
                                        <?php if (in_array($name, $ovcdArr) ){echo 'selected';}else{ echo " ";} ?> >
                                        <?php echo $name ?> </option>
                        <?php
                                }
                        } pg_close($conn);
                     ?>
                    </select>
                </div>
                
		<div class="modal-footer">
                	<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline" name="submit" value="Update" > Save </button>
              	</div>

        </form>

<?php }} ?>

</html>


