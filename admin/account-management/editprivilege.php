<?php  
	session_start(); 
        require_once'../library/config.php';
        logout_session($conn); //Check wheather user logged in
        auto_logout($conn); //Logout iddle user
        $GLOBALS['error'] = "";

	#Get Enroollment token
    	$access_url = $_GET['access_url'];                                                                                       
    	if (isset($_POST['submit'])) {                                                                                                    
        	$access_url = test_input($_POST['access_url']);
                $right_name = test_input($_POST['right_name']);
		$user = $_SESSION['user'];

		$sql = "UPDATE access_rights SET
			right_name = '$right_name',
			access_lastupdated_by = '$user'
			WHERE access_url = '$access_url'";
		$results = pg_query($conn,$sql);

                $ip = ip_add();
                $type = "-- warning --";
                $GLOBALS['error'] = "User privileges  " .$right_name. " has been updated.";
                $msg = $error;
                syslogs($conn,$ip,$msg,$user,$type);
                                                                                           
		echo '<script>window.location="../userPrivileges/"</script>'; 
    	}

	#Get data for the form to edit
 	$sql = "SELECT * FROM access_rights 
        	WHERE access_url = '$access_url' LIMIT 1";
    	$result = pg_query($conn, $sql);
    	if ($result) {
    		// output data of each row
    		while($mem = pg_fetch_assoc($result)) {
?>
<body>
<form method="post" action="editprivilege.php" role="form">
        <div class="modal-body">
                <div class="form-group">

                        <input type="hidden" class="form-control" id="access_url" name="access_url" 
				value="<?php echo $mem['access_url'];?>" readonly="true"/>
                </div>
                <div class="form-group">
                        <label for="name">User privilege name : </label>
                        <input type="text" class="form-control" id="right_name" name="right_name" maxlength="15" 
				value="<?php echo $mem['right_name'];?>" />
                </div>
                
		<div class="modal-footer">
                	<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline" name="submit" value="Update" > Save </button>
              	</div>

        </form>
</body>

<?php }} ?>

</html>


