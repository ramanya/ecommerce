<?php
	session_start();
	require_once'../library/config.php';
 	logout_session($conn); //Check wheather user logged in
    auto_logout($conn); //Logout iddle user
    $GLOBALS['app'] = "slider";
    $GLOBALS['nav'] = "slider";
    $GLOBALS['error'] = "";

    #Uploading member photo
    if(isset($_FILES['slider_name'])){
        $error= array();
        $file_name = $_FILES['slider_name']['name'];
        $file_size =$_FILES['slider_name']['size'];
        $file_tmp =$_FILES['slider_name']['tmp_name'];
        $file_type=$_FILES['slider_name']['type'];
        //$file_ext=strtolower(end(explode('.',$_FILES['slider_name']['name'])));     
        $file_ext = pathinfo($_FILES["slider_name"]["name"])['extension'];
        photo_upload($error,$file_name,$file_size,$file_tmp,$file_type,$file_ext);
    }
    # Processing associations
    if(isset($_POST['slider'])){
	    $slider_code = randomString();
		$slider_name = $_FILES['slider_name']['name'];
        $slide_description = test_input($_POST['slide_description']);
        $link = sha1($slider_code);
        $user = test_input($_SESSION['user']);

        $sql = "SELECT * FROM slider WHERE slider_name = '$slider_name'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_array($result);
        $count = pg_num_rows($result);
        // If result matched $myusername and $mypassword, table row must be 1 row
        if($count > 0) {
            $GLOBALS['error'] = "Invalid! This image ".$slider_name." already exists.";
            $GLOBALS['slider'] = $row['slider_code_url'];
        }else{
		
            $sql = "INSERT INTO slider (slider_code,slider_name,slider_description,slider_createdby,slider_code_url)
                    VALUES ('$slider_code','$slider_name','$slide_description','$user','$link')";
            $result = pg_query($conn,$sql);
		    $tab = '1';
            if ($result){
                $ip = ip_add();
                $type = "-- normal --";
                $GLOBALS['error'] = "A slider image ".$slider_name." has been created by  " .$user. ".";
                $msg = $error;
                syslogs($conn,$ip,$msg,$user,$type);
                #echo '<script>window.location="../association/"</script>';
				$GLOBALS['slider'] = $link;
            }else{
                $ip = ip_add();
                $type = "-- warning --";
                $GLOBALS['error'] = "Invalid! Your request has failed. Try again later! ";
                $msg = $error;
                syslogs($conn,$ip,$msg,$user,$type);
				$GLOBALS['slider'] = $link;
                #echo '<script>window.location="../association/"</script>';
            }
        }
    }

    # Processing staff update
    if(isset($_POST['update_slider'])){
        $link = test_input($_POST['slider_code_url']);
		$slider_name = $_FILES['slider_name']['name'];
        $slider_description = test_input($_POST['slider_description']);
        $user = test_input($_SESSION['user']);
        $association_org = test_input($_SESSION['cci']);
		$photo_url = test_input($_POST['photo_url']);

        if (empty($slider_name)){
            $slider_name = $photo_url;
        }
        
        $sql = "UPDATE slider SET slider_name = '$slider_name',
			slider_description = '$slider_description',
			slider_createdby = '$user',
			slider_updatedon = now()
			WHERE slider_code_url = '$link'";
        $result = pg_query($conn,$sql);

		$tab = '1';
        if ($result){
            $ip = ip_add();
            $type = "-- normal --";
            $GLOBALS['error'] = "Image slider ". $slider_name. "has been modified by  " .$user. ".";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
		
			$GLOBALS['slider'] = $link;
        }else{
            $ip = ip_add();
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! Your request has failed.Try again later! ";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
			$GLOBALS['slider'] = $link;
        }
    }

 	require_once'../header.php';
 	require_once'../navigation.php';
 	include('body.php');
 	require_once'../footer.php';
?>

