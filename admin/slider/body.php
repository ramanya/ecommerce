<style>

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 7px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Image Slider Manager </h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
	    <?php
		    //For alert messages
        	if(strlen($error) > 0){
			    echo '	<div class="alert alert-success" role="alert" data-auto-dismiss="3000">
  					        '.$error.'
				        </div>';
		    }
	    ?>

	<table id="cci" class="table table-bordered ">
        <tr>
	        <td>

		    <div class="tab">
  			    <button class="tablinks" onclick="openCity(event, 'scheme')" id="defaultOpen"> Slider Images </button>
		        </div>

		        <div id="scheme" class="tabcontent">
  			        <h4> Images   
                    <?php
                        $cci = $_SESSION['cci'];
                        $access = $_SESSION['privilege'];
                        if  (strstr($access,'INSERT') && empty($cci)){
                            echo "<button class='button button2' data-toggle='modal' data-target='#modal-slider'>
                                  <i class='fa fa-plus'></i> Add Image </button>";
                        }else{
                            echo "<button class='button button4' data-toggle='modal' data-target='#modal-slide'disabled>
                                <i class='fa fa-plus'></i> Add Image </button>";
                        }
                    ?>

			        </h4>

                	<table id="example1" class="table table-bordered ">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Code </th>
                            <th> Photo </th>
                            <th>Name </th>
                            <th> IsOnline </th>
                            <th>CreatedOn </th>
                            <th align='center'> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $cci = $_SESSION['cci'];
                            $access = $_SESSION['privilege'];
                            if (strstr($access,'READ') && (empty($cci))){
                                $root = "SELECT * FROM slider ORDER BY slider_createdon DESC";
                            }
                            $res1 = pg_query($conn, $root);
                            if ($res1) {
                                // output data of each row
                                $num = 1;
                                while($row1 = pg_fetch_assoc($res1)) {
                                    (string) $id = $row1['slider_code_url'];
                                    (string) $status = $row1['slide_status'];
                                    (string) $slider_name = $row1['slider_name'];
                                    (string) $slider_code_url = $row1['slider_code_url'];
                                    echo "<tr class='text-sm'>";
                                    echo "	<td width='4%'> " . $num . "  </td>";
                                    echo "	<td> " . $row1['slider_code']. "  </td>";
                                    echo "<td>";
                                    if (!empty($slider_name)){
                                        echo '  <img src="images/'
                                                .$slider_name.'" alt="User Image"style="width:30px" >';
                                    }else{
                                        echo '  <img src="images/img_default.png" alt="Avatar" 
                                               style="width:30px">';
                                        }

                                    echo "</td>";
                                    echo "  <td> " . strtoupper($row1['slider_description']) . " </td>";
                                    echo "  <td> " . strtoupper($row1['slide_status']) . " </td>";
                                    echo "  <td> " . strtoupper(edit_date($row1['slider_createdon'])) . " </td>";
                                    echo '  <td width="25%">';
                                    echo '  <a class="button button5" href="#view'.$slider_code_url.'" data-toggle="modal">
                                            <i class="fa fa-eye"></i> View</a>';

                                    #Open scheme Details after adding record
                                    if (!empty($slider)){
                                        echo "  <script type='text/javascript'>
                                                    $(document).ready(function(){
                                                        $('#view".$slider."').modal('show');
                                                    });
                                                 </script>";
                                    }

                                    if  (strstr($access,'EDIT')){
                                        echo '<a class="button button" href="#edit'.$slider_code_url.'" data-toggle="modal">
                                           	<i class="fa fa-edit"></i> Edit</a>';
                                    }else{
                                        echo "<button class='button button4' data-toggle='modal' data-target='#modal-staff'disabled>
                                            <i class='fa fa-edit'></i> Edit </button>";
                                    }

                                    if  (!empty(strstr($access,'DELETE'))){
                                        echo '<a href="slider_delete.php?error='. $id.'"
                                            onclick="return confirm(\'Are you sure to delete?\')">
                                            <button class="button button3" >
                                            <i class="fa fa-trash"> </i> Delete </button> </a>';
                                    }else{
                                        echo '<a href="#scheme_delete.php?error='. $id.'"
                                            onclick="return confirm(\'Are you sure to delete?\')">
                                            <button class="button button5" disabled>
                                            <i class="fa fa-trash"> </i> Delete </button> </a>';
                                    }

                                    include('edit_slider.php');
                                    echo '</td>';
                                    $num++;
                                    echo '</tr>';
                                }
                            } else {
                                echo "<tr>";
                                echo "<td colspan='6'> No Campus configured in the system yet! </td>";
                                echo "</tr>";
                                echo "<tr>";
                                echo "<td colspan='6' align='right'>";
                                echo "</td>";
                                echo "</tr>";
                            }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Code </th>
                            <th> Photo </th>
                            <th>Name </th>
                            <th> IsOnline </th>
                            <th>CreatedOn </th>
                            <th align='center'> Action </th>
                        </tr>
                        </tfoot>
                 	</table>

		        </div>
		

                <div id="version" class="tabcontent">
                        <h3>Tokyo</h3>
                        <p> No Data available. </p>
                </div>

            </div>

		</td>
    </tr>
    </table>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Image Slider Manager 
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For tab navigation   -->
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>





      <!-- for scheme -->
      <div class="modal fade" id="modal-slider">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title"> Upload Image for Slider  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../slider/" method="post" enctype="multipart/form-data">
            <div class="modal-body">

            <table class="table table-bordered  mb-0">
		    <tr>
                <td width="50%">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Image slide ( size of 1920 X 974) :</label>
                        <input name="slider_name" type="file" id="exampleInputFile" >
                    </div>

                </td>
                <td>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Description :</label>
                        <input type="text"  name="slide_description" class="form-control" 
                            placeholder="E.g. For sale" > 
                    </div>
                   </td>
                </tr>

	        </table>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="slider" class="btn btn-info"> Upload </button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



<!-- Java Scripts for tabs on enrollment   -->
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<!-- for table search and pagination -->
<script>
  $(function () {
    $("#fac").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


