        <!-- Begin form for adding user -->
		<div class="modal fade" id="view<?php echo $slider_code_url;?>" tabindex="-1" role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
          <div class="modal-dialog modal-xl">
            <div class="modal-content">
            	<div class="modal-header bg-info disabled color-palette">
              	   <h4 class="modal-title">Slider Details </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM slider WHERE slider_code_url ='".$slider_code_url."'");
                        $urow=pg_fetch_array($edit);
                        (string) $GLOBALS['slider_name'] = $urow['slider_name'];
           	?>

        <div class="table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table table-bordered mb-0">
            	<tr>

			<td width="50%" align="center">
			<!-- for the association logo -->

                        <?php   if (!empty($slider_name)){
                                        echo '  <img src="images/'.$slider_name.'" alt="User Image"
                                        height="350" width="350" > ';
                                }else{
                                        echo '  <img src="images/img_avatar.png" alt="User Image"
                                        height="150" width="150" > ';
                                }
                        ?>


			</td>

			<td>
                    		<label> Slider Details :  </label>
				<hr />
			
                            <label> Publication Code :  </label>
                            <?php echo $urow['slider_code']; ?> 
                            <br />
                            <label> Published on :  </label>
                            <?php echo $urow['slider_createdon']; ?>

                    		<label> By :  </label>
                    		<?php echo $urow['slider_createdby']; ?> 
			                <br />
                    		<label> Last Updated on  : </label>
                    		<?php echo $urow['slider_updatedon']; ?> 
			                <br />
                  	</td>

		</tr>

        </table>

       	</div> <!-- end of the table -->


       	</div>
       		<div class="modal-footer justify-content-between">
       		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php
        	echo '<button title="Click to print." class="btn btn-default" 
                      onclick="window.open('."'".'Printing.php?error='.$slider_code_url."','_blank','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=400')".'" > <i class="fa fa-print"></i> Print </button>  ';
	?>

       	 	</div>
        </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



        <!-- Begin form for adding user -->
        <div class="modal fade" id="edit<?php echo $slider_code_url; ?>" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
            	<div class="modal-header bg-success disabled color-palette">
              	   <h4 class="modal-title">Editing Slider Details </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM slider WHERE slider_code_url ='".$slider_code_url."'");
                        $urow=pg_fetch_array($edit);
			            (string) $GLOBALS['slider_name'] = $urow['slider_name'];
           	    ?>

            <form action="../slider/" method="post" enctype="multipart/form-data">
            <div class="modal-body">

            <table class="table table-bordered mb-0">

	       <tr>
                  <td width="50%">
                    <div class="form-group">
                <?php
                        if (!empty($slider_name)){
                                echo '  <img src="images/'.$slider_name.'" alt="User Image"
                                        height="50" width="50" >
                                        <font color="red"> Current Image! </font>
                                        <input name="photo_url" type="hidden" value='.$slider_name. '> <br />
                                        <input name="slider_name" type="file" id="exampleInputFile" >';
                        }else{
                                echo '<input name="slider_name" type="file" id="exampleInputFile" >';
                        }
                ?>

                    </div>
                </td>

	          <td width="50%">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Description :</label>
                        <input  type="hidden" name="slider_code_url" class="form-control" value="<?php echo $slider_code_url; ?>" 
                                id="inputdefault" style="width: 100%;"  required>
                        <input type="text"  name="slider_description" value="<?php echo $urow['slider_description']; ?>"
				class="form-control" placeholder="Enter here ..." required>
                    </div>
	          </td>
             </tr>
	        </table>

            </div>
            </div>


            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="update_slider" class="btn btn-success"> Update </button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->










      <!-- for root -->
      <div class="modal fade" id="core<?php echo $association_code; ?>"  role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-warning disabled color-palette">
              <h4 class="modal-title">Add Core Management Member </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../association/" method="post">
            <div class="modal-body">

            <table class="table table-bordered mb-0">
               <tr>

                  <td width="33%" >

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Salutation : </label>
                        <select name="core_management_salutation[]" class="select2" multiple="multiple" data-placeholder="Select a State"
                                data-dropdown-css-class="select2-purple" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM salutation ORDER BY salutation_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['salutation_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                </td>
                  <td width="33%">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Name :</label>
                   	<input type="hidden" name="core_management_assoc" value="<?php echo $association_code; ?>" 
                   	class="form-control" >
                        <input type="text" name="core_management_member" class="form-control" placeholder="Enter here ..." required>
                    </div>

		  </td>
		  <td width="33%" >

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Title : </label>
                        <select name="core_management_title" class="form-control select2" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM title ORDER BY title_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['title_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
		</td>

		</tr>
		<tr>
		<td>

		<div class="form-group">
           		<label>Start Date:</label> <br />

                        <select name="sday"  style="width: 25%;">
			<option value="<?php echo date('d'); ?>" selected> Day </option> 
                        <?php
                        $sql = "SELECT * FROM dates ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rd = pg_fetch_assoc($result)) {
                                        $name = $rd['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
		

                        <select name="smon"  style="width: 25%;">
			<option value="<?php echo date('m'); ?>" selected> Month  </option>
                        <?php
                        $sql = "SELECT * FROM months ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rm = pg_fetch_assoc($result)) {
                                        $name = $rm['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>


                        <select name="syy"  style="width: 30%;">
			<option value="<?php echo date('Y'); ?>" selected> Year </option>

                        <?php
                        $sql = "SELECT * FROM years ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($ry = pg_fetch_assoc($result)) {
                                        $name = $ry['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                  <!-- /.input group -->
            	</div>

		</td>
		<td>

                <div class="form-group">
                        <label> End Date:</label> <br />

                        <select name="eday"  style="width: 25%;">
			<option value="<?php echo date('d'); ?>" selected> Day </option> 
                        <?php
                        $sql = "SELECT * FROM dates ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rd = pg_fetch_assoc($result)) {
                                        $name = $rd['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
		

                        <select name="emon"  style="width: 25%;">
			<option value="<?php echo date('m'); ?>" selected> Month  </option>
                        <?php
                        $sql = "SELECT * FROM months ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rm = pg_fetch_assoc($result)) {
                                        $name = $rm['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>


                        <select name="eyy"  style="width: 30%;">
			<option value="<?php echo date('Y'); ?>" selected> Year </option>

                        <?php
                        $sql = "SELECT * FROM years ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($ry = pg_fetch_assoc($result)) {
                                        $name = $ry['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>

		</div>
		</td>

                  <td>

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Term of Office : </label>
                        <select name="core_management_term" class="form-control select2" style="width: 100%;" required>
			<option value="" selected> Click to select </option>
                        <?php
                        $sql = "SELECT * FROM assoc_term_office ORDER BY term_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['term_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                </td>

		</tr>
		</table>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="core_management" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>



      <!-- for root -->
      <div class="modal fade" id="exec<?php echo $association_code; ?>" role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-warning disabled color-palette">
              <h4 class="modal-title">Add Executive Committee Member </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../association/" method="post">
            <div class="modal-body">

            <table class="table table-bordered">
               <tr>

                  <td width="33%" >

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Salutation : </label>
                        <select name="exec_committee_salutation[]" class="select2" multiple="multiple" data-placeholder="Select a State"
                                data-dropdown-css-class="select2-purple" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM salutation ORDER BY salutation_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['salutation_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                </td>
                <td width="33%">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Name :</label>
                   	<input type="hidden" name="exec_committee_assoc" value="<?php echo $association_code; ?>" 
                   	class="form-control" >
                        <input type="text" name="exec_committee_member" class="form-control" placeholder="Enter here ..." required>
                    </div>

		  </td>
		  <td width="33%" >

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Title : </label>
                        <select name="exec_committee_title" class="form-control select2" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM title ORDER BY title_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['title_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
		</td>

		</tr>
		<tr>

		<td>

		<div class="form-group">
           		<label>Start Date:</label> <br />

                        <select name="sday"  style="width: 25%;">
			<option value="<?php echo date('d'); ?>" selected> Day </option> 
                        <?php
                        $sql = "SELECT * FROM dates ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rd = pg_fetch_assoc($result)) {
                                        $name = $rd['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
		

                        <select name="smon"  style="width: 25%;">
			<option value="<?php echo date('m'); ?>" selected> Month  </option>
                        <?php
                        $sql = "SELECT * FROM months ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rm = pg_fetch_assoc($result)) {
                                        $name = $rm['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>


                        <select name="syy"  style="width: 30%;">
			<option value="<?php echo date('Y'); ?>" selected> Year </option>

                        <?php
                        $sql = "SELECT * FROM years ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($ry = pg_fetch_assoc($result)) {
                                        $name = $ry['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                  <!-- /.input group -->
            	</div>

		</td>
		<td>

                <div class="form-group">
                        <label> End Date:</label> <br />

                        <select name="eday"  style="width: 25%;">
			<option value="<?php echo date('d'); ?>" selected> Day </option> 
                        <?php
                        $sql = "SELECT * FROM dates ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rd = pg_fetch_assoc($result)) {
                                        $name = $rd['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
		

                        <select name="emon"  style="width: 25%;">
			<option value="<?php echo date('m'); ?>" selected> Month  </option>
                        <?php
                        $sql = "SELECT * FROM months ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rm = pg_fetch_assoc($result)) {
                                        $name = $rm['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>


                        <select name="eyy"  style="width: 30%;">
			<option value="<?php echo date('Y'); ?>" selected> Year </option>

                        <?php
                        $sql = "SELECT * FROM years ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($ry = pg_fetch_assoc($result)) {
                                        $name = $ry['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>

		</div>
		</td>

                  <td >

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Term of Office : </label>
                        <select name="exec_committee_term" class="form-control select2" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM assoc_term_office ORDER BY term_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['term_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                </td>

                </tr>

		</table>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="exec_committee" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>



      <!-- for root -->
      <div class="modal fade" id="board<?php echo $association_code; ?>" role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-warning disabled color-palette">
              <h4 class="modal-title">Add Board of Trustees Member </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../association/" method="post">
            <div class="modal-body">

            <table class="table table-bordered">
               <tr>

                  <td width="33%" >

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Salutation : </label>
                        <select name="board_trustees_salutation[]" class="select2" multiple="multiple" data-placeholder="Select a State"
                                data-dropdown-css-class="select2-purple" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM salutation ORDER BY salutation_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['salutation_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                </td>

                <td width="33%">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Name :</label>
                   	<input type="hidden" name="board_trustees_assoc" value="<?php echo $association_code; ?>" 
                   	class="form-control" >
                        <input type="text" name="board_trustees_member" class="form-control" placeholder="Enter here ..." required>
                    </div>

		  </td>
		  <td width="33%" >

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Title : </label>
                        <select name="board_trustees_title" class="form-control select2" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM title ORDER BY title_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['title_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
		</td>
		</tr>
		<tr>
		<td>

		<div class="form-group">
           		<label>Start Date:</label> <br />

                        <select name="sday"  style="width: 25%;">
			<option value="<?php echo date('d'); ?>" selected> Day </option> 
                        <?php
                        $sql = "SELECT * FROM dates ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rd = pg_fetch_assoc($result)) {
                                        $name = $rd['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
		

                        <select name="smon"  style="width: 25%;">
			<option value="<?php echo date('m'); ?>" selected> Month  </option>
                        <?php
                        $sql = "SELECT * FROM months ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rm = pg_fetch_assoc($result)) {
                                        $name = $rm['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>


                        <select name="syy"  style="width: 30%;">
			<option value="<?php echo date('Y'); ?>" selected> Year </option>

                        <?php
                        $sql = "SELECT * FROM years ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($ry = pg_fetch_assoc($result)) {
                                        $name = $ry['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                  <!-- /.input group -->
            	</div>

		</td>
		<td>

                <div class="form-group">
                        <label> End Date:</label> <br />

                        <select name="eday"  style="width: 25%;">
			<option value="<?php echo date('d'); ?>" selected> Day </option> 
                        <?php
                        $sql = "SELECT * FROM dates ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rd = pg_fetch_assoc($result)) {
                                        $name = $rd['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
		

                        <select name="emon"  style="width: 25%;">
			<option value="<?php echo date('m'); ?>" selected> Month  </option>
                        <?php
                        $sql = "SELECT * FROM months ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($rm = pg_fetch_assoc($result)) {
                                        $name = $rm['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>


                        <select name="eyy"  style="width: 30%;">
			<option value="<?php echo date('Y'); ?>" selected> Year </option>

                        <?php
                        $sql = "SELECT * FROM years ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($ry = pg_fetch_assoc($result)) {
                                        $name = $ry['name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>

		</div>
		</td>

                  <td>

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Term of Office : </label>
                        <select name="board_trustees_term" class="form-control select2" style="width: 100%;" required>
                        <?php
                        $sql = "SELECT * FROM assoc_term_office ORDER BY term_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['term_name']; ?>
                                        <option value="<?php echo $name; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                </td>

                </tr>


		</table>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="board_trustees" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>













