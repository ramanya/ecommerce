    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Location Information </h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

                <?php
                        if(strlen($error) > 0){
                                echo '
                                        <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" 
                                        aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i> Alert!</h4>' .$error . '
                                        </div>';
                        }
                ?>



		<table id="cci" class="table table-bordered ">
              		<tr>
			<td>
			<?php
			   $tab_district = $_GET["tab_district"];
			   $tab_county = $_GET["tab_county"];
			   $tab_subcounty = $_GET["tab_subcounty"];
			   $tab_parish = $_GET["tab_parish"];
			   $tab_village = $_GET["tab_village"];
			?>
			<button class="tablink" onclick="openPage('root', this, '#04812F')"id="defaultOpen"> 
			   <i class="fas fa-circle nav-icon"></i>  Districts </button>
			<button class="tablink" onclick="openPage('Home', this, '#04812F')" <?php if ($tab_county == '1'){ echo 'id="defaultOpen"';} ?>>
			   <i class="fas fa-circle nav-icon"></i> Counties / Municipalities </button>
			<button class="tablink" onclick="openPage('News', this, '#04812F')" <?php if ($tab_subcounty == '1'){ echo 'id="defaultOpen"';} ?>> 
			   <i class="fas fa-circle nav-icon"></i> Sub Counties </button>
                        <button class="tablink" onclick="openPage('Contact', this, '#04812F')" <?php if ($tab_parish == '1'){ echo 'id="defaultOpen"';} ?>>
                           <i class="fas fa-circle nav-icon"></i>  Parishes </button>
			<button class="tablink" onclick="openPage('About', this, '#04812F')" 
			<?php if ($tab_village == '1'){ echo 'id="defaultOpen"';}else{echo 'id="defaultOpen"';} ?>> 
				<i class="fas fa-circle nav-icon"></i> Villages </button>

                        <div id="root" class="tabcontent">
                           <h3> Districts 
   	                      <button class='button button2' data-toggle='modal' data-target='#modal-district'>
                	         <i class="fas fa-circle nav-icon"> </i> Add District </button>
			   </h3>

              		   <table id="example1" class="table table-bordered table-striped">
 			      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Code</th>
                		 <th>District</th>
                		 <th>CreatedBy </th>
                		 <th>CreatedOn </th>
				 <th align='center'> Action </th>
                	       </tr>
                	       </thead>
                	       <tbody>
  			       <?php	
			          $root = "SELECT * FROM district ORDER BY district_name ASC LIMIT 20";
				  $res1 = pg_query($conn, $root);
				  if ($res1) {
    				     // output data of each row
				     $number = 1;
    				     while($row1 = pg_fetch_assoc($res1)) {
                                        (string) $id = $row1['district_code_link'];
                                        (string) $trash = $row1['district_trash'];
                                        (string) $district_code_link = $row1['district_code_link'];
       					echo "<tr>";
       					echo "<td>" . $number . "</td>";

					echo "<td>" . $row1['district_code'] . "</td>";
					echo "<td>" . $row1['district_name'] . "</td>";
		               		echo "<td>" . $row1['district_createdby'] . "</td>";
		               		echo "<td>" . $row1['district_createdon'] . "</td>";
					echo '<td width="26%">';
                                        echo ' <a class="button button" href="#edit'.$district_code_link.'" data-toggle="modal">
                                               <i class="fa fa-edit"></i> Edit</a>';

					if (!empty($name)){
					   echo ' <button class="button button2" title="Disable account!" 
					 	  onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						  <i class="fa fa-user"> </i> Disable </button> ';
					}else{
                                           echo ' <button class="button button2" title="Enable account!" 
                                                  onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                  <i class="fa fa-user-times"> </i> Enable </button> ';
				       }

                                      if (($trash == 'f')){
                                         echo ' <button class="button button5" title="Trash record!" 
                                                onclick="window.location.href ='."'".'district_trash.php?error='.$id."';".'"> 
                                                <i class="fa fa-trash"> </i> Trash </button> ';
                                      }else{
                                ?>
                                        <a href="district_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }

                                      include('edit_district.php');
                                      echo '</td>';
				      $number++;
                                      echo '</tr>';
                                    }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> No districts configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  	       }
                	      ?>
                	      </tbody>
                	      <tfoot>
                	      <tr>
              		      <tr>
                                 <th>Id</th>
                                 <th>Code </th>
                                 <th>District </th>
                                 <th>CreatedBy </th>
                                 <th>CreatedOn </th>
                                 <th align='center'> Action </th>
			      </tr>
                	      </tr>
                	      </tfoot>
              		   </table>
                        </div>

			<div id="Home" class="tabcontent">
                           <h3>Counties / Municiparities
	                      <button class='button button2' data-toggle='modal' data-target='#modal-county'>
                	         <i class='fa fa-user-secret'> </i> Add County / Municiparity </button>
			   </h3>
              		   <table id="privilege" class="table table-bordered table-striped">
                	      <thead>
              		         <tr>
                		    <th>Id</th>
                		    <th>District</th>
                		    <th>County</th>
                		    <th>Created By</th>
                		    <th>Created On</th>
				    <th align='center'> Action </th>
                		 </tr>
                		 </thead>
                		 <tbody>
  				 <?php	
				    $pr = "SELECT * FROM district, county 
					   WHERE district.district_code = county.district_code
					   ORDER BY county.county_name ASC LIMIT 20";
				    $rs = pg_query($conn, $pr);
				    if ($rs) {
    				       // output data of each row
				       $number = 1;
    				       	while($rw = pg_fetch_assoc($rs)) {
                                           (string) $id = $rw['county_code_link'];
                                           (string) $county_code_link = $rw['county_code_link'];
                                           (string) $trash = $rw['county_trash'];
       					   echo "<tr>";
                                           echo "<td>" . $number . "</td>";
					   echo "<td>" . $rw['district_name'] . "</td>";
					   echo "<td>" . $rw['county_name'] . "</td>";
					   echo "<td>" . $rw['county_createdby'] . "</td>";
					   echo "<td>" . $rw['county_createdon'] . "</td>";
					   echo "<td width='26%'>";
                                           echo ' <a class="button button" href="#edit'.$county_code_link.'" data-toggle="modal">
                                                  <i class="fa fa-edit"></i> Edit</a>';

					   if ($is_active == 't'){
                                              echo ' <button class="button button2" title="Disable account!" 
					             onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						     <i class="fa fa-user"> </i> Disable </button> ';
					   }else{
                                              echo ' <button class="button button4" title="Enable account!" 
                                                     onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                     <i class="fa fa-user-times"> </i> Enable </button> ';
					  }

                                          if ($trash == 'f'){
                                             echo ' <button class="button button5" title="Trash record!" 
                                                    onclick="window.location.href ='."'".'county_trash.php?error='.$id."';".'"> 
                                                    <i class="fa fa-trash"> </i> Trash </button> ';
                                          }else{
                                ?>
                                        <a href="county_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }
                                         include('edit_county.php');
                                         echo '</td>';

                                         $number++;
                                         echo '</tr>';
                                      }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> No Counties configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  		}
                		?>
                		</tbody>
                		<tfoot>
                		<tr>
                		<tr>
                        	   <th>Id</th>
                		   <th> District </th>
                		   <th> County </th>
                                   <th> Created By</th>
                                   <th> Created On</th>
                        	   <th align='center'> Action </th>
                                </tr>
                	        </tr>
                	        </tfoot>
              		     </table>
			</div>


			<div id="News" class="tabcontent">

                           <h3> Sub County 
			      <button class='button button2' data-toggle='modal' data-target='#modal-subcounty'>
                	         <i class='fa fa-user-secret'> </i> Add Sub County </button>
			   </h3>

			   <table id="role" class="table table-bordered table-striped">
                	      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>District</th>
                		 <th>County</th>
                		 <th>Subcounty</th>
				 <th align='center'> Action </th>
                	      </tr>
                	      </thead>
                	      <tbody>

			      <?php	
			         $roles = "SELECT * FROM district,county,sub_county
					   WHERE district.district_code = county.district_code
					   AND county.county_code = sub_county.county_code
					   ORDER BY sub_county.id DESC LIMIT 20";
				 $result_role = pg_query($conn, $roles);
				 if ($result_role) {
    				   // output data of each row
				   $number = 1;
    				   while($row_role = pg_fetch_assoc($result_role)) {
                                      (string) $id = $row_role['sub_county_code_link'];
                                      (string) $sub_county_code_link = $row_role['sub_county_code_link'];
                                      (string) $trash = $row_role['sub_county_trash'];

                                      echo "<tr>";
                                      echo "<td>" . $number . "</td>";
                                      echo "<td>" . $row_role['district_name'] . "</td>";
				      echo "<td>" . $row_role['county_name'] . "</td>";
				      echo "<td>" . $row_role['sub_county_name'] . "</td>";

				      echo "<td width='26%'>";
                                      echo  ' <a class="button button" href="#edit'.$sub_county_code_link.'" data-toggle="modal">
                                              <i class="fa fa-edit"></i> Edit</a>';

               			      if ($is_active == 't'){
                                         echo ' <button class="button button2" title="Disable account!" 
						onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						<i class="fa fa-user"> </i> Disable </button> ';
				      }else{
                                         echo ' <button class="button button4" title="Enable account!" 
                                                onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                <i class="fa fa-user-times"> </i> Enable </button> ';
				     }

                                     if ($trash == 'f'){
                                        echo ' <button class="button button5" title="Trash record!" 
                                               onclick="window.location.href ='."'".'subcounty_trash.php?error='.$id."';".'"> 
                                               <i class="fa fa-trash"> </i> Trash </button> ';
                                     }else{
                                ?>
                                        <a href="subcounty_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }
				    
                                     include('edit_subcounty.php');
				     echo '</td>';
				     $number++;
                                     echo '</tr>';
                                  }
                               } else {
                                  echo "<tr>";
                                  echo "<td colspan='6'> No subcounties configured in the system yet! </td>";
                                  echo "</tr>";
                                  echo "<tr>";
                                  echo "<td colspan='6' align='right'>";
                                  echo "</td>";
                                  echo "</tr>";
                  	       }
                	    ?>
                	    </tbody>
                	    <tfoot>
                	    <tr>
                	    <tr>
                                 <th>Id</th>
                                 <th>District</th>
                                 <th>County</th>
                                 <th>Subcounty</th>
                                 <th align='center'> Action </th>
                	    </tr>
                	    </tr>
                	    </tfoot>
              		 </table>
	              </div>
		
		      <div id="Contact" class="tabcontent">

                           <h3>Parishes 
   	                      <button class='button button2' data-toggle='modal' data-target='#modal-parish'>
                	         <i class='fa fa-plus'> </i> Add Parish </button>
			   </h3>

              		   <table id="parishes" class="table table-bordered table-striped">
 			      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Name </th>
                		 <th>Created By </th>
                		 <th>Created On </th>
				 <th align='center'> Action </th>
                	       </tr>
                	       </thead>
                	       <tbody>
  			       <?php	
			          $root = "SELECT parish_name,parish_createdon,parish_trash,
						parish_createdby,parish_code_link FROM parish 
					   ORDER BY id DESC LIMIT 20"; 
				  $res1 = pg_query($conn, $root);
				  if ($res1) {
    				     // output data of each row
				     $number = 1;
    				     while($row1 = pg_fetch_assoc($res1)) {
                                        (string) $id = $row1['parish_code_link'];
                                        (string) $trash = $row1['parish_trash'];
                                        (string) $parish_code_link = $row1['parish_code_link'];
       					echo "<tr>";
       					echo "<td>" . $number . "</td>";

					echo "<td>" . $row1['parish_name'] . "</td>";
					echo "<td>" . $row1['parish_createdby'] . "</td>";
					echo "<td>" . $row1['parish_createdon'] . "</td>";
					echo '<td>';
                                        echo ' <a class="button button" href="#edit'.$parish_code_link.'" data-toggle="modal">
                                               <i class="fa fa-edit"></i> Edit</a>';

					if ($is_active == 't'){
					   echo ' <button class="button button2" title="Disable account!" 
					 	  onclick="window.location.href ='."'".'disable.php?error='.$id."';".'"> 
						  <i class="fa fa-user"> </i> Disable </button> ';
					}else{
                                           echo ' <button class="button button4" title="Enable account!" 
                                                  onclick="window.location.href ='."'".'enable.php?error='.$id."';".'"> 
                                                  <i class="fa fa-user-times"> </i> Enable </button> ';
				       }

                                      if ($trash == 'f'){
                                         echo ' <button class="button button5" title="Trash record!" 
                                                onclick="window.location.href ='."'".'parish_trash.php?error='.$id."';".'"> 
                                                <i class="fa fa-trash"> </i> Trash </button> ';
                                      }else{
				?>
                                        <a href="parish_delete.php?error=<?php echo $id;?>" 
						onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
				<?php


                                      }

                                      include('edit_parish.php');
                                      echo '</td>';
				      $number++;
                                      echo '</tr>';
                                    }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> No parishes configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  	       }
                	      ?>
                	      </tbody>
                	      <tfoot>
                	      <tr>
              		      <tr>
                                 <th>Id</th>
                                 <th>Name </th>
                                 <th>Created By </th>
                                 <th>Created On </th>
                                 <th align='center'> Action </th>
			      </tr>
                	      </tr>
                	      </tfoot>
              		   </table>


		      </div>

		     <div id="About" class="tabcontent">

                           <h3> Villages  
   	                      <button class='button button2' data-toggle='modal' data-target='#modal-village'>
                	         <i class='fa fa-plus'> </i> Add Village </button>
			   </h3>

              		   <table id="villages" class="table table-bordered table-striped">
 			      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Name </th>
                		 <th>Created By </th>
                		 <th>Created On </th>
				 <th align='center'> Action </th>
                	       </tr>
                	       </thead>
                	       <tbody>
  			       <?php	
			          $root = "SELECT * FROM village ORDER BY id DESC LIMIT 30"; 
				  $res1 = pg_query($conn, $root);
				  if ($res1) {
    				     // output data of each row
				     $number = 1;
    				     while($row1 = pg_fetch_assoc($res1)) {
                                        (string) $id = $row1['village_code_link'];
                                        (string) $trash = $row1['village_trash'];
                                        (string) $village_code_link = $row1['village_code_link'];
       					echo "<tr>";
       					echo "<td>" . $number . "</td>";

					echo "<td>" . $row1['village_name'] . "</td>";
					echo "<td>" . $row1['village_createdby'] . "</td>";
					echo "<td>" . $row1['village_createdon'] . "</td>";
					echo '<td>';
                                        echo ' <a class="button button" href="#edit'.$village_code_link.'" data-toggle="modal">
                                               <i class="fa fa-edit"></i> Edit</a>';

					if ($is_active == 't'){
					   echo ' <button class="button button2" title="Disable account!" 
					 	  onclick="window.location.href ='."'".'disable.php?error='.$id."';".'"> 
						  <i class="fa fa-user"> </i> Disable </button> ';
					}else{
                                           echo ' <button class="button button4" title="Enable account!" 
                                                  onclick="window.location.href ='."'".'enable.php?error='.$id."';".'"> 
                                                  <i class="fa fa-user-times"> </i> Enable </button> ';
				       }

                                      if ($trash == 'f'){
                                         echo ' <button class="button button5" title="Trash record!" 
                                                onclick="window.location.href ='."'".'village_trash.php?error='.$id."';".'"> 
                                                <i class="fa fa-trash"> </i> Trash </button> ';
                                      }else{
				?>
                                        <a href="village_delete.php?error=<?php echo $id;?>" 
						onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
				<?php


                                      }

                                      include('edit_village.php');
                                      echo '</td>';
				      $number++;
                                      echo '</tr>';
                                    }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> No villages configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  	       }
                	      ?>
                	      </tbody>
                	      <tfoot>
                	      <tr>
              		      <tr>
                                 <th>Id</th>
                                 <th>Name </th>
                                 <th>Created By </th>
                                 <th>Created On </th>
                                 <th align='center'> Action </th>
			      </tr>
                	      </tr>
                	      </tfoot>
              		   </table>
		     </div>
		</td>
             </tr>
         </table>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          User Account Management
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


      <!-- for department -->
      <div class="modal fade" id="modal-village">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Village  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../location-information/" method="post">
            <div class="modal-body">

                <div class="form-group">
                   <label for="exampleInputPassword1"> Parish Name :</label>
                   <select name="parish_code" class="form-control select2" style="width: 100%;">
		      <option value="" selected> --- Select State --- </option>
                      <?php
                         $sql = "SELECT * FROM parish ORDER BY parish_name ASC";
                         $result = pg_query($conn, $sql);
                         if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $code = $row['parish_code'];
                                        $name = $row['parish_name']; ?>
                                        <option value="<?php echo $code; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>

                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Village Name : </label>
                        <input type="text" name="village_name" class="form-control" placeholder="Enter village name " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="village" class="btn btn-primary">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



      <!-- for root -->
      <div class="modal fade" id="modal-parish">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Parish  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../location-information/" method="post">

            <div class="modal-body">
                <div class="form-group">
                   <label for="exampleInputPassword1"> Sub County :</label>
                   <select name="sub_county_code" class="form-control select2" style="width: 100%;">
                      <option value="" selected> --- Select State --- </option>
                      <?php
                         $sql = "SELECT * FROM sub_county ORDER BY sub_county_name ASC";
                         $result = pg_query($conn, $sql);
                         if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $code = $row['sub_county_code'];
                                        $name = $row['sub_county_name']; ?>
                                        <option value="<?php echo $code; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>
      
                        </select>

                </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Parish Name : </label>
                        <input type="text" name="parish_name" class="form-control" placeholder="Enter parish name " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="parish" class="btn btn-primary">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->




      <!-- for root -->
      <div class="modal fade" id="modal-district">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add New District  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../location-information/" method="post">
            <div class="modal-body">
              
                    <div class="form-group">
                        <label for="exampleInputEmail1">District Name : </label>
                        <input type="text" name="district_name" class="form-control" placeholder="Enter district name" required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="district" class="btn btn-primary">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->




  <!-- /.content-wrapper -->

        <!-- Begin form for adding privilege -->
        <div class="modal fade" id="modal-county">
          <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                   <h4 class="modal-title">Add County / Municiparity </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

                <form action="../location-information/" method="post">
                <div class="box-body">

                <div class="form-group">
        	   <label for="exampleInputPassword1"> District :</label>
	           <select name="district" class="form-control select2" style="width: 100%;">
                      <?php
                         $sql = "SELECT * FROM district ORDER BY district_name ASC";
                         $result = pg_query($conn, $sql);
                         if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $code = $row['district_code']; 
                                        $name = $row['district_name']; ?>
                                        <option value="<?php echo $code; ?> "<?php
                                        if ($day == $row['name'])
                                                { echo 'selected'; } ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>

		    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">County / Municiparity Name : </label>
                        <input type="text" name="county_name" class="form-control" placeholder="Enter county name " required>
                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="county" class="btn btn-primary">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user privilege -->

        <!-- Begin form for adding religion -->
        <div class="modal fade" id="modal-subcounty">
          <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                   <h4 class="modal-title">Add Sub County </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

                <form action="../location-information/" method="post">
                <div class="box-body">

                <div class="form-group">
                   <label for="exampleInputPassword1"> County Name :</label>
                   <select name="county_code" class="form-control select2" style="width: 100%;">
		      <option value="" selected>--- Select State ---</option>
                      <?php
                         $sql = "SELECT * FROM county ORDER BY county_name ASC";
                         $result = pg_query($conn, $sql);
                         if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $code = $row['county_code'];
                                        $name = $row['county_name']; ?>
                                        <option value="<?php echo $code; ?> ">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>

                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Sub County Name : </label>
                        <input type="text" name="sub_county_name" class="form-control" placeholder="Enter sub county name" required>
                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="subcounty" class="btn btn-primary">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user privilege -->


<!-- Java Scripts for tabs on enrollment   -->
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#privilege").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#role").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#syslog").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#parishes").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#villages").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

