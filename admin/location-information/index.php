<?php
	session_start();
	require_once'../library/config.php';
 	logout_session($conn); //Check wheather user logged in
	auto_logout($conn); //Logout iddle user
        $GLOBALS['app'] = "settings";
        $GLOBALS['nav'] = "location";
        $GLOBALS['error'] = "";

        # Processing village
        if(isset($_POST['village'])){
                $village_code = randomString();
                $parish_code = test_input($_POST['parish_code']);
                $village_name = test_input($_POST['village_name']);
                $link = sha1($village_code);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM village 
                        WHERE parish_code = '$parish_code'
                        AND village_name = $village_name";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The Village name " .$village_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO village (parish_code,village_code,village_name,village_createdby,village_code_link)
                                VALUES ('$parish_code','$village_code','$village_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The village name. ".$village_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_village='.$tab.'"</script>';
                }
        }

        # Processing editing village 
        if(isset($_POST['village_update'])){
                $parish_code = test_input($_POST['parish_code']);
                $village_name = test_input($_POST['village_name']);
                $link = test_input($_POST['village_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM village
                        WHERE parish_code = '$parish_code' 
                        AND village_name = '$village_name'
                        AND village_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE village SET parish_code = '$parish_code',
                                village_name = '$village_name',
                                village_createdon = now(),
                                village_createdby = '$user'
                                WHERE village_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The village name ".$village_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_village='.$tab.'"</script>';
                }
        }

        # Processing parish
        if(isset($_POST['parish'])){
                $parish_code = randomString();
                $sub_county_code = test_input($_POST['sub_county_code']);
                $parish_name = test_input($_POST['parish_name']);
                $link = sha1($parish_code);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM parish 
                        WHERE sub_county_code = '$sub_county_code'
                        AND parish_name = $parish_name";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The Parish name " .$parish_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO parish (parish_code,sub_county_code,parish_name,parish_createdby,parish_code_link)
                                VALUES ('$parish_code','$sub_county_code','$parish_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The parish name. ".$parish_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_parish='.$tab.'"</script>';
                }
        }

        # Processing editing parish 
        if(isset($_POST['parish_update'])){
                $sub_county_code = test_input($_POST['sub_county_code']);
                $parish_name = test_input($_POST['parish_name']);
                $link = test_input($_POST['parish_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM parish
                        WHERE sub_county_code = '$sub_county_code' 
                        AND parish_name = '$parish_name'
                        AND parish_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE parish SET sub_county_code = '$sub_county_code',
                                parish_name = '$parish_name',
                                parish_createdon = now(),
                                parish_createdby = '$user'
                                WHERE parish_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The parish name ".$parish_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_parish='.$tab.'"</script>';
                }
        }

        # Processing sub county
        if(isset($_POST['subcounty'])){
                $subcounty_code = randomString();
                $sub_county_name = test_input($_POST['sub_county_name']);
                $county_code = test_input($_POST['county_code']);
                $link = sha1($subcounty_code);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM sub_county 
                        WHERE county_code = '$county_code'
                        AND sub_county_name = $sub_county_name";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The Sub County name " .$sub_county_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO sub_county (county_code,sub_county_code,sub_county_name,sub_county_createdby,sub_county_code_link)
                                VALUES ('$county_code','$subcounty_code','$sub_county_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The Sub County name. ".$sub_county_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_subcounty='.$tab.'"</script>';
                }
        }

        # Processing editing sub county 
        if(isset($_POST['subcounty_update'])){
                $county_code = test_input($_POST['county_code']);
                $sub_county_name = test_input($_POST['sub_county_name']);
                $link = test_input($_POST['sub_county_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM sub_county
                        WHERE county_code = '$county_code' 
                        AND sub_county_name = '$sub_county_name'
                        AND sub_county_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE sub_county SET county_code = '$county_code',
                                sub_county_name = '$sub_county_name',
                                sub_county_lastupdateon = now(),
                                sub_county_lastupdateby = '$user'
                                WHERE sub_county_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The sub county name ".$sub_county_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_subcounty='.$tab.'"</script>';
                }
        }

        # Processing county
        if(isset($_POST['county'])){
                $county_code = randomString();
                $district = test_input($_POST['district']);
                $county_name = test_input($_POST['county_name']);
                $link = sha1($county_code);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM county 
			WHERE district_code = '$district'
			AND county_name = $county_name";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The county name " .$county_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO county (county_code,district_code,county_name,county_createdby,county_code_link)
                                VALUES ('$county_code','$district','$county_name.','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The county name. ".$county_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_county='.$tab.'"</script>';
                }
        }

        # Processing editing county 
        if(isset($_POST['update_county'])){
                $district = test_input($_POST['district']);
                $county_name = test_input($_POST['county_name']);
                $link = test_input($_POST['county_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM county
                        WHERE district_code = '$district' 
			AND county_name = '$county_name'
			AND county_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE county SET district_code = '$district',
				county_name = '$county_name',
                                county_lastupdatedon = now(),
                                county_lastupdatedby = '$user'
                                WHERE county_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The county name ".$county_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_county='.$tab.'"</script>';
                }
        }


        # Processing district
        if(isset($_POST['district'])){
		$district_code = randomString(); 
                $district_name = test_input($_POST['district_name']);
                $link = sha1($district_name);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM district WHERE district_name = '$district_name'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The district name " .$district_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO district (district_code,district_name,district_createdby,district_code_link)
                                VALUES ('$district_code','$district_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The district name ".$district_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_district='.$tab.'"</script>';
                }
        }

        # Processing editing district 
        if(isset($_POST['update_distict'])){
                $district_name = test_input($_POST['district_name']);
                $link = test_input($_POST['district_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM district
                        WHERE district_name = '$district_name' AND district_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE district SET district_name = '$district_name',
                                district_lastupdatedon = now(),
                                district_lastupdatedby = '$user'
                                WHERE district_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The district name ".$district_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../location-information/?tab_district='.$tab.'"</script>';
                }
        }

        # Processing department
        if(isset($_POST['department'])){
                $department_name = test_input($_POST['department_name']);
                $link = sha1($department_name);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM department WHERE department_name = '$department_name'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The department name " .$department_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO department (department_name,department_createdby,department_code_link)
                                VALUES ('$department_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The department name ".$department_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_department='.$tab.'"</script>';
                }
        }

        # Processing editing disability 
        if(isset($_POST['update_department'])){
                $department_name = test_input($_POST['department_name']);
                $link = test_input($_POST['department_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM department
                        WHERE department_name = '$department_name' AND department_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE department SET department_name = '$department_name',
                                department_lastupdatedon = now(),
                                department_lastupdatedby = '$user'
                                WHERE department_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The department name ".$department_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_department='.$tab.'"</script>';
                }
        }

        # Processing disability
        if(isset($_POST['disability'])){
                $disability_name = test_input($_POST['disability_name']);
                $link = sha1($disability_name);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM diasability WHERE disability_name = '$disability_name'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The disability name " .$disability_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO disability (disability_name,disability_createdby,disability_code_link)
                                VALUES ('$disability_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The disability name ".$disability_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
                }
        }

        # Processing editing disability 
        if(isset($_POST['update_disability'])){
                $disability_name = test_input($_POST['disability_name']);
                $link = test_input($_POST['disability_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM disability
                        WHERE disability_name = '$disability_name' AND disability_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE disability SET disability_name = '$disability_name',
                                disability_lastupdatedon = now(),
                                disability_lastupdatedby = '$user'
                                WHERE disability_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The disability name ".$disability_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
                }
        }

        # Processing Religion
        if(isset($_POST['religion'])){
                $religion_name = test_input($_POST['religion_name']);
                $link = sha1($religion_name);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM religion WHERE religion_name = '$religion_name'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The religion name " .$tribe_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO religion (religion_name,religion_createdby,religion_code_link)
                                VALUES ('$religion_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The religion name ".$religion_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_religion='.$tab.'"</script>';
                }
        }

        # Processing editing religion 
        if(isset($_POST['update_religion'])){
                $religion_name = test_input($_POST['religion_name']);
                $link = test_input($_POST['religion_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM religion
                        WHERE religion_name = '$religion_name' AND religion_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE religion SET religion_name = '$religion_name',
                                religion_lastupdatedon = now(),
                                religion_lastupdatedby = '$user'
                                WHERE religion_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The religion name ".$religion_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_religion='.$tab.'"</script>';
                }
        }


        # Processing Tribe
        if(isset($_POST['tribe'])){
                $tribe_name = test_input($_POST['tribe_name']);
                $link = sha1($tribe_name);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM tribe WHERE tribe_name = '$tribe_name'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The tribe name " .$tribe_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO tribe (tribe_name,tribe_createdby,tribe_code_link)
                                VALUES ('$tribe_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The tribe name ".$tribe_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_tribe='.$tab.'"</script>';
                }
        }

        # Processing editing tribe 
        if(isset($_POST['update_tribe'])){
                $tribe_name = test_input($_POST['tribe_name']);
                $link = test_input($_POST['tribe_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM tribe 
                        WHERE tribe_name = '$tribe_name' AND tribe_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE tribe SET tribe_name = '$tribe_name',
                                tribe_lastupdatedon = now(),
                                tribe_lastupdatedby = '$user'
                                WHERE tribe_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The tribe name ".$tribe_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_tribe='.$tab.'"</script>';
                }
        }

        # Processing Nationality
        if(isset($_POST['nation'])){
        	$nationality = test_input($_POST['nationality']);
                $link = sha1($nationality);
                $user = test_input($_SESSION['user']);
		$ip = ip_add();
		
                $sql = "SELECT * FROM nationality WHERE name = '$nationality'"; 
                $result = pg_query($conn,$sql);
		$row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
       			$type = "-- warning --";
                        $GLOBALS['error'] = "The nationality of " .$nationality. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
		}else{
                	$sql = "INSERT INTO nationality (name,nationality_createdby,nationality_url)
                        	VALUES ('$nationality','$user','$link')";
                	$result = pg_query($conn,$sql);
			$tab = '1';
                       	$type = "-- Normal --";
                       	$GLOBALS['error'] = "The nationality for the Country ".$nationality." has been added ";
                       	$msg = $error;
                      	syslogs($conn,$ip,$msg,$user,$type);
                       	echo '<script>window.location="../master-data-settings/?tab_nationality='.$tab.'"</script>';
                }
        }

        # Processing editing nationality
        if(isset($_POST['update_nationality'])){
                $name = test_input($_POST['name']);
                $link = test_input($_POST['nationality_url']);
                $url = sha1($link);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM nationality 
			WHERE name = '$name' AND id = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE nationality SET name = '$name',
				nationality_lastupdateon = now(),
				nationality_lastupdatedby = '$user',
				nationality_url = '$url'
				WHERE id = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The nationality ".$nationality." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_nationality='.$tab.'"</script>';
                }
        }

 	require_once'../header.php';
 	require_once'../navigation.php';
 	include('body.php');
 	require_once'../footer.php';
?>

