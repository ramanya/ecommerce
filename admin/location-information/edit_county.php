
        <!-- Begin form for adding user -->
        <div class="modal fade" id="edit<?php echo $county_code_link; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
            	<div class="modal-header">
              	   <h4 class="modal-title">Editing County / Municiparity </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM county WHERE county_code_link ='".$county_code_link."'");
                        $erow=pg_fetch_array($edit);
			(string) $district = $erow['district_code'];
           	?>


                <form method="POST" action="../location-information/">
                <div class="box-body">

                <div class="form-group">
                   <label for="exampleInputPassword1"> District :</label>
                   <select name="district" class="form-control select2" style="width: 100%;">
                      <?php
                         $sql = "SELECT * FROM district ORDER BY district_name ASC";
                         $result = pg_query($conn, $sql);
                         if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $code = $row['district_code'];
                                        $name = $row['district_name']; ?>
                                        <option value="<?php echo $code; ?> "<?php
                                        if ($district == $row['district_code'])
                                                { echo 'selected'; } ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>

                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1"> County / Municiparity :</label>

                        <input  type="hidden" name="county_code_link" class="form-control" value="<?php echo $erow['county_code_link']; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                        <input 	type="text" name="county_name" class="form-control" value="<?php echo $erow['county_name']; ?>" 
				id="inputdefault" style="width: 100%;"  required>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="update_county" class="btn btn-primary">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->
