
        <!-- Begin form for adding user -->
        <div class="modal fade" id="edit<?php echo $sub_county_code_link; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
            	<div class="modal-header">
              	   <h4 class="modal-title">Editing Sub County </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM sub_county WHERE sub_county_code_link ='".$sub_county_code_link."'");
                        $erow=pg_fetch_array($edit);
			$county_code = $erow['county_code'];
           	?>

                <form method="POST" action="../location-information/">
                <div class="box-body">

                <div class="form-group">
                   <label for="exampleInputPassword1"> County Name :</label>
                   <select name="county_code" class="form-control select2" style="width: 100%;">
                      <?php
                         $sql = "SELECT * FROM county ORDER BY county_name ASC";
                         $result = pg_query($conn, $sql);
                         if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $code = $row['county_code'];
                                        $name = $row['county_name']; ?>
                                        <option value="<?php echo $code; ?> "<?php
                                        if ($county_code == $row['county_code'])
                                                { echo 'selected'; } ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>

                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Sub County Name :</label>

                        <input  type="hidden" name="sub_county_code_link" class="form-control" value="<?php echo $erow['sub_county_code_link']; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                        <input 	type="text" name="sub_county_name" class="form-control" value="<?php echo $erow['sub_county_name']; ?>" 
				id="inputdefault" style="width: 100%;"  required>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="subcounty_update" class="btn btn-primary">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->
