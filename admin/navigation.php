  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-info elevation-4">
    <!-- Brand Logo -->
    <a href="../home/" class="brand-link">
      <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"> <b> CKil </b>  </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../dist/img/gem-icon.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="../home/" class="d-block"> <b> MAIN NAVIGATION </b> </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item has-treeview <?php if (!empty($nav) && ($app == 'dashboard')){ echo 'menu-open';} ?>">
            <a href="#" class="nav-link <?php if ($app == 'dashboard'){ echo 'active';} ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../home/" class="nav-link <?php if ($nav == 'home'){ echo 'active';} ?>">
                  <i class="nav-icon fas fa-chart-pie"></i>
                  <p> Quick Stats & Reports </p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item has-treeview <?php if (!empty($nav) && ($app == 'settings')){ echo 'menu-open';} ?>">
            <a href="#" class="nav-link <?php if ($app == 'settings'){ echo 'active';} ?>">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                cPanel Settings
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">

        <?php
                $apps = $_SESSION['applications'];
		//Check if user has access to account Manager
                if (strstr($apps,'account-management')){
        ?>
              <li class="nav-item">
                <a href="../account-management/" class="nav-link <?php if ($nav == 'account'){ echo 'active';} ?>">
              	  <i class="far fa-dot-circle nav-icon"></i>
                  <p>User Account Management </p>
                </a>
              </li>

        <?php   } 
		//Check if user has access to Master Data
                if (strstr($apps,'master-data-settings')){
        ?>
              <li class="nav-item">
                <a href="../master-data-settings/" class="nav-link <?php if ($nav == 'master'){ echo 'active';} ?>">
              	  <i class="far fa-dot-circle nav-icon"></i>
                  <p>Master Data </p>
                </a>
              </li>
        <?php   } 
		//Check if user has access to location data
		if (strstr($apps,'location-information')){	
	?>

              <li class="nav-item">
                <a href="../location-information/" class="nav-link <?php if ($nav == 'location'){ echo 'active';} ?>">
              	  <i class="far fa-dot-circle nav-icon"></i>
                  <p>Location Information </p>
                </a>
              </li>

	<?php } ?>

            </ul>
          </li>




          <li class="nav-item has-treeview <?php if (!empty($nav) && ($app == 'slider')){ echo 'menu-open';} ?>">
            <a href="#" class="nav-link <?php if ($app == 'slider'){ echo 'active';} ?>">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Site Content
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">

        <?php
                $apps = $_SESSION['applications'];
		//Check if user has access to account Manager
                if (strstr($apps,'slider')){
        ?>
              <li class="nav-item">
                <a href="../slider/" class="nav-link <?php if ($nav == 'slider'){ echo 'active';} ?>">
              	  <i class="far fa-dot-circle nav-icon"></i>
                  <p> Slider </p>
                </a>
              </li>

        <?php   } 
		//Check if user has access to Master Data
                if (strstr($apps,'products')){
        ?>
              <li class="nav-item">
                <a href="../products/" class="nav-link <?php if ($nav == 'products'){ echo 'active';} ?>">
              	  <i class="far fa-dot-circle nav-icon"></i>
                  <p> Product  Management  </p>
                </a>
              </li>
        <?php   } 
                if (strstr($apps,'orders')){
        ?>
              <li class="nav-item">
                <a href="../orders/" class="nav-link <?php if ($nav == 'orders'){ echo 'active';} ?>">
                  <i class="far fa-dot-circle nav-icon"></i>
                  <p> Orders Management </p>
                </a>
              </li>
        <?php   }
        ?>

            </ul>
          </li>




        

        </ul>



      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
