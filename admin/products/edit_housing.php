

        <!-- Begin to view Religious -->
        <div class="modal fade" id="view<?php echo $product_code_link;?>" tabindex="-1" role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
          <div class="modal-dialog modal-xl">
            <div class="modal-content bg-default">
            	<div class="modal-header bg-info disabled color-palette">
              	   <h4 class="modal-title">Product Details </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              	    $edit=pg_query($conn,"SELECT * FROM product WHERE product_code_link ='".$product_code_link."'");
                    $urow=pg_fetch_array($edit);
                    (string) $GLOBALS['product_photo'] = $urow['product_photo'];
                    (string) $GLOBALS['status'] = $urow['product_availability'];
           	    ?>

                <?php
                    //For alert messages
                    if(strlen($error) > 0){
                        echo '  <div class="alert alert-success" role="alert" data-auto-dismiss="3000">
                            '.$error.'
                        </div>';
                    }
                ?>

                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered">
            	    <tr>
			            <td width="50%" align="center">
			                <!-- for the org logo -->
                            <?php   
				                if (!empty($product_photo)){
                                    echo '  <img src="images/'.$product_photo.'" alt="User Image"
                                        height="350" width="350" > <br /> ';
					                echo '<b> <i> Product photo </i> </b> <br />';
                                }
                            ?>


			            </td>
			            <td>
                    	    <label> Product Details   </label>
				            <hr />
                            <label> Code :  </label>
                            <?php echo $urow['product_code']; ?> 
                            <br />	
                            <label > Status :</label>
                            <?php if ($status == 't'){echo "Available";}else{echo "Not Available";} ?> 
			                <br />
                            <label> Product Name :  </label>
                            <?php echo $urow['product_name']; ?>
                            <br />
                    		<label> Product Description : </label>
                    		<?php echo $urow['product_description']; ?> 
                            <br />
                            <label > Product Price  :</label>
                            <?php echo $urow['product_price']; ?>
                            <br />
                            <label > Published by :</label>
                            <?php echo $urow['product_createdby']; ?>
                             <br/>
                            <label > Last updated on :</label>

                            <?php echo $urow['product_lastupdatedon']; ?>
                            <br /> 
                             <label > Last updated By :</label>

                            <?php echo $urow['product_lastupdatedby']; ?>
                  	    </td>
                    </tr>
		            <tr>
                </table>
       	</div> <!-- end of the table -->

       	</div>
       		<div class="modal-footer justify-content-between">
       		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    <?php
        	echo '<button title="Click to print." class="btn btn-default" 
                      onclick="window.open('."'".'Printing.php?error='.$product_code_link."','_blank','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=400')".'" > <i class="fa fa-print"></i> Print </button>  ';
	    ?>

       	 	</div>
        </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


        <!-- Begin form for uploading photos -->
        <div class="modal fade" id="upload<?php echo $product_code_link;?>" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
            	<div class="modal-header bg-info disabled color-palette">
              	   <h4 class="modal-title"> Upload more photos for the product </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
                <div class="modal-body">

                <?php
                    $edit=pg_query($conn,"SELECT * FROM product WHERE product_code_link ='".$product_code_link."'");
                    $urow=pg_fetch_array($edit);
			        (string) $GLOBALS['product_photo'] = $urow['product_photo'];
           	    ?>

                <form action="../products/" method="post" enctype="multipart/form-data">
                <div class="box-body">
                    <b class="text-sm"> product <?php echo $urow['product_code']." ".$urow['product_name']." ::: 
                         ".$urow['product_price']; ?> you can upload maximum 6 more photos! </b>
            
            <table class="table table-bordered mb-0">
	         <tr>
                <td width="50%">
                    <div class="form-group">
                        <input type="hidden" maxlength="50" name="product_code_link" class="form-control"
                         value="<?php echo $urow['product_code_link']; ?>" required>

                        <label for="exampleInputEmail1"> Photo 1 :</label>
                        <input name="housing_photo1" type="file" id="exampleInputFile" required>
                    </div>
                </td>

                <td width="50%" >
                    <div class="form-group">
                        <input type="text" maxlength="100" name="housing_photo1_cap" class="form-control"
                        placeholder="Enter Caption here ..." required>
                    </div>
                </td>

            </tr>

             <tr>
                <td width="50%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Photo 2 :</label>
                        <input name="housing_photo2" type="file" id="exampleInputFile" required>
                    </div>
                </td>

                <td width="50%" >
                    <div class="form-group">
                        <input type="text" maxlength="100" name="housing_photo2_cap" class="form-control"
                        placeholder="Enter Caption here ..."  required>
                    </div>
                </td>

            </tr>

             <tr>
                <td width="50%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Photo 3 :</label>
                        <input name="housing_photo3" type="file" id="exampleInputFile" >
                    </div>
                </td>
                <td width="50%" >
                    <div class="form-group">
                        <input type="text" maxlength="100" name="housing_photo3_cap" class="form-control"
                        placeholder="Enter Caption here ..." >
                    </div>
                </td>
            </tr>

             <tr>
                <td width="50%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Photo 4 :</label>
                        <input name="housing_photo4" type="file" id="exampleInputFile" >
                    </div>
                </td>
                <td width="50%" >
                    <div class="form-group">
                        <input type="text" maxlength="100" name="housing_photo4_cap" class="form-control"
                        placeholder="Enter Caption here ..." >
                    </div>
                </td>
            </tr>

             <tr>
                <td width="50%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Photo 5 :</label>
                        <input name="housing_photo5" type="file" id="exampleInputFile" >
                    </div>
                </td>
                <td width="50%" >
                    <div class="form-group">
                        <input type="text" maxlength="100" name="housing_photo5_cap" class="form-control"
                        placeholder="Enter Caption here ..." >
                    </div>
                </td>
            </tr>


		</table>

                </div>
                <!-- /.box-body -->
              </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="update_photos" class="btn btn-info"> Publish </button>
              </form>
            </div>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for uploading more photos -->



        <!-- Begin form for editing house -->
        <div class="modal fade" id="edit<?php echo $product_code_link;?>" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
            	<div class="modal-header bg-success disabled color-palette">
              	   <h4 class="modal-title"> Editing Product Show Details </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
                <div class="modal-body">

                <?php
                    $edit=pg_query($conn,"SELECT * FROM product WHERE product_code_link ='".$product_code_link."'");
                    $urow=pg_fetch_array($edit);
			        (string) $GLOBALS['product_photo'] = $urow['product_photo'];
                    (string) $GLOBALS['product_category'] = $urow['product_category'];

           	    ?>

                <form action="../products/" method="post" enctype="multipart/form-data">
                <div class="box-body">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table table-bordered mb-0">
	                         <tr>
                <td width="25%">
                    <div class="form-group">
                        <input type="hidden" maxlength="50" name="product_code_link" class="form-control"
                         value="<?php echo $urow['product_code_link']; ?>" required>

                        <?php
                            if (!empty($product_photo)){
                                echo '  <img src="images/'.$product_photo.'" alt="User Image"
                                    height="50" width="50" >
                                    <font color="red"> Property photo </font>
                                    <input name="photo_url" type="hidden" value='.$product_photo. '> <br />
                                    <input name="product_photo" type="file" id="exampleInputFile" >';
                            }else{
                                echo '<input name="product_photo" type="file" id="exampleInputFile" >';
                            }
                        ?>
                    </div>
                </td>

                 <td width="50%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Product Description :</label>
                        <textarea rows="6"  name="product_description" class="form-control"  
                            placeholder="Enter product description here ... " ><?php echo $urow['product_description']; ?></textarea>
                    </div>
                </td>
                  <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Product Category : </label>
                       
                        <select name="product_category" class="form-control select2" style="width: 100%;" required>
                         <?php
                        $sql = "SELECT * FROM product_category ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                      
                                        $name = $row['name'];
                                         ?>
                                        <option value="<?php echo $name; ?> "<?php
                                        if ($product_category == $name)
                                        { echo 'selected'; } ?> title=<?php echo $product_category; ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                </td>

            </tr>
            <tr>
                  <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Product Name :</label>
                        <input type="text" maxlength="100" name="product_name" class="form-control"
                            placeholder="Enter product name here ... " 
                            value="<?php echo $urow['product_name']; ?>" 
                            title="Please enter product name" required>
                    </div>
                </td>

                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Product Status : </label>
                        <select name="product_availability" class="form-control select2" style="width: 100%;" required>
                            <option value="" selected> Click to select </option>
                            <option value="true" <?php if ($urow['product_availability'] == 't'){echo "selected";} ?>> Available </option>
                            <option value="false" <?php if ($urow['product_availability'] == 'f'){echo "selected";} ?>> Not Available </option>
                        </select>
                    </div>
                </td>

               <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Product Price :</label>
                        <input type="number" maxlength="100" name="product_price" class="form-control"
                            value="<?php echo $urow['product_price']; ?>"
                            placeholder="Enter price in $ e.g. 100 ... " required>
                    </div>
                </td>
            </tr>
           

		</table>

            </div>
                </div>
                <!-- /.box-body -->
              </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="update_housing" class="btn btn-success">Update </button>
              </form>
            </div>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->

<script type="text/javascript">
        $(document).ready(function(){

            $("#sel_district").change(function(){
                var district_code = $(this).val();

                $.ajax({
                    url: 'getCounty.php',
                    type: 'post',
                    data: {district_code:district_code},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_county").empty();
                        for( var i = 0; i<len; i++){
                            var county_code = response[i]['county_code'];
                            var county_name = response[i]['county_name'];

                            $("#sel_county").append("<option value=''>--- Select County ---</option>");
                            $("#sel_county").append("<option value='"+county_code+"'>"+county_name+"</option>");

                        }
                    }
                });
            });

        });
</script>


    <script type="text/javascript">
        $(document).ready(function(){

            $("#sel_county").change(function(){
                var county_code = $(this).val();

                $.ajax({
                    url: 'getsubCounty.php',
                    type: 'post',
                    data: {county_code:county_code},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_subcounty").empty();
                        for( var i = 0; i<len; i++){
                            var sub_county_code = response[i]['sub_county_code'];
                            var sub_county_name = response[i]['sub_county_name'];
                            
                            $("#sel_subcounty").append("<option value='"+sub_county_code+"'>"+sub_county_name+"</option>");

                        }
                    }
                });
            });

        });
    </script>


    <script type="text/javascript">
        $(document).ready(function(){

            $("#sel_subcounty").change(function(){
                var sub_county_code = $(this).val();

                $.ajax({
                    url: 'getParish.php',
                    type: 'post',
                    data: {sub_county_code:sub_county_code},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_parish").empty();
                        for( var i = 0; i<len; i++){
                            var parish_code = response[i]['parish_code'];
                            var parish_name = response[i]['parish_name'];

                            $("#sel_parish").append("<option value='"+parish_code+"'>"+parish_name+"</option>");

                        }
                    }
                });
            });

        });
    </script>


    <script type="text/javascript">
        $(document).ready(function(){

            $("#sel_parish").change(function(){
                var parish_code = $(this).val();

                $.ajax({
                    url: 'getVillage.php',
                    type: 'post',
                    data: {parish_code:parish_code},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_village").empty();
                        for( var i = 0; i<len; i++){
                            var village_code = response[i]['village_code'];
                            var village_name = response[i]['village_name'];
                            $("#sel_village").append("<option value='"+village_code+"'>"+village_name+"</option>");

                        }
                    }
                });
            });

        });
    </script>


