

        <!-- Begin form -->
        <div class="modal fade" id="reply<?php echo $app_messages_code_link;?>" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
            	<div class="modal-header bg-info disabled color-palette">
              	   <h4 class="modal-title"> Sending a message </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
                <div class="modal-body">

                <?php
                    $edit=pg_query($conn,"SELECT * FROM app_messages  WHERE app_messages_code_link ='".$app_messages_code_link."'");
                    $urow=pg_fetch_array($edit);
           	    ?>

                <form action="../houses/" method="post" enctype="multipart/form-data">
                <div class="box-body">

                    <div class="form-group">
                        <input type="hidden" maxlength="50" name="housing_code_link" class="form-control"
                         value="<?php echo $urow['housing_code_link']; ?>" required>

                        <input type="hidden" maxlength="50" name="sender" class="form-control"
                         value="<?php echo $urow['app_messages_to']; ?>" required>


                        <label for="exampleInputEmail1"> To :</label>
                        <input type="text" maxlength="100" name="to" class="form-control"
                            value="<?php echo $urow['app_messages_from']; ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Subject :</label>
                        <input type="text" maxlength="100" name="subject" class="form-control"
                            value="<?php echo $urow['app_messages_subject']; ?>" readonly> 
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Message :</label>
                        <textarea type="text" maxlength="1000" name="message" class="form-control"> </textarea>
                             
                    </div>


                </div>
                <!-- /.box-body -->
              </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="message_reply" class="btn btn-success">Send Message </button>
              </form>
            </div>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form  -->


