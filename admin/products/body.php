<style>

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 7px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title"> Product Content Manager </h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
      </div>
      <div class="card-body">
       <?php
		    //For alert messages
       if(strlen($error) > 0){
         echo '	<div class="alert alert-success" role="alert" data-auto-dismiss="3000">
         '.$error.'
         </div>';
     }
     ?>

     <table id="cci" class="table table-bordered ">
        <tr>
           <td>

              <div class="tab">
                <button class="tablinks" onclick="openCity(event, 'housing')" id="defaultOpen"> 
                Products  Published  for Sale </button>

                <button class="tablinks" onclick="openCity(event, 'messages')" >
                Product Categories </button>
            </div>

            <div id="housing" class="tabcontent">
               <h4> Product Listing 
                <?php
                $cci = $_SESSION['cci'];
                $access = $_SESSION['privilege'];
                if  (strpos($access,'INSERT') && empty($cci)){
                    echo "<button class='button button2' data-toggle='modal' data-target='#modal-housing'>
                    <i class='fa fa-plus'></i> Add Product </button>";
                }else{
                    echo "<button class='button button4' data-toggle='modal' data-target='#modal-housing' disabled>
                    <i class='fa fa-plus'></i> Add Product </button>";
                }
                ?>

            </h4>

            <table id="example1" class="table table-bordered ">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Code </th>
                        <th> Icon </th>
                        <th> Name </th>
                        <th> Price </th>
                        <th> Category </th>

                        <th> Status </th>
                        <th>CreatedOn </th>
                        <th align='center'> Action </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $cci = $_SESSION['cci'];
                    $access = $_SESSION['privilege'];
                    $user = $_SESSION['user']; 
                    $user_profile = $_SESSION['companyusertype'];
                    if (strpos($access,'READ') && ($user_profile == 'Agents')){
                        $root = "SELECT * FROM product WHERE product_createdby = '$user' ORDER BY product_createdon DESC LIMIT 500";
                    }else{
                        $root = "SELECT * FROM product ORDER BY product_createdon DESC LIMIT 500";
                    }

                    $res1 = pg_query($conn, $root);
                    if ($res1) {
                                // output data of each row
                        $num = 1;
                        while($row1 = pg_fetch_assoc($res1)) {
                            (string) $id = $row1['product_code_link'];
                            (string) $status = $row1['product_availability'];
                            (string) $product_photo = $row1['product_photo'];
                            (string) $product_code_link = $row1['product_code_link'];
                            echo "<tr class='text-sm'>";
                            echo "	<td width='4%'> " . $num . "  </td>";
                            echo "	<td> " . $row1['product_code']. "  </td>";
                            echo "<td>";
                            if (!empty($product_photo)){
                                echo '  <img src="images/'
                                .$product_photo.'" alt="User Image" width="30" height="30" >';
                            }else{
                                echo '  <img src="images/img_default.png" alt="Avatar" width="100" height="100"';
                            }

                            echo "</td>";
                            echo "  <td> " . strtoupper($row1['product_name']) . " </td>";
                            echo "  <td> " . strtoupper($row1['product_price']) . " </td>";
                            echo "  <td> " . strtoupper($row1['product_category']) . " </td>";

                            echo "  <td> "; 
                            if ($status == 't'){echo "Available";}else{echo "Not Available";}
                            echo" </td>";
                            echo "  <td> " . strtoupper($row1['product_createdon']) . " </td>";
                            echo '  <td width="30%">';
                            echo '  <a class="button button4" href="#view'.$product_code_link.'" data-toggle="modal">
                            <i class="fa fa-eye"></i> View</a>';

                                    #Open scheme Details after adding record
                            if (!empty($product)){
                                echo "  <script type='text/javascript'>
                                $(document).ready(function(){
                                    $('#view".$product."').modal('show');
                                    });
                                    </script>";
                                }

                                if  (strpos($access,'EDIT')){
                                    echo '<a class="button button" href="#edit'.$product_code_link.'" data-toggle="modal">
                                    <i class="fa fa-edit"></i> Edit</a>';
                                }else{
                                    echo "<button class='button button4' data-toggle='modal' data-target='#modal-staff'disabled>
                                    <i class='fa fa-edit'></i> Edit </button>";
                                }

                                if  (strpos($access,'UPLOAD')){
                                echo '<a class="button button2" href="#upload'.$product_code_link.'" data-toggle="modal">
                                    <i class="fa fa-upload"></i> Upload </a>';
                                }else{
                                    echo "<button class='button button4' data-toggle='modal' data-target='#modal-staff'disabled>
                                    <i class='fa fa-upload'></i> Upload </button>";
                                }

                                if  (!empty(strpos($access,'DELETE'))){
                                    echo '<a href="house_delete.php?error='. $id.'"
                                    onclick="return confirm(\'Are you sure to delete?\')">
                                    <button class="button button3" >
                                    <i class="fa fa-trash"> </i> Delete </button> </a>';
                                }else{
                                    echo '<a href="house_delete.php?error='. $id.'"
                                    onclick="return confirm(\'Are you sure to delete?\')">
                                    <button class="button button4" disabled>
                                    <i class="fa fa-trash"> </i> Delete </button> </a>';
                                }

                                include('edit_housing.php');
                                echo '</td>';
                                $num++;
                                echo '</tr>';
                            }
                        } else {
                            echo "<tr>";
                            echo "<td colspan='6'> No product configured in the system yet! </td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<td colspan='6' align='right'>";
                            echo "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th> Code </th>
                            <th> Icon </th>
                            <th> Name </th>
                            <th> Price </th>
                              <th> Category </th>
                            <th> Status </th>
                            <th>CreatedOn </th>
                            <th align='center'> Action </th>
                        </tr>
                    </tfoot>
                </table>

            </div>


            <div id="messages" class="tabcontent">
                <h3>Product Categories
                  <?php
                  $cci = $_SESSION['cci'];
                  $access = $_SESSION['privilege'];
                  if  (strpos($access,'INSERT') && empty($cci)){
                    echo "<button class='button button2' data-toggle='modal' data-target='#modal-category'>
                    <i class='fa fa-plus'></i> Add Category </button>";
                }else{
                    echo "<button class='button button4' data-toggle='modal' data-target='#modal-category' disabled>
                    <i class='fa fa-plus'></i> Add Category </button>";
                }
                ?>
            </h3>


            <table id="message" class="table table-bordered ">
                <thead>
                    <tr>
                        <th> Id</th>
                        <th> Name </th>
                        <th> Description </th>
                        <th> Created On </th>
                        <th> Created By </th>
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $cci = $_SESSION['cci'];
                    $access = $_SESSION['privilege'];
                    if (strpos($access,'READ') && (empty($cci))){
                        $root = "SELECT * FROM  product_category ORDER BY category_id DESC LIMIT 500";
                    }
                    $res1 = pg_query($conn, $root);
                    if ($res1) {
                                // output data of each row
                        $num = 1;
                        while($row1 = pg_fetch_assoc($res1)) {
                            (string) $id = $row1['category_code_link'];
                            (string) $app_messages_code_link = $row1['category_code_link'];
                            echo "<tr class='text-sm'>";
                            echo "	<td width='4%'> " . $num . "  </td>";
                            echo "	<td> " . $row1['name']. "  </td>";
                            echo "	<td> " . $row1['description']. "  </td>";
                            echo "	<td> " . $row1['category_createdon']. "  </td>";
                            echo "	<td> " . $row1['category_createdby']. "  </td>";
                            echo '  <td width="10%">';
                            if (!empty($app_messages_code_link)){
                              echo '<a href="category_delete.php?error='. $id.'"
                              onclick="return confirm(\'Are you sure to delete?\')">
                              <button class="button button3" >
                              <i class="fa fa-trash"> </i> Delete </button> </a>';
                          }else{
                           echo '<a href="category_delete.php?error='. $id.'"
                           onclick="return confirm(\'Are you sure to delete?\')">
                           <button class="button button4" disabled>
                           <i class="fa fa-trash"> </i> Delete </button> </a>';

                       }


                       include('reply_message.php');
                       echo '</td>';
                       $num++;
                       echo '</tr>';
                   }
               } else {
                echo "<tr>";
                echo "<td colspan='6'> No categories added yet </td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td colspan='6' align='right'>";
                echo "</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th> Id</th>
                <th> Name </th>
                <th> Description </th>
                <th> Created On </th>
                <th> Created By </th>
                <th> Action </th>

            </tr>
        </tfoot>
    </table>


</div>

</div>

</td>
</tr>
</table>


</div>
<!-- /.card-body -->
<div class="card-footer">
  Housing Content Manager 
</div>
<!-- /.card-footer-->
</div>
<!-- /.card -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- For tab navigation   -->
<script>
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>


<!-- for scheme -->
<div class="modal fade" id="modal-housing">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-info disabled color-palette">
          <h4 class="modal-title"> Publish Product for sale  </h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <form action="../products/" method="post" enctype="multipart/form-data">
        <div class="modal-body">

            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered  mb-0">
                  <tr class="text-sm">
                    <td width="50%">
                        <div class="form-group">
                            <label for="exampleInputEmail1"> Product Photo ( Size 73 × 73 pixels) :</label>
                            <input name="product_photo" type="file" id="exampleInputFile" required>
                        </div>

                    </td>
                    <td width="50%">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Product Name :</label>
                            <input type="text" maxlength="100" name="product_name" class="form-control"
                            placeholder="Enter name of product here ... " 
                            title="Please enter name of product" required>
                        </div>
                    </td>
                    <td width="25%" >
                        <div class="form-group">
                         <label for="exampleInputPassword1"> Product Category : </label>
                       <!--   <select name="product_category" class="form-control select2" style="width: 100%;" required>
                            <option value="" selected> Click to select </option>
                            <option value="true"> Available </option>
                            <option value="false"> Not Available </option>
                        </select> -->
                        <select name="product_category" class="form-control select2" style="width: 100%;" required >
                            <option selected>click to select</option>
                             <?php
                        $sql = "SELECT * FROM product_category ORDER BY name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                      
                                        $name = $row['name'];
                                         ?>

                                        <option value="<?php echo $name; ?> "  >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>
                        </select>
                    </div>
                </td>

            </tr>
            <tr class="text-sm">
                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Product Description :</label>
                        <textarea rows="6"  name="product_description" class="form-control" 
                        placeholder="Enter product description here ... " ></textarea> 
                    </div>
                </td>

                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Product Price :</label>
                        <input type="number" maxlength="100" name="product_price" class="form-control"
                        placeholder="Enter price in US Dollars e.g. 10 ... " style="width: 100%;" required>
                    </div>
                </td>
                <td width="25%" >
                    <div class="form-group">
                     <label for="exampleInputPassword1"> Product Status : </label>
                     <select name="product_availability" class="form-control select2" style="width: 100%;" required>
                        <option value="" selected> Click to select </option>
                        <option value="true"> Available </option>
                        <option value="false"> Not Available </option>
                    </select>
                </div>
            </td>
        </tr>

    </table>
</div>

</div>
<div class="modal-footer justify-content-between">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="submit" name="housing" class="btn btn-info"> Publish </button>
</form>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>


<!-- for scheme -->
<div class="modal fade" id="modal-category">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-info disabled color-palette">
          <h4 class="modal-title"> Add Product Category </h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <form action="../products/" method="post" enctype="multipart/form-data">
        <div class="modal-body">

            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered  mb-0">
                    <tr class="text-sm">

                        <td width="50%">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Name :</label>
                                <input type="text" maxlength="100" name="category_name" class="form-control"
                                placeholder="Enter name of category here ... " 
                                title="Please enter name of category" required>
                            </div>
                        </td>

                    </tr>
                    <tr class="text-sm">
                        <td width="25%">
                            <div class="form-group">

                                <label for="exampleInputEmail1">Category Description :</label>
                                <input type="text" maxlength="100" name="category_description" class="form-control"
                                placeholder="Enter Description of category here ... " 
                                title="Please enter Description of category" required>

                            </td>



                        </tr>

                    </table>
                </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="category" class="btn btn-info"> Publish </button>
          </form>
      </div>
  </div>
  <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Java Scripts for tabs on enrollment   -->
<script>
    function openPage(pageName,elmnt,color) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
    }
    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
  });
});
</script>


<!-- for table search and pagination -->
<script>
  $(function () {
    $("#message").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
  });
});
</script>


