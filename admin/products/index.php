<?php
session_start();
require_once'../library/config.php';
logout_session($conn); //Check wheather user logged in
auto_logout($conn); //Logout iddle user
$GLOBALS['app'] = "slider";
$GLOBALS['nav'] = "products";
$GLOBALS['error'] = "";

#Uploading symbol photo
if(isset($_FILES['product_photo'])){
	$errors= array();
	$file_names = $_FILES['product_photo']['name'];
	$file_sizes =$_FILES['product_photo']['size'];
	$file_tmps =$_FILES['product_photo']['tmp_name'];
	$file_types=$_FILES['product_photo']['type'];
	//$file_ext=strtolower(end(explode('.',$_FILES['org_profile_symbol']['name'])));
	$file_exts = pathinfo($_FILES["product_photo"]["name"])['extension'];
	photo_upload($errors,$file_names,$file_sizes,$file_tmps,$file_types,$file_exts);
}

# Processing Religious Institute
if(isset($_POST['housing'])){
	$product_code = randomString();
	$product_photo = $_FILES['product_photo']['name'];
	$product_description = test_input($_POST['product_description']);
	$product_name = test_input($_POST['product_name']);
	$product_price = test_input($_POST['product_price']);
	$product_availability = test_input($_POST['product_availability']);
	$product_category = test_input($_POST['product_category']);

	$link = sha1($product_code);
	$user = test_input($_SESSION['user']);

	

	$sql = "SELECT * FROM product WHERE (product_photo = '$product_photo')
		AND (product_name = '$product_name')";
	$result = pg_query($conn,$sql);
	$row = pg_fetch_assoc($result);
	$count = pg_num_rows($result);
	// If result matched 
	if($count > 0) {
		$type = "-- warning --";
		$GLOBALS['error'] = "Invalid! This property  " .$product_name. " already exists.";
		$msg = $error;
		syslogs($conn,$ip,$msg,$user,$type);
		$GLOBALS['housing'] = $row['product_code_link'];
	}else{
		$sql = "INSERT INTO product (product_code,product_photo,
			product_description,product_name,product_price,
			product_availability,product_createdby,product_code_link,product_category) 
			VALUES ('$product_code','$product_photo',
				'$product_description','$product_name',
				'$product_price','$product_availability','$user','$link','$product_category')";
		$result = pg_query($conn,$sql);

		if ($result){
			$ip = ip_add();
			$type = "-- normal --";
			$GLOBALS['error'] = "Product has been added by  " .$user. ".";
			$msg = $error;
			syslogs($conn,$ip,$msg,$user,$type);
			$GLOBALS['housing'] = $link;
			#Tab activation
			$GLOBALS['tab'] = "1";
		}else{
			$ip = ip_add();
			$type = "-- warning --";
			$GLOBALS['error'] = "Invalid! Your request has failed try again.";
			$msg = $error;
			syslogs($conn,$ip,$msg,$user,$type);
			$GLOBALS['housing'] = $link;
			#Tab activation
			$GLOBALS['tab'] = "1";
		}
	}
}
	
# adding product category  
if(isset($_POST['category'])){
	$category_code = randomString();
	$category_description = test_input($_POST['category_description']);
	$category_name = test_input($_POST['category_name']);
	$link = sha1($category_code);
	$user = test_input($_SESSION['user']);

	

	$sql = "SELECT * FROM product_category WHERE (name = '$category_name')
		AND (description = '$category_description')";
	$result = pg_query($conn,$sql);
	$row = pg_fetch_assoc($result);
	$count = pg_num_rows($result);
	// If result matched 
	if($count > 0) {
		$type = "-- warning --";
		$GLOBALS['error'] = "Invalid! This Category  " .$category_name. " already exists.";
		$msg = $error;
		syslogs($conn,$ip,$msg,$user,$type);
		$GLOBALS['housing'] = $row['category_code_link'];
	}else{
		$sql = "INSERT INTO product_category (category_code,name,
			description,category_createdby,category_code_link) 
			VALUES ('$category_code','$category_name',
				'$category_description','$user','$link')";
		$result = pg_query($conn,$sql);

		if ($result){
			$ip = ip_add();
			$type = "-- normal --";
			$GLOBALS['error'] = "Product category has been added by  " .$user. ".";
			$msg = $error;
			syslogs($conn,$ip,$msg,$user,$type);
			$GLOBALS['housing'] = $link;
			#Tab activation
			$GLOBALS['tab'] = "1";
		}else{
			$ip = ip_add();
			$type = "-- warning --";
			$GLOBALS['error'] = "Invalid! Your request has failed try again.";
			$msg = $error;
			syslogs($conn,$ip,$msg,$user,$type);
			$GLOBALS['housing'] = $link;
			#Tab activation
			$GLOBALS['tab'] = "2";
		}
	}
}
# Processing editing  
if(isset($_POST['update_housing'])){
	$link = test_input($_POST['product_code_link']);
	$product_photo = $_FILES['product_photo']['name'];
	$product_description = test_input($_POST['product_description']);
	$product_name = test_input($_POST['product_name']);
	$product_price = test_input($_POST['product_price']);
	$product_availability = test_input($_POST['product_availability']);
	$product_category = test_input($_POST['product_category']);


	$user = test_input($_SESSION['user']);
	$photo_url = test_input($_POST['photo_url']);
	$ip = ip_add();

	if (empty($product_photo)){
		$product_photo = $photo_url;
	}

	

	$sql = "UPDATE product SET 
		product_photo = '$product_photo',
		product_description = '$product_description',
		product_name = '$product_name',
		product_price = '$product_price',
		product_availability = '$product_availability',
		product_lastupdatedon = now(),
		product_lastupdatedby = '$user',
		product_category= '$product_category'
		WHERE product_code_link = '$link'";
	$result = pg_query($conn,$sql);
	$tab = '1';
	$type = "-- Normal --";
	$GLOBALS['error'] = "Product posting ".$product_name." has been updated.";
	$msg = $error;
	syslogs($conn,$ip,$msg,$user,$type);
	$GLOBALS['housing'] = $link;
}

#Uploading photo
if(isset($_FILES['housing_photo1'])){
	$errors= array();
	$file_names = $_FILES['housing_photo1']['name'];
	$file_sizes =$_FILES['housing_photo1']['size'];
	$file_tmps =$_FILES['housing_photo1']['tmp_name'];
	$file_types=$_FILES['housing_photo1']['type'];
	//$file_ext=strtolower(end(explode('.',$_FILES['org_profile_symbol']['name'])));
	$file_exts = pathinfo($_FILES["housing_photo1"]["name"])['extension'];
	photo_upload($errors,$file_names,$file_sizes,$file_tmps,$file_types,$file_exts);
}

#Uploading photo
if(isset($_FILES['housing_photo2'])){
	$errors= array();
	$file_names = $_FILES['housing_photo2']['name'];
	$file_sizes =$_FILES['housing_photo2']['size'];
	$file_tmps =$_FILES['housing_photo2']['tmp_name'];
	$file_types=$_FILES['housing_photo2']['type'];
	//$file_ext=strtolower(end(explode('.',$_FILES['org_profile_symbol']['name'])));
	$file_exts = pathinfo($_FILES["housing_photo2"]["name"])['extension'];
	photo_upload($errors,$file_names,$file_sizes,$file_tmps,$file_types,$file_exts);
}

#Uploading photo
if(isset($_FILES['housing_photo3'])){
	$errors= array();
	$file_names = $_FILES['housing_photo3']['name'];
	$file_sizes =$_FILES['housing_photo3']['size'];
	$file_tmps =$_FILES['housing_photo3']['tmp_name'];
	$file_types=$_FILES['housing_photo3']['type'];
	//$file_ext=strtolower(end(explode('.',$_FILES['org_profile_symbol']['name'])));
	$file_exts = pathinfo($_FILES["housing_photo3"]["name"])['extension'];
	photo_upload($errors,$file_names,$file_sizes,$file_tmps,$file_types,$file_exts);
}

#Uploading symbol photo
if(isset($_FILES['housing_photo4'])){
	$errors= array();
	$file_names = $_FILES['housing_photo4']['name'];
	$file_sizes =$_FILES['housing_photo4']['size'];
	$file_tmps =$_FILES['housing_photo4']['tmp_name'];
	$file_types=$_FILES['housing_photo4']['type'];
	//$file_ext=strtolower(end(explode('.',$_FILES['org_profile_symbol']['name'])));
	$file_exts = pathinfo($_FILES["housing_photo4"]["name"])['extension'];
	photo_upload($errors,$file_names,$file_sizes,$file_tmps,$file_types,$file_exts);
}


#Uploading photo
if(isset($_FILES['housing_photo5'])){
	$errors= array();
	$file_names = $_FILES['housing_photo5']['name'];
	$file_sizes =$_FILES['housing_photo5']['size'];
	$file_tmps =$_FILES['housing_photo5']['tmp_name'];
	$file_types=$_FILES['housing_photo5']['type'];
	//$file_ext=strtolower(end(explode('.',$_FILES['org_profile_symbol']['name'])));
	$file_exts = pathinfo($_FILES["housing_photo5"]["name"])['extension'];
	photo_upload($errors,$file_names,$file_sizes,$file_tmps,$file_types,$file_exts);
}

# Add photos to the property  
if(isset($_POST['update_photos'])){
	$link = test_input($_POST['product_code_link']);
	$housing_photo1 = $_FILES['housing_photo1']['name'];
	$housing_photo2 = $_FILES['housing_photo2']['name'];
	$housing_photo3 = $_FILES['housing_photo3']['name'];
	$housing_photo4 = $_FILES['housing_photo4']['name'];
	$housing_photo5 = $_FILES['housing_photo5']['name'];
	$housing_photo1_cap = test_input($_POST['housing_photo1_cap']);
	$housing_photo2_cap = test_input($_POST['housing_photo2_cap']);
	$housing_photo3_cap = test_input($_POST['housing_photo3_cap']);
	$housing_photo4_cap = test_input($_POST['housing_photo4_cap']);
	$housing_photo5_cap = test_input($_POST['housing_photo5_cap']);
	$user = test_input($_SESSION['user']);
	$ip = ip_add();

	$sql = "UPDATE product SET housing_photo1 = '$housing_photo1',
		housing_photo2 = '$housing_photo2',
		housing_photo3 = '$housing_photo3',
		housing_photo4 = '$housing_photo4',
		housing_photo5 = '$housing_photo5',
		housing_photo1_cap = '$housing_photo1_cap',
		housing_photo2_cap = '$housing_photo2_cap',
		housing_photo3_cap = '$housing_photo3_cap',
		housing_photo4_cap = '$housing_photo4_cap',
		housing_photo5_cap = '$housing_photo5_cap',
		product_lastupdatedon = now(),
		product_lastupdatedby = '$user'
		WHERE product_code_link = '$link'";
	$result = pg_query($conn,$sql);
	$tab = '1';
	$type = "-- Normal --";
	$GLOBALS['error'] = "Product photo album has been published.";
	$msg = $error;
	syslogs($conn,$ip,$msg,$user,$type);
	$GLOBALS['housing'] = $link;
}


require_once'../header.php';
require_once'../navigation.php';
include('body.php');
require_once'../footer.php';
?>

