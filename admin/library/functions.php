<?php

//Send emails
//require 'mailer.php';
use PHPMailer\PHPMailer\PHPMailer;
require 'vendor/autoload.php';

function send_email($conn,$sub,$msg,$to,$from){
		  #send mail
		  $mail = new PHPMailer();
		  $mail->isSMTP();
		  $mail->Host = gethostname();
		  $mail->SMTPAuth = true;
		  $mail->Username = 'ramanya@gemcs.biz';
		  $mail->Password = ',amanya,.@2020';
		  $mail->setFrom($from);
		  $mail->addAddress($to);
		  $mail->Subject = $sub;
		  $mail->Body    = $msg;
		  $mail->send();
		  return null;
}

//Get IP address
function ip_add(){
		  //whether ip is from share internet
		  if (!empty($_SERVER['HTTP_CLIENT_IP'])){
					 $ip_address = $_SERVER['HTTP_CLIENT_IP'];
		  }
		  //whether ip is from proxy
		  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
					 $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
		  }
		  //whether ip is from remote address
		  else{
					 $ip_address = $_SERVER['REMOTE_ADDR'];
		  }

		  #Get IP Address 
		  $ip = $ip_address;
		  return $ip;
}

// Saving System logs to DB
function syslogs($conn,$ip,$msg,$user,$type){
		  $sys_log ="INSERT INTO syslogs (syslogs_connection_from,syslogs_messages,
					 syslogs_created_by,logtype) 
					 VALUES ('$ip','$msg','$user','$type')";
		  $res = pg_query($conn,$sys_log);
}

// Saving village details to db
function add_village($conn,$code,$name,$parish,$user){
		  $link = sha1($code);
		  $sql = "INSERT INTO village (village_code,parish_code,village_name,village_code_link) 
					 VALUES ('$code','$parish','$name','$link')";
		  $res = pg_query($conn,$sql);
}

// Editing district details
function edit_village($conn,$code,$name,$parish,$user){
		  $link = $code;
		  $sql = "UPDATE village SET parish_code = '$parish', village_name = '$name'  
					 WHERE village_code_link = '$link'";
		  $res = pg_query($conn,$sql);
}

// Saving county details to db
function add_parish($conn,$code,$name,$subcounty,$user){
		  $link = sha1($code);
		  $sql = "INSERT INTO parish (parish_code,sub_county_code,parish_name,parish_code_link) 
					 VALUES ('$code','$subcounty','$name','$link')";
		  $res = pg_query($conn,$sql);
}

// Editing district details
function edit_parish($conn,$code,$name,$subcounty,$user){
		  $link = $code;
		  $sql = "UPDATE parish SET sub_county_code = '$subcounty', parish_name = '$name'  
					 WHERE parish_code_link = '$link'";
		  $res = pg_query($conn,$sql);
}

// Saving county details to db
function add_subcounty($conn,$code,$name,$county,$user){
		  $link = sha1($code);
		  $sql = "INSERT INTO sub_county (sub_county_code,county_code,sub_county_name,sub_county_code_link) 
					 VALUES ('$code','$county','$name','$link')";
		  $res = pg_query($conn,$sql);
}

// Editing district details
function edit_subcounty($conn,$code,$name,$county,$user){
		  $link = $code;
		  $sql = "UPDATE sub_county SET county_code = '$county', sub_county_name = '$name'  
					 WHERE sub_county_code_link ='$link'";
		  $res = pg_query($conn,$sql);
}

// Saving county details to db
function add_county($conn,$code,$name,$district,$user){
		  $link = sha1($code);
		  $sql = "INSERT INTO county (county_code,district_code,county_name,county_code_link) 
					 VALUES ('$code','$district','$name','$link')";
		  $res = pg_query($conn,$sql);
}

// Editing district details
function editcounty($conn,$code,$name,$district,$user){
		  $link = $code;
		  $sql = "UPDATE county SET district_code = '$district', county_name = '$name'  
					 WHERE county_code_link ='$link'";
		  $res = pg_query($conn,$sql);
}

// Saving district details to db
function add_district($conn,$tribecode,$name,$user,$region){
		  $link = sha1($tribecode);
		  $sql = "INSERT INTO district (district_code,district_name,district_code_link,region) 
					 VALUES ('$tribecode','$name','$link',$region)";
		  $res = pg_query($conn,$sql);
}

// Editing district details
function edit_district($conn,$id,$name,$user,$region){
		  $link = test_input($id);
		  $sql = "UPDATE district SET district_name = '$name', region = '$region'  
					 WHERE district_code_link ='$link'";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
}

//Staff creation
function stf($conn,$staff_code,$staff_lname,$staff_email,$staff_nin,$ccidetails,
		  $staff_phone,$staff_title,$staff_education,$staff_speciality,$user){
		  $link = sha1($staff_code);
		  $sql = "INSERT INTO staff (staff_code,staff_lname,staff_phone,staff_email,
					 staff_title,staff_education,staff_speciality,staff_nin,ccidetails,staff_url,staff_createdby) 
					 VALUES ('$staff_code','$staff_lname','$staff_phone','$staff_email','$staff_title',
								'$staff_education','$staff_speciality','$staff_nin','$ccidetails','$link','$user')";
		  $res = pg_query($conn,$sql);

		  //Creating login account for CCI
		  $link1 = sha1($staff_code);
		  $passCode = randomString();
		  $cfmPassCode = $passCode;
		  $sql = "INSERT INTO Company (CompanyAccountName,CompanyFullName,CompanyUserType,CompanyEmail,
					 CompanyPhone,passCode,cfmPassCode,CompanyNameCreatedBy,CompanyUrl,user_ccfDetails)
					 VALUES('$staff_email','$staff_lname','001SPT','$staff_email','$staff_phone',
								'$passCode','$cfmPassCode','$user','$link1','$ccidetails')";
		  $res = pg_query($conn,$sql);

		  $sub = "Account Creation";
		  $msg = "Account has been created\n Username is : ". $staff_email . " password is : ";
		  $email = $staff_email;
		  $phone = $staff_phone;
		  $new_pwd = $passCode;
		  //Sending email to the new staff added
		  emailing($conn,$sub,$msg,$email,$phone,$new_pwd);

}
// Saving ccidetail to db
function editStaff($conn,$staff_code,$staff_lname,$staff_email,$staff_nin,$ccidetails,
		  $staff_phone,$staff_title,$staff_education,$staff_speciality,$user){
		  $link = $staff_code;
		  $sql = "UPDATE staff SET staff_lname = '$staff_lname',
					 staff_email = '$staff_email',
					 staff_nin = '$staff_nin',
					 staff_phone = '$staff_phone',
					 staff_title = '$staff_title',
					 staff_education = '$staff_education',
					 staff_speciality = '$staff_speciality',
					 staff_createdby = '$user',
					 ccidetails = '$ccidetails'
					 WHERE staff_url = '$link'";
		  if (!pg_query($conn,$sql))
		  {
					 echo("Error description: " . pg_error($conn));
		  }	
}

// Saving Region details to db
function location($conn,$locationcode,$locationname,$locationtown,$locationregion,$user){
		  $region = "INSERT INTO Location (LocationCode,LocationName,LocationTown,
					 LocationRegion,LocationCreatedBy) VALUES ('$locationcode','$locationname','$locationtown',
					 '$locationregion','$user')";
		  $res = pg_query($conn,$region);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The location " . $locationcode . " " . $locationname . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Saving User Type details to db
function usertype($conn,$usertypecode,$usertypename,$user){
		  $link = sha1($usertypecode);
		  $usertype = "INSERT INTO userType (usertypeCode,usertypeName,usertypeCreatedBy,usertype_code_link) VALUES 
					 ('$usertypecode','$usertypename','$user','$link')";
		  $res = pg_query($conn,$usertype);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The user type " . $usertypecode . " " . $usertypename . "has been created by " . $user . ".";
		  $msg = $error;
		  $type = "--normal--";
		  syslogs($conn,$connecting_IP,$msg,$type,$user);
}

// Saving User type  Changes to db
function editusertype($conn,$usertypecode,$usertypename,$user){
		  $link = sha1($usertypecode);
		  $region = "UPDATE userType SET usertypeName ='$usertypename',usertypeCreatedBy = '$user' WHERE usertype_code_link ='$link'";
		  $res = pg_query($conn,$region);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The user type " . $usertypename . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

// Saving Tribe details to db
function tribe($conn,$tribecode,$name,$user){
		  $link = sha1($tribecode);
		  $tribe = "INSERT INTO tribe (tribe_name,tribe_createdby,tribe_code_link) 
					 VALUES ('$name','$user','$link')";
		  $res = pg_query($conn,$tribe);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The tribe " . $tribecode . " " . $name . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

function edittribe($conn,$id,$name,$user){
		  $link = $id;
		  $region = "UPDATE tribe SET tribe_name = '$name', tribe_createdby = '$user' 
					 WHERE tribe_code_link ='$link'";
		  $res = pg_query($conn,$region);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The region " . $regionname . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Saving Referral details to db
function referral($conn,$referral,$name,$user){
		  $link = sha1($referral);
		  $sql = "INSERT INTO referral (referral_name,referral_code_link) 
					 VALUES ('$name','$link')";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The Referral " . $referral . " " . $name . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

// Editing Referral details
function editreferral($conn,$id,$name,$user){
		  $link = test_input($id);
		  $sql = "UPDATE referral SET referral_name = '$name'  
					 WHERE religion_code_link ='$link'";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The referral " . $name . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

// Saving Religion details to db
function religion($conn,$religioncode,$name,$user){
		  $link = sha1($religioncode);
		  $sql = "INSERT INTO religion (religion_name,religion_createdby,religion_code_link) 
					 VALUES ('$name','$user','$link')";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The Religion " . $religioncode . " " . $name . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Editing Religion details
function editreligion($conn,$id,$name,$user){
		  $link = test_input($id);
		  $sql = "UPDATE religion SET religion_name = '$name', religion_createdby = '$user' 
					 WHERE religion_code_link ='$link'";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The region " . $regionname . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

// Saving disability details to db
function disability($conn,$disabilitycode,$name,$user){
		  $link = sha1($disabilitycode);
		  $sql = "INSERT INTO disability (disability_name,disability_createdby,disability_code_link) 
					 VALUES ('$name','$user','$link')";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The Disability " . $disabilitycode . " " . $name . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Editing Religion details
function editdisability($conn,$id,$name,$user){
		  $link = test_input($id);
		  $sql = "UPDATE disability SET disability_name = '$name', disability_createdby = '$user' 
					 WHERE disability_code_link ='$link'";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The region " . $regionname . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

// Saving Region details to db
function spec($conn,$spec_code,$spec_name,$user){
		  $link = sha1($spec_code);
		  $sql = "INSERT INTO speciality (spec_code,spec_url,spec_name,spec_createdby) 
					 VALUES ('$spec_code','$link','$spec_name','$user')";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The title " . $spec_code . " " . $spec_name . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Saving Region Changes to db
function editspec($conn,$spec_code,$spec_name,$user){
		  $link = sha1($spec_code);
		  $sql = "UPDATE speciality SET spec_name = '$spec_name',spec_createdby = '$user' 
					 WHERE spec_url ='$link'";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The speciality " . $spec_code . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Saving Region details to db
function edu($conn,$edu_code,$edu_name,$user){
		  $link = sha1($edu_code);
		  $sql = "INSERT INTO education (edu_code,edu_url,edu_name,edu_createdby) 
					 VALUES ('$edu_code','$link','$edu_name','$user')";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The title " . $edu_code . " " . $edu_name . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Saving Region Changes to db
function editedu($conn,$edu_code,$edu_name,$user){
		  $link = sha1($edu_code);
		  $sql = "UPDATE education SET edu_name = '$edu_name',edu_createdby = '$user' WHERE edu_url ='$link'";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The title " . $edu_code . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

// Saving Region details to db
function title($conn,$title_code,$title_name,$user){
		  $link = sha1($title_code);
		  $sql = "INSERT INTO title (title_code,title_url,title_name,title_createdby) 
					 VALUES ('$title_code','$link','$title_name','$user')";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The title " . $title_code . " " . $title_name . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Saving Region Changes to db
function edittitle($conn,$title_code,$title_name,$user){
		  $link = sha1($title_code);
		  $sql = "UPDATE title SET title_name = '$title_name',title_createdby = '$user' WHERE title_url ='$link'";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The title " . $title_code . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

// Saving Region details to db
function fundingarea($conn,$fundingarea_code,$fundingarea_name,$user){
		  $link = sha1($title_code);
		  $sql = "INSERT INTO fundingarea (fundingarea_code,fundingarea_url,fundingarea_name,fundingarea_createdby) 
					 VALUES ('$fundingarea_code','$link','$fundingarea_name','$user')";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The funding area " . $fundingarea_code . " " . $fundingarea_name . 
					 "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Saving Region Changes to db
function editfundingarea($conn,$fundingarea_code,$fundingarea_name,$user){
		  $link = sha1($fundingarea_code);
		  $sql = "UPDATE fundingarea SET fundingarea_name = '$fundingarea_name',fundingarea_createdby = '$user' 
					 WHERE fundingarea_url ='$link'";
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The funding area " . $fundingarea_code . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}


// Saving Region details to db
function cctype($conn,$cctypecode,$cctypename,$user){
		  $link = sha1($cctypecode);
		  $cctype = "INSERT INTO cctypes (cctype_code,cctype_code_link,cctype_name,cctype_created_by) 
					 VALUES ('$cctypecode','$link','$cctypename','$user')";
		  $res = pg_query($conn,$cctype);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The region " . $cctypecode . " " . $cctypename . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}
// Saving CC Type Changes to db
function editcctype($conn,$cctypecode,$cctypename,$user){
		  $link = sha1($cctypecode);
		  $cctype ="UPDATE cctypes SET cctype_name = '$cctypename',cctype_created_by = '$user' 
					 WHERE cctype_code_link ='$link'"; 
		  $res = pg_query($conn,$cctype);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The region " . $cctypecode . "has been modified by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg);
}

// Password recoverer with email " . $email . " has requested for password change";
function recovery($conn,$email,$phone){
		  $message = "INSERT INTO passwd_recovery (email,phone) VALUES ('$email','$phone')";
		  $res = pg_query($conn,$message);
		  // $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "The user with email address " . $email . " requested for password recovery.";
		  // companyCreationLogging($conn,$connecting_IP,$msg);
		  // use wordwrap() if lines are longer than 70 characters
		  $msg = wordwrap($msg,70);

		  // send email
		  mail($email,"Password Recovery",$msg);
}
// Cleaning inputs from the forms 
function test_input($data) {
		  $data = trim($data);
		  $data = stripslashes($data);
		  $data = htmlspecialchars($data);
		  return $data;
}

//Verifying Users at Login
function login($conn,$myusername,$mypassword){
		  if (!empty($myusername) && !empty($mypassword)){
					 $sql = "SELECT * FROM company,user_roles 
								WHERE companyusertype != 'root' 
								AND company.companyusertype = user_roles.role_name 
								AND companyuserisactive = true 
								AND companyemail = '$myusername' AND passcode = '$mypassword'";
					 $result = pg_query($conn, $sql);
					 if (pg_num_rows($result) > 0) {
								// output data of each row
								while($row = pg_fetch_assoc($result)) {
										  $ip = ip_add();
										  $_SESSION['login_user'] = $row['companyaccountname'];
										  $_SESSION['user'] = $row['companyfullname'];
										  $_SESSION['login_email'] = $myusername;
										  $_SESSION['login_userid'] =$row['companynamecreatedby'];
										  $_SESSION['companyusertype'] =$row['companyusertype'];
										  $_SESSION['privilege'] =$row['role_access_rights'];
										  $_SESSION['applications'] =$row['role_modules'];
										  $_SESSION['companyphone'] =$row['companyphone'];
										  $_SESSION['companycreatedon'] =$row['companycreatedon'];
										  $_SESSION['domain'] =$row['company_domain'];
										  $_SESSION['cci'] = $row['company_org_id'];
										  $_SESSION['start'] = time(); // Taking now logged in time.
										  // Ending a session in 30 nutes from the starting time.
										  $_SESSION['expire'] = $_SESSION['start'] + (60 * 120);
										  $msg = $_SESSION['login_user'] . " logged in from " . $ip;
										  $user = $row['companyfullname'];
										  $online = "UPDATE company SET isonline = true WHERE companyemail = '$myusername'";
										  $on = pg_query($conn,$online);
										  # Log action
										  $msg = "The user " . $user . " has logged in.";
										  $type = "-- normal --";
										  syslogs($conn,$ip,$msg,$user,$type);
										  header("location: home/");
								}
					 }else{
								$GLOBALS['error'] = "Invalid! Try again...";
					 }
		  }
		  elseif (strlen(trim($myusername)) > 0 && strlen(trim($mypassword)) == '0'){
					 $GLOBALS['error'] = "Invalid! Provide password.";
					 $msg = "This IP " . $ip . " attempting to login without a password.";
					 $user = $myusername;
					 $type = "--warning--";
					 $failed_log = "INSERT INTO syslogs (syslogs_connection_from,syslogs_messages) 
								VALUES ('$ip','$msg')";
					 $ress = pg_query($conn,$failed_log);
		  }
		  elseif (strlen(trim($myusername)) == '0' && strlen(trim($mypassword)) > 0){
					 $GLOBALS['error'] = "Invalid! Provide username.";
					 $msg = "This IP " . $ip . " attempting to login without username.";
					 $user = $myusername;
					 $type = "--warning--";
					 $failed_log = "INSERT INTO syslogs (syslogs_connection_from,syslogs_messages) 
								VALUES ('$ip','$msg')";
					 $ress = pg_query($conn,$failed_log);
		  }
		  else{
					 $GLOBALS['error'] = "Invalid! Provide Credentials.";
					 $msg = "This IP " . $connecting_IP . " attempting to login.";
					 $failed_log = "INSERT INTO syslogs (syslogs_connection_from,syslogs_messages) 
								VALUES ('$ip','$msg')";
					 $ress = pg_query($conn,$failed_log);
		  }
}

// Logging out
function logout_session($conn){

		  if (!isset($_SESSION)){
					 session_start();
		  }

		  $user_check = $_SESSION['login_user'];
		  $ses_sql = pg_query($conn,"SELECT CompanyAccountName 
					 FROM Company WHERE CompanyAccountName = '$user_check' ");
		  $row = pg_fetch_assoc($ses_sql);
		  $login_session = $row['companyaccountname'];
		  if(!isset($_SESSION['login_user'])){
					 $ip = ip_add();
					 $type = "-- normal --";
					 $GLOBALS['error'] = "User " .$user_check. " logged out.";
					 $msg = $error;
					 $user = $user_check;
					 syslogs($conn,$ip,$msg,$user,$type);
					 header("location: ../logout/");
		  }
}

//Logging out logs
function logout_log($conn){

		  if (!isset($_SESSION)){
					 session_start();
		  }

		  $user_check = $_SESSION['login_user'];
		  $user = $_SESSION['login_user'];
		  $msg = $_SESSION['login_user'] . " has logged out.";
		  $msg = $msg;
		  $ip = ip_add();
		  $type = "-- normal --";
		  syslogs($conn,$ip,$msg,$user,$type);
		  $offline = "UPDATE Company SET isonline = FALSE, last_loggin = now() WHERE CompanyAccountName = '$user'";
		  $off = pg_query($conn,$offline);
}

//Log out iddle users after 10 minutes
function auto_logout($conn){

		  if (!isset($_SESSION)){
					 session_start();
		  }

		  $user_check = $_SESSION['login_user'];
		  $now = time();
		  if ($now > $_SESSION['expire']) {

					 $ip = ip_add();
					 $user = $_SESSION['login_user'];
					 $msg = $_SESSION['login_user'] . " has been logged out for being idle from " . $ip;

					 $offline = "UPDATE Company SET isonline = FALSE, last_loggin = now() 
								WHERE CompanyAccountName = '$user'";
					 $off = pg_query($conn,$offline);

					 $type = "-- warning --";
					 $GLOBALS['error'] = "User " .$user. " has been logged out.";
					 $msg = $error;
					 syslogs($conn,$ip,$msg,$user,$type);
					 //Destroy the current session
					 session_destroy();
					 //Redirect 
					 header("Location: ../logout/");
		  }
}


// Saving account to DB
function create_account($conn,$username,$fname,$usertype,$email,$phone,$passCode,$cfmPassCode,$user){
		  $link = sha1($email);
		  $pass = sha1($passCode);
		  $pass_cfm = $pass;
		  $create_user = "INSERT INTO Company (CompanyAccountName,CompanyFullName,CompanyUserType,
					 CompanyEmail,CompanyPhone,passCode,cfmPassCode,CompanyNameCreatedBy,CompanyUrl)
					 VALUES('$username','$fname','$usertype','$email','$phone','$pass',
								'$pass_cfm','$user','$link')";
		  $res = pg_query($conn,$create_user);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $type = "---normal---";
		  $msg = "The account " . $username . " " . $email . "has been created by " . $user . ".";
		  syslogs($conn,$connecting_IP,$msg,$type,$user);

		  $sub = "Account Creation";
		  $msg = "Account has been created\n Username is : ". $email . " password is : ";
		  $email = $email;
		  $phone = $phone;
		  $new_pwd = $passCode;
		  //Sending email to the new staff added
		  emailing($conn,$sub,$msg,$email,$phone,$new_pwd);

}
// Generating random numbers
function randomString($length = 5) {
		  $str = "";
		  $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		  $max = count($characters) - 1;
		  for ($i = 0; $i < $length; $i++) {
					 $rand = mt_rand(0, $max);
					 $str .= $characters[$rand];
		  }
		  $number = rand(100,999);
		  $ch = $str;
		  $str = $number.$ch;
		  return $str;
}

// Saving Number of children who have died 
function recover_pwd($conn, $email,$phone,$new_pawd){
		  $pwd = sha1($new_pawd); #hashing new password.

		  $sql = "UPDATE Company SET passCode ='$pwd', cfmPassCode = '$pwd'
					 WHERE CompanyEmail = '$email' AND CompanyPhone = '$phone'"; 
		  $res = pg_query($conn,$sql);
		  $connecting_IP = $_SERVER['REMOTE_ADDR'];
		  $msg = "Password for user " . $email . " " . $phone . " renewed from password recovery system.";
		  syslogs($conn,$connecting_IP,$msg);
}

#Function for uploading photos
function photo_upload($error,$file_name,$file_size,$file_tmp,$file_type,$file_ext){

		  $expensions= array("jpeg","jpg","png","JPG","JPEG","PNG");

		  if(in_array($file_ext,$expensions)=== false){
					 $error[]="extension not allowed, please choose a JPEG or PNG file.";
		  }

		  if($file_size > 9097155){
					 $error[]='File size must be excately 4 MB';
		  }

		  if(empty($error)==true){
					 move_uploaded_file($file_tmp,"images/".$file_name);
					 # echo "Success";
		  }else{
					 # print_r($error);
		  }
}

// Saving Number of children who have died 
function display_date($date){
		  $yy = substr($date,0,4);
		  $mm = substr($date,5,2);
		  $dd = substr($date,8,2);
		  $date = $dd."/".$mm."/".$yy;
		  return $date;
}

// Saving Number of children who have died 
function edit_date($date){
		  $yy = substr($date,0,4);
		  $mm = substr($date,5,2);
		  $dd = substr($date,8,2);
		  $date = $yy."-".$mm."-".$dd;
		  return $date;
}

// Converting date format 
function convert_dob($data) {
		  $data = trim($data);
		  // Check if date has been set
		  if (isset($data)){
					 $mm = substr($data,0,2);  
					 $dd = substr($data,3,2);
					 $yy = substr($data,6,4);
					 $data = $yy."-".$mm."-".$dd;
					 return $data;
		  }else{
					 $data = "0000-00-00";
					 return $data;
		  }
}


//Visitor Counter
function total_views($conn, $page_id = null){
		  if($page_id === null){
					 // count total website views
					 $query = "SELECT sum(total_views) as total_views FROM pages";
					 $result = pg_query($conn, $query);

					 if(pg_num_rows($result) > 0){
								while($row = pg_fetch_assoc($result)){
										  if($row['total_views'] === null){
													 return 0;
										  }else{
													 return $row['total_views'];
										  }
								}
					 }else{
								return "No records found!";
					 }
		  }else{
					 // count specific page views
					 $query = "SELECT total_views FROM pages WHERE id='$page_id'";
					 $result = pg_query($conn, $query);

					 if(pg_num_rows($result) > 0){
								while($row = pg_fetch_assoc($result)){
										  if($row['total_views'] === null){
													 return 0;
										  }else{
													 return $row['total_views'];
										  }
								}
					 }else{
								return "No records found!";
					 }
		  }
}

function is_unique_view($conn, $visitor_ip, $page_id){
		  $query = "SELECT * FROM page_views WHERE visitor_ip='$visitor_ip' AND page_id='$page_id'";
		  $result = pg_query($conn, $query);

		  if(pg_num_rows($result) > 0){
					 return false;
		  }else{
					 return true;
		  }
}

function add_view($conn, $visitor_ip, $page_id){
		  if(is_unique_view($conn, $visitor_ip, $page_id) === true){
					 // insert unique visitor record for checking whether the visit is unique or not in future.
					 $query = "INSERT INTO page_views (visitor_ip, page_id) VALUES ('$visitor_ip', '$page_id')";

					 if(pg_query($conn, $query)){
								// At this point unique visitor record is created successfully. Now update total_views of specific page.
								$query = "UPDATE pages SET total_views = total_views + 1 WHERE id='$page_id'";

								if(!pg_query($conn, $query)){
										  echo "Error updating record: " . pg_error($conn);
								}
					 }else{
								echo "Error inserting record: " . pg_error($conn);
					 }
		  }
}

?>
