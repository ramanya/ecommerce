  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                	<?php
							$total_website_views = total_views($conn);
							echo "<h3>". $total_website_views . "</h3>";
						?>

                <p> Number of visitors </p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#../" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                    <?php

                        $sql = "SELECT count(*) as total FROM product";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                            // output data of each row
                             while($row = pg_fetch_assoc($result)) {
                                echo "<h3>".$row['total']."</h3>";
                            }
                        }
                    ?>

                <p> Number of Products </p>
              </div>
              <div class="icon">
                <i class="ion ion-home"></i>
              </div>
              <a href="../products/" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">

                <?php
                        $cci = $_SESSION['cci'];
                        $access = $_SESSION['privilege'];
                        if  (strpos($access,'READ') && (!empty($cci))){
                                $sql = "SELECT count(*) as total FROM company WHERE company_org_id = '$cci'";
                        }else{
                                $sql = "SELECT count(*) as total FROM company";
                        }

                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                // output data of each row
                                while($row = pg_fetch_assoc($result)) {
                                        echo "<h3>".$row['total']."</h3>";
                                }
                        }
                ?>


                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
		<?php 
			$cci = $_SESSION['cci'];
                        $access = $_SESSION['privilege'];
                        if  (strpos($access,'READ') && (empty($cci))){
                        	echo '<a href="../account-management/" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>';
                        }else{
                        	echo '<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>';
                       }
		?>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">

                <?php
                        $sql = "SELECT count(*) as total FROM orders";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                // output data of each row
                                while($row = pg_fetch_assoc($result)) {
                                        echo "<h3>".$row['total']."</h3>";
                                }
                        }
                ?>


                <p> Number of Orders </p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="../orders/" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->

	



<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
