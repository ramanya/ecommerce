<?php
	session_start();
	require_once'../library/config.php';
 	logout_session($conn); //Check wheather user logged in
    auto_logout($conn); //Logout iddle user
    $GLOBALS['app'] = "dashboard";
    $GLOBALS['nav'] = "home";
    $GLOBALS['error'] = "";

 	require_once'../header.php';
 	require_once'../navigation.php';
 	include('body.php');
 	require_once'../footer.php';
?>
