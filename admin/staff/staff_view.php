
        <!-- Begin form for adding user -->
        <div class="modal fade" id="edit<?php echo $id; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
            	<div class="modal-header">
              	   <h4 class="modal-title">Staff Account Details </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM staff WHERE staff_url ='".$id."'");
                        $urow=pg_fetch_array($edit);
			(string) $GLOBALS['staff_title'] = $urow['staff_title'];
			(string) $GLOBALS['staff_education'] = $urow['staff_education'];
			(string) $GLOBALS['staff_speciality'] = $urow['staff_speciality'];
			(string) $GLOBALS['staff_photo'] = $urow['staff_photo'];
			(string) $GLOBALS['staff_login'] = $urow['staff_login'];
           	?>


                <form method="POST" action="../staff/">
                <div class="box-body">

            <table width="100%" class="table table-bordered table-striped">
               <tr>
                  <td width="50%">

	          <?php if (!empty($staff_photo)){
                  		echo '  <img src="images/'.$staff_photo.'" alt="User Image"
                        	height="50" width="50" > ';
                  	}else{
                        	echo '  <img src="images/img_avatar.png" alt="User Image"
                                height="50" width="50" > ';
                  	}
                ?>

		</td>
		<td>
                    <label> Full Name :  </label>
                    <?php echo $urow['staff_lname']; ?> 
		</td>
	   </tr>
	   <tr>
		<td>
                    <label> Email Address :  </label>
                    <?php echo $urow['staff_email']; ?> 
		</td>
		<td>
                        <label for="exampleInputEmail1"> Phone Number :</label>
                        <?php echo $urow['staff_phone']; ?> 
		</td>
           </tr>
                <tr>
                   <td>
                        <label for="exampleInputEmail1">NIN :</label>
                        <?php echo $urow['staff_nin']; ?> 
                    </td>
                    <td>
                        <label for="exampleInputPassword1"> Select Staff Title : </label>
			<?php echo $staff_title; ?> 
                   </td>

                </tr>

                <tr>
                  <td>
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select Level of Education : </label>
			<?php echo $staff_education;?> 
                  </td>

                  <td>
                        <label for="exampleInputPassword1"> Select Speciality : </label>
			<?php echo $staff_speciality; ?> 
                  </td>
                </tr>
                <tr>
                </tr>
        </table>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="update_staff" class="btn btn-primary">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

