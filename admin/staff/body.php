  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Staff Account Management </h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

                <?php
                        if(strlen($error) > 0){
                                echo '
                                        <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" 
                                        aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i> Alert!</h4>' .$error . '
                                        </div>';
                        }
                ?>



		<table id="cci" class="table table-bordered ">
              		<tr>
			<td>
			<?php
			   $tab_staff = $_GET["tab_staff"];
			   $tab_priv = $_GET["tab_priv"];
			   $tab_role = $_GET["tab_role"];
			   $tab_user = $_GET["tab_user"];
			?>
			<button class="tablink" onclick="openPage('root', this, '#04812F')" id="defaultOpen" > 
			   <i class="fa fa-user"></i>  Staff Members </button>
			<button class="tablink" onclick="openPage('#Home', this, '#04812F')" <?php if ($tab_priv == '1'){ echo 'id="defaultOpen"';} ?>>
			   <i class="fa fa-user-secret"></i> Privileges </button>
			<button class="tablink" onclick="openPage('#News', this, '#04812F')" <?php if ($tab_role == '1'){ echo 'id="defaultOpen"';} ?>> 
			   <i class="fa fa-sun-o"></i> User Roles </button>
			<button class="tablink" onclick="openPage('#Contact', this, '#04812F')" 
			<?php if ($tab_user == '1'){ echo 'id="defaultOpen"';}else{echo 'id="defaultOpen"';} ?>> <i class="fa fa-users"> </i> Users </button>
			<button class="tablink" onclick="openPage('#About', this, '#04812F')"> System Logs </button>

                        <div id="root" class="tabcontent">
                           <h3>Staff/Members Accounts

                                <?php
                                        $cci = $_SESSION['cci'];
                                        $access = $_SESSION['privilege'];
                                        if  (strpos($access,'INSERT') && !empty($cci)){

                                                echo "  <button class='button button2' data-toggle='modal' data-target='#modal-staff'>
                                                        <i class='fa fa-plus'></i>Add Staff </button>";
                                        }else{
                                                echo "	<button class='button button4' data-toggle='modal' data-target='#modal-staff'disabled>
                                 			<i class='fa fa-plus'></i>Add Staff </button>";
                                        }
                                ?>
 
			   </h3>

              		   <table id="example1" class="table table-bordered table-striped">
 			      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Name </th>
                		 <th>Email </th>
                		 <th>Phone </th>
				 <th align='center'> Action </th>
                	       	</tr>
                	       	</thead>
                	       	<tbody>
  			       	<?php

                               		$cci = $_SESSION['cci'];
                                        $access = $_SESSION['privilege'];
                                        if  (strpos($access,'READ') && (!empty($cci))){
						$root = "SELECT * FROM staff WHERE staff_org='$cci' ORDER BY staff_lname ASC";
                                        }
					
					if (empty($cci)){
						$root = "SELECT * FROM staff ORDER BY staff_lname ASC";
                                        }
	
			          #$root = "SELECT * FROM staff ORDER BY staff_lname ASC";
				  $res1 = pg_query($conn, $root);
				  if ($res1) {
    				     // output data of each row
				     $number = 1;
    				     while($row1 = pg_fetch_assoc($res1)) {
                                        (string) $id = $row1['staff_url'];
                                        (string) $trash = $row1['staff_trash'];
					(string) $status = $row1['staff_status'];
                                        (string) $staff_url = $row1['staff_url'];
                                        (string) $staff_photo = $row1['staff_photo'];
       					echo "<tr>";
       					echo "<td>" . $number . "</td>";
					echo "<td>";
                                        
					if (!empty($staff_photo)){
                                        	echo '  <img src="images/'
                                                     .$staff_photo.'" alt="User Image"style="width:30px" >';
                                        }else{
                                                echo '  <img src="images/img_default.png" alt="Avatar" 
                                             		style="width:30px">';
                                       	}

					echo "  " .  $row1['staff_lname'];  
					echo "</td>";
					echo "<td>" . $row1['staff_email'] . "</td>";
					echo "<td>" . $row1['staff_phone'] . "</td>";
					echo '<td width="33%">';

                                        echo ' <a class="button button5" href="#view'.$staff_url.'" data-toggle="modal">
                                               <i class="fa fa-eye"></i> View</a>';

                                        if  (strpos($access,'EDIT')){
                                        	echo ' 	<a class="button button" href="#edit'.$staff_url.'" data-toggle="modal">
                                               		<i class="fa fa-edit"></i> Edit</a>';
                                        }else{
                                                echo "  <button class='button button4' data-toggle='modal' data-target='#modal-staff'disabled>
                                                        <i class='fa fa-edit'></i> Edit </button>";
                                        }

					if ($status == 't'){
					   echo ' <button class="button button2" title="Disable account!" 
					 	  onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						  <i class="fa fa-user"> </i> Disable </button> ';
					}else{
                                           echo ' <button class="button button4" title="Enable account!" 
                                                  onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                  <i class="fa fa-user-times"> </i> Enable </button> ';
				       }

                                      if ($trash == 'f'){
                                         echo ' <button class="button button5" title="Trash record!" 
                                                onclick="window.location.href ='."'".'staff_trash.php?error='.$id."';".'"> 
                                                <i class="fa fa-trash"> </i> Trash </button> ';
                                      }else{
                                        echo '<a href="staff_delete.php?error='. $id.'" 
                                                onclick="return confirm(\'Are you sure to delete?\')"> <button class="button button3" > 
                                                <i class="fa fa-user-times"> Delete </i> </button> </a>';
                                      }

                                      include('edit_staff.php');
                                      echo '</td>';
				      $number++;
                                      echo '</tr>';
                                    }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> There are no staff members configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  	       }
                	      ?>
                	      </tbody>
                	      <tfoot>
                	      <tr>
              		      <tr>
                                 <th>Id</th>
                                 <th>Name </th>
                                 <th>Name </th>
                                 <th>Phone </th>
                        	 <th align='center'> Action </th>
			      </tr>
                	      </tr>
                	      </tfoot>
              		   </table>
                        </div>

			<div id="Home" class="tabcontent">
                           <h3>User Privileges 
	                      <button class='button button2' data-toggle='modal' data-target='#modal-privilege'>
                	         <i class='fa fa-user-secret'></i>Add Privilege</button>
			   </h3>
              		   <table id="privilege" class="table table-bordered table-striped">
                	      <thead>
              		         <tr>
                		    <th>Id</th>
                		    <th>User privilege name</th>
                		    <th>Created By</th>
                		    <th>Created On</th>
				    <th align='center'> Action </th>
                		 </tr>
                		 </thead>
                		 <tbody>
  				 <?php	
				    $pr = "SELECT * FROM access_rights ORDER BY right_name ASC";
				    $rs = pg_query($conn, $pr);
				    if ($rs) {
    				       // output data of each row
				       $number = 1;
    				       	while($rw = pg_fetch_assoc($rs)) {
                                           (string) $id = $rw['access_url'];
                                           (string) $access_url = $rw['access_url'];
                                           (string) $trash = $rw['access_trash'];
       					   echo "<tr>";
                                           echo "<td>" . $number . "</td>";
					   echo "<td>" . $rw['right_name'] . "</td>";
					   echo "<td>" . $rw['access_created_by'] . "</td>";
					   echo "<td>" . $rw['access_created_on'] . "</td>";
					   echo "<td width='25%'>";
                                           echo ' <a class="button button" href="#edit'.$access_url.'" data-toggle="modal">
                                                  <i class="fa fa-edit"></i> Edit</a>';

					   if ($is_active == 't'){
                                              echo ' <button class="button button2" title="Disable account!" 
					             onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						     <i class="fa fa-user"> </i> Disable </button> ';
					   }else{
                                              echo ' <button class="button button4" title="Enable account!" 
                                                     onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                     <i class="fa fa-user-times"> </i> Enable </button> ';
					  }

                                          if ($trash == 'f'){
                                             echo ' <button class="button button5" title="Trash record!" 
                                                    onclick="window.location.href ='."'".'priv_trash.php?error='.$id."';".'"> 
                                                    <i class="fa fa-trash"> </i> Trash </button> ';
                                          }else{
                                ?>
                                        <a href="priv_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }
                                         include('edit_privilege.php');
                                         echo '</td>';

                                         $number++;
                                         echo '</tr>';
                                      }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> There user privilege configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  		}
                		?>
                		</tbody>
                		<tfoot>
                		<tr>
                		<tr>
                        	   <th>Id</th>
                		   <th>User privilege name</th>
                                   <th>Created By</th>
                                   <th>Created On</th>
                        	   <th align='center'> Action </th>
                                </tr>
                	        </tr>
                	        </tfoot>
              		     </table>
			</div>


			<div id="News" class="tabcontent">

                           <h3>User Roles / Profile 
			      <button class='button button2' data-toggle='modal' data-target='#modal-role'>
                	         <i class='fa fa-user-secret'></i>Add User Role</button>
			   </h3>

			   <table id="role" class="table table-bordered table-striped">
                	      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Role name</th>
                		 <th>Privileges</th>
                		 <th>Created By</th>
				 <th align='center'> Action </th>
                	      </tr>
                	      </thead>
                	      <tbody>

			      <?php	
			         $roles = "SELECT * FROM user_roles ORDER BY role_name ASC";
				 $result_role = pg_query($conn, $roles);
				 if ($result_role) {
    				   // output data of each row
				   $number = 1;
    				   while($row_role = pg_fetch_assoc($result_role)) {
                                      (string) $id = $row_role['role_url_link'];
                                      (string) $role_url_link = $row_role['role_url_link'];
                                      (string) $trash = $row_role['role_trash'];

                                      echo "<tr>";
                                      echo "<td>" . $number . "</td>";
                                      echo "<td>" . $row_role['role_name'] . "</td>";
				      echo "<td>" . $row_role['role_access_rights'] . "</td>";
				      echo "<td>" . $row_role['role_createdby'] . "</td>";

				      echo "<td>";
                                      echo  ' <a class="button button" href="#edit'.$role_url_link.'" data-toggle="modal">
                                              <i class="fa fa-edit"></i> Edit</a>';

               			      if ($is_active == 't'){
                                         echo ' <button class="button button2" title="Disable account!" 
						onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						<i class="fa fa-user"> </i> Disable </button> ';
				      }else{
                                         echo ' <button class="button button4" title="Enable account!" 
                                                onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                <i class="fa fa-user-times"> </i> Enable </button> ';
				     }

                                     if ($trash == 'f'){
                                        echo ' <button class="button button5" title="Trash record!" 
                                               onclick="window.location.href ='."'".'role_trash.php?error='.$id."';".'"> 
                                               <i class="fa fa-trash"> </i> Trash </button> ';
                                     }else{
                                ?>
                                        <a href="role_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }
				    
                                     include('button.php');
				     echo '</td>';
				     $number++;
                                     echo '</tr>';
                                  }
                               } else {
                                  echo "<tr>";
                                  echo "<td colspan='6'> There user privilege configured in the system yet! </td>";
                                  echo "</tr>";
                                  echo "<tr>";
                                  echo "<td colspan='6' align='right'>";
                                  echo "</td>";
                                  echo "</tr>";
                  	       }
                	    ?>
                	    </tbody>
                	    <tfoot>
                	    <tr>
                	    <tr>
                               <th>Id</th>
                	       <th>Role name</th>
                	       <th>Privileges</th>
                               <th>Created By</th>
                               <th align='center'> Action </th>
                	    </tr>
                	    </tr>
                	    </tfoot>
              		 </table>
	              </div>
		
		      <div id="Contact" class="tabcontent">

                           <h3>User accounts 
   	                      <button class='button button2' data-toggle='modal' data-target='#modal-user'>
                	         <i class='fa fa-plus'></i>Add User Account</button>
			   </h3>

              		   <table id="users" class="table table-bordered table-striped">
 			      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Username</th>
                		 <th>Name </th>
                		 <th>User Role </th>
                		 <th>Last Loggin </th>
				 <th align='center'> Action </th>
                	       </tr>
                	       </thead>
                	       <tbody>
  			       <?php	
			          $root = "SELECT * FROM company,user_roles 
					   WHERE (companyusertype != 'root') 
					   OR (company.companyusertype = user_roles.role_access_rights)
					   ORDER BY companyaccountname ASC";
				  $res1 = pg_query($conn, $root);
				  if ($res1) {
    				     // output data of each row
				     $number = 1;
    				     while($row1 = pg_fetch_assoc($res1)) {
                                        (string) $id = $row1['companyurl'];
                                        (string) $trash = $row1['company_trash'];
                                        (string) $is_active = $row1['companyuserisactive'];
                                        (string) $companyurl = $row1['companyurl'];
       					echo "<tr>";
       					echo "<td>" . $number . "</td>";

					$status = $row1['companyuserisactive'];
					$online = $row1['isonline'];
					if (($status == 'f') && ($online == 'f')){
                   			   echo " <td> 
						     <i class='fa fa-circle text-danger'></i> " . " " ."
						     <i class='fa fa-user-times'> </i> "."- " 
						     . $row1['companyaccountname'] . "</td>";
					}elseif (($status == 't') && ($online == 't')){
                                       	   echo " <td> 
						     <i class='fa fa-circle text-success'></i> " . " " ."
						     <i class='fa fa-user'> </i> " . "- " 
                                                     . $row1['companyaccountname'] . "</td>";
					}else{
                                           echo "<td> 
                                                    <i class='fa fa-circle text-danger'></i> " . " " ."
                                                    <i class='fa fa-user'> </i> " . "- "  
                                                    . $row1['companyaccountname'] . "</td>";
					}

					echo "<td>" . $row1['companyfullname'] . "</td>";
		               		echo "<td title='" . $row1['role_access_rights'] . "'>" . $row1['role_name'] . "</td>";
					if (!empty($row1['last_loggin'])){
					   echo "<td>" . $row1['last_loggin'] . "</td>";
					}else{
					   echo "<td> <font color='red'> Never logged in </font> </td>";
					}

					echo '<td width="27%">';
                                        echo ' <a class="button button" href="#edit'.$companyurl.'" data-toggle="modal">
                                               <i class="fa fa-edit"></i> Edit</a>';

					if ($is_active == 't'){
					   echo ' <button class="button button2" title="Disable account!" 
					 	  onclick="window.location.href ='."'".'disable.php?error='.$id."';".'"> 
						  <i class="fa fa-user"> </i> Disable </button> ';
					}else{
                                           echo ' <button class="button button4" title="Enable account!" 
                                                  onclick="window.location.href ='."'".'enable.php?error='.$id."';".'"> 
                                                  <i class="fa fa-user-times"> </i> Enable </button> ';
				       }

                                      if ($trash == 'f'){
                                         echo ' <button class="button button5" title="Trash record!" 
                                                onclick="window.location.href ='."'".'user_trash.php?error='.$id."';".'"> 
                                                <i class="fa fa-trash"> </i> Trash </button> ';
                                      }else{
				?>
                                        <a href="user_delete.php?error=<?php echo $id;?>" 
						onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
				<?php


                                      }

                                      include('edit_user.php');
                                      echo '</td>';
				      $number++;
                                      echo '</tr>';
                                    }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> There user types configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  	       }
                	      ?>
                	      </tbody>
                	      <tfoot>
                	      <tr>
              		      <tr>
                                 <th>Id</th>
                        	 <th>Username</th>
                		 <th>Name </th>
                        	 <th>User Role</th>
                        	 <th>Last Loggin</th>
                        	 <th align='center'> Action </th>
			      </tr>
                	      </tr>
                	      </tfoot>
              		   </table>


		      </div>

		     <div id="About" class="tabcontent">
  		        <h3>System logs </h3>
             		<table id="syslog" class="table table-bordered table-striped">
                	   <thead>
                	   <tr>
                  	      <th>Log id</th>
                  	      <th>IP</th>
                  	      <th>Log message</th>
                  	      <th>User</th>
                  	      <th>Date</th>
                	   </tr>
                	   </thead>
                	   <tbody>
                	   <?php
                              $sl = "SELECT * FROM syslogs 
				     WHERE syslogs_messages !=''	
				     ORDER BY syslogs_id DESC limit 200";
                              $rt = pg_query($conn, $sl);
                              if ($rt){
                                 $number = 1;
                                 while($sys = pg_fetch_assoc($rt)) {
			   ?>
                	   <tr>
                  	      <td> <small> <?php echo $sys['syslogs_id']; ?> </small> </td>
                  	      <td> <small><?php echo $sys['syslogs_connection_from'];?></small></td>
                  	      <td> <small> <?php echo $sys['syslogs_messages']; ?> </small> </td>
                  	      <td> <small> <?php echo $sys['syslogs_created_by']; ?> </small> </td>
                  	      <td width="20%"> <small> <?php echo $sys['syslogs_day_created']; ?> </small> </td>
                	   </tr>
			   <?php
			         $number++;
			      }
			   }  
			  ?>
			  <tr>	
                  	     <th>Log id</th>
                  	     <th>IP</th>
                  	     <th>Log message</th>
                  	     <th>User</th>
                  	     <th>Date</th>
			  </tr>
            	       </table> 
		    </div>
		</td>
             </tr>
         </table>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          User Account Management
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




      <!-- for root -->
      <div class="modal fade" id="modal-staff">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Create a staff account </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../staff/" method="post" enctype="multipart/form-data">
            <div class="modal-body">

	    <table width="100%" class="table table-bordered table-striped">
	       <tr>
	          <td width="50%">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Photo :</label>
                        <input name="staff_photo" type="file" id="exampleInputFile" >
                    </div>
	           </td>
		   <td> 

                    <div class="form-group">
                        <label for="exampleInputEmail1">Full Name :</label>
                        <input type="text" name="staff_lname" class="form-control" placeholder="Enter user's name" required>
                    </div>
		    </td>
		</tr>
		<tr>
		   <td>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Email Address :</label>
                        <input type="email" name="staff_email" class="form-control" placeholder="Enter user's email address" required>
                    </div>
		   </td>
		   <td>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Phone Number :</label>
                        <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                        </div>
                        <input type="text" class="form-control" name="staff_phone"
                          class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                    </div>


		   </td>
		</tr>
		<tr>
                   <td>

                    <div class="form-group">
                        <label for="exampleInputEmail1">NIN :</label>
                        <input type="text" name="staff_nin" class="form-control" placeholder="Enter National ID number" >
                    </div>
                    </td>
		    <td>

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select Staff Title : </label>
			<select name="staff_title" class="form-control select2" style="width: 100%;" required>
				<option value="" selected> Click to select </option>
                        <?php
                        $sql = "SELECT * FROM title ORDER BY title_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['title_name']; ?> 
                                        <option value="<?php echo $name; ?>">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
		    </div>
		   </td>

		</tr>
		<tr>
		  <td>
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select Level of Education : </label>
                        <select name="staff_education" class="form-control select2" style="width: 100%;" >
				<option value="" selected> Click to select </option>
                        <?php
                        $sql = "SELECT * FROM education ORDER BY education_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['education_name']; ?>
                                        <option value="<?php echo $name; ?>">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
		  </td>

                  <td>
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select Speciality : </label>
                        <select name="staff_speciality" class="form-control select2" style="width: 100%;" >
				<option value="" selected> Click to select </option>
                        <?php
                        $sql = "SELECT * FROM speciality ORDER BY speciality_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['speciality_name']; ?>
                                        <option value="<?php echo $name; ?>">
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                  </td>
		</tr>
		<tr>
		   <td colspan="2">

                      <div class="icheck-primary d-inline">
                        <input type="checkbox" id="checkboxPrimary2" name="staff_login" value="1" >
                        <label for="checkboxPrimary2">
                          Do you want to create a loging a ccount for this user?
                        </label>
                      </div>

		   </td>
		</tr>
	</table>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="staff" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->




<!-- Java Scripts for tabs on enrollment   -->
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#privilege").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#role").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#syslog").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#users").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

