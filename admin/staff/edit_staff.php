        <!-- Begin form for adding user -->
        <div class="modal fade" id="view<?php echo $staff_url; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content bg-info">
            	<div class="modal-header">
              	   <h4 class="modal-title">Staff Account Details </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM staff WHERE staff_url ='".$staff_url."'");
                        $urow=pg_fetch_array($edit);
			(string) $GLOBALS['staff_title'] = $urow['staff_title'];
			(string) $GLOBALS['staff_education'] = $urow['staff_education'];
			(string) $GLOBALS['staff_speciality'] = $urow['staff_speciality'];
			(string) $GLOBALS['staff_photo'] = $urow['staff_photo'];
			(string) $GLOBALS['staff_login'] = $urow['staff_login'];
           	?>

            <table width="100%" class="table table-bordered table-striped">
            	<tr>
                	<td width="30%" align="center">

	          	<?php 	if (!empty($staff_photo)){
                  			echo '  <img src="images/'.$staff_photo.'" alt="User Image"
                        		height="150" width="150" > ';
                  		}else{
                        		echo '  <img src="images/img_avatar.png" alt="User Image"
                                	height="150" width="150" > ';
                  		}
                	?>

			</td>
			<td>
                    		<label> Full Name :  </label>
                    		<?php echo $urow['staff_lname']; ?> 
			<br />
                    		<label> Email Address :  </label>
                    		<?php echo $urow['staff_email']; ?> 
			<br />
                        	<label for="exampleInputEmail1"> Phone Number :</label>
                        	<?php echo $urow['staff_phone']; ?> 
			<br />
                        	<label for="exampleInputEmail1">NIN :</label>
                        	<?php echo $urow['staff_nin']; ?> 
                    	<br />
                        	<label for="exampleInputPassword1"> Select Staff Title : </label>
				<?php echo $staff_title; ?> 
                	<br />
                        	<label for="exampleInputPassword1"> Select Level of Education : </label>
				<?php echo $staff_education;?> 
                  	<br />
                        	<label for="exampleInputPassword1"> Select Speciality : </label>
				<?php echo $staff_speciality; ?> 
                  	</td>
                </tr>
        </table>

       	</div>
       		<div class="modal-footer justify-content-between">
       		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php
        	echo '<button title="Click to print." class="btn btn-default" 
                      onclick="window.open('."'".'Printing.php?error='.$staff_url."','_blank','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=400')".'" > <i class="fa fa-print"></i> Print </button>  ';
	?>

       	 	</div>
        </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



        <!-- Begin form for adding user -->
        <div class="modal fade" id="edit<?php echo $staff_url; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
            	<div class="modal-header">
              	   <h4 class="modal-title">Editing staff account </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM staff WHERE staff_url ='".$staff_url."'");
                        $urow=pg_fetch_array($edit);
			(string) $GLOBALS['staff_title'] = $urow['staff_title'];
			(string) $GLOBALS['staff_education'] = $urow['staff_education'];
			(string) $GLOBALS['staff_speciality'] = $urow['staff_speciality'];
			(string) $GLOBALS['staff_photo'] = $urow['staff_photo'];
			(string) $GLOBALS['staff_login'] = $urow['staff_login'];
           	?>


                <form method="POST" action="../staff/">

            <table width="100%" class="table table-bordered table-striped">
               <tr>
                  <td width="50%">

                    <div class="form-group">

                        <input  type="hidden" name="staff_url" class="form-control" value="<?php echo $staff_url; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                <?php
                        if (!empty($staff_photo)){
                                echo '  <img src="images/'.$staff_photo.'" alt="User Image"
                                        height="50" width="50" >
                                        <font color="red"> Current staff photo! </font>
                                        <input name="url" type="hidden" value='.$staff_photo. '> <br />
                                        <input name="staff_photo" type="file" id="exampleInputFile" >';
                        }else{
                                echo '<input name="staff_photo" type="file" id="exampleInputFile" >';
                        }
                ?>

                    </div>
		</td>
		<td>
                    <div class="form-group">
                    <label> Full Name :  </label>
                        <input  type="text" name="staff_lname" class="form-control" value="<?php echo $urow['staff_lname']; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                    </div>
		</td>
	   </tr>
	   <tr>
		<td>
                    <div class="form-group">
                    <label> Email Address :  </label>
                        <input  type="text" name="staff_email" class="form-control" value="<?php echo $urow['staff_email']; ?>" 
                                id="inputdefault" style="width: 100%;"  readonly>
                    </div>
		</td>
		<td>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Phone Number :</label>
                        <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                        </div>
                        <input type="text" class="form-control" name="staff_phone" value="<?php echo $urow['staff_phone']; ?>" 
                          class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                    </div>

		</td>
           </tr>
                <tr>
                   <td>

                    <div class="form-group">
                        <label for="exampleInputEmail1">NIN :</label>
                        <input type="text" name="staff_nin" class="form-control" value="<?php echo $urow['staff_nin']; ?>" 
				 placeholder="Enter National ID number" >
                    </div>
                    </td>
                    <td>

                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select Staff Title : </label>
                        <select name="staff_title" class="form-control select2" style="width: 100%;" required>
                                <option value="" selected> Click to select </option>
                        <?php
                        $sql = "SELECT * FROM title ORDER BY title_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['title_name']; ?>
                                        <option value="<?php echo $name; ?>" 
					<?php if ($staff_title == $name) {echo "selected";} ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                   </td>

                </tr>

                <tr>
                  <td>
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select Level of Education : </label>
                        <select name="staff_education" class="form-control select2" style="width: 100%;" >
                                <option value="" selected> Click to select </option>
                        <?php
                        $sql = "SELECT * FROM education ORDER BY education_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['education_name']; ?>
                                        <option value="<?php echo $name; ?>"
					<?php if ($staff_education == $name) {echo "selected";} ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                  </td>

                  <td>
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Select Speciality : </label>
                        <select name="staff_speciality" class="form-control select2" style="width: 100%;" >
                                <option value="" selected> Click to select </option>
                        <?php
                        $sql = "SELECT * FROM speciality ORDER BY speciality_name ASC";
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                        $name = $row['speciality_name']; ?>
                                        <option value="<?php echo $name; ?>"
					<?php if ($staff_speciality == $name) {echo "selected";} ?> >
                                        <?php  echo $name; ?> </option>
                        <?php
                                }
                        }
                        ?>

                        </select>
                    </div>
                  </td>
                </tr>
                <tr>
                   <td colspan="2">

                      <div class="icheck-primary d-inline">
                        <input type="checkbox" id="checkboxPrimary1" name="staff_login" value="1" 
			<?php if ($staff_login == '1') {echo "checked"; } ?> >
                        <label for="checkboxPrimary1">
                          Do you want to create a loging a ccount for this user?
                        </label>
                      </div>
                   </td>
                </tr>
        </table>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="update_staff" class="btn btn-primary">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

