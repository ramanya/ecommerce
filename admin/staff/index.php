<?php
	session_start();
	require_once'../library/config.php';
 	logout_session($conn); //Check wheather user logged in
	auto_logout($conn); //Logout iddle user
        $GLOBALS['app'] = "reg";
        $GLOBALS['nav'] = "staff";
        $GLOBALS['error'] = "";

        #Uploading child's photo
        if(isset($_FILES['staff_photo'])){
                $error= array();
                $file_name = $_FILES['staff_photo']['name'];
                $file_size =$_FILES['staff_photo']['size'];
                $file_tmp =$_FILES['staff_photo']['tmp_name'];
                $file_type=$_FILES['staff_photo']['type'];
                //$file_ext=strtolower(end(explode('.',$_FILES['staff_photo']['name'])));     
                $file_ext = pathinfo($_FILES["staff_photo"]["name"])['extension'];
                photo_upload($error,$file_name,$file_size,$file_tmp,$file_type,$file_ext);
        }

        # Processing Staff
        if(isset($_POST['staff'])){
		$staff_code = randomString();
                $staff_lname = test_input($_POST['staff_lname']);
                $link = sha1($staff_lname);
		$staff_photo = $_FILES['staff_photo']['name'];
                $staff_lname = test_input($_POST['staff_lname']);
                $staff_email = test_input($_POST['staff_email']);
                $staff_phone = test_input($_POST['staff_phone']);
                $staff_nin = test_input($_POST['staff_nin']);
                $staff_title = test_input($_POST['staff_title']);
                $staff_login = test_input($_POST['staff_login']);
                $staff_education = test_input($_POST['staff_education']);
                $staff_speciality = test_input($_POST['staff_speciality']);
                $user = test_input($_SESSION['user']);
                $reg_inst = test_input($_SESSION['cci']);

		if (empty($staff_login)){
			$staff_login = '0';
		}

                $sql = "SELECT * FROM staff WHERE (staff_phone = '$staff_phone') OR 
                	(staff_email = '$staff_email') OR (staff_nin = '$staff_nin') OR 
			(staff_photo = '$staff_photo')";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_array($result,MYSQLI_ASSOC);
                $count = pg_num_rows($result);
                // If result matched $myusername and $mypassword, table row must be 1 row
                if($count > 0) {
                	$GLOBALS['error'] = "Invalid! Check username ".$staff_lname .", " . $staff_email . " or " . $staff_phone ." already exists.";
                }else{
		
                	$sql = "INSERT INTO staff(staff_code,staff_lname,staff_phone,staff_email,
					staff_url,staff_createdby,staff_title,staff_education,
					staff_speciality,staff_nin,staff_login,staff_photo,staff_org) 
                        	VALUES ('$staff_code','$staff_lname','$staff_phone','$staff_email',
					'$link','$user','$staff_title','$staff_education',
					'$staff_speciality','$staff_nin','$staff_login','$staff_photo','$reg_inst')";
                	$result = pg_query($conn,$sql);

                //Creating login account for CCI
                if ($staff_login == '1'){
                        $link1 = sha1($staff_code);
                        $passCode = sha1(randomString());
                        $cfmPassCode = $passCode;
			$ip = ip_add();
                        $sql = "INSERT INTO company (companyaccountname,companyemail,companyphone,passcode,cfmpasscode,
					companynamecreatedby,companyurl,companyusertype,companyfullname,companyconip,
					company_org_id)
                                VALUES('$staff_email','$staff_email','$staff_phone','$passCode','$cfmPassCode',
                                '$user','$link1','Support','$staff_lname','$ip','$reg_inst')";
                        $res = pg_query($conn,$sql);

			//Send email aleart
                                $lnk = "https://yourdomain.com";
                                $sub = "Account Creation";
                                $email = $email;
                                $phone = $phone;
                                $new_pwd = $passCode;
                                $msg = "Account has been created\n Username is : ". $email . " password is : "
                                        .$new_pwd. "\n The link is ".$lnk;
                                //Sending email to the new staff added
                                sendemail($conn,$sub,$msg,$email);
                                $GLOBALS['error'] = "Account successfuly created.";

                }
		
			$tab = '1';
                	if ($result){
                        	$ip = ip_add();
                        	$type = "-- normal --";
                        	$GLOBALS['error'] = "Staff account has been created by  " .$user. ".";
                        	$msg = $error;
                        	syslogs($conn,$ip,$msg,$user,$type);
                        	echo '<script>window.location="../staff/?tab_role='.$tab.'"</script>';
                	}else{
                        	$ip = ip_add();
                        	$type = "-- warning --";
                        	$GLOBALS['error'] = "Invalid! Adding staff account has failed. ";
                        	$msg = $error;
                        	syslogs($conn,$ip,$msg,$user,$type);
                        	echo '<script>window.location="../staff/?tab_role='.$tab.'"</script>';
                	}
        	}
	}

        # Processing staff update
        if(isset($_POST['update_staff'])){
                $staff_lname = test_input($_POST['staff_lname']);
                $staff_photo = $_FILES['staff_photo']['name'];
                $staff_lname = test_input($_POST['staff_lname']);
                $staff_email = test_input($_POST['staff_email']);
                $staff_phone = test_input($_POST['staff_phone']);
                $staff_nin = test_input($_POST['staff_nin']);
                $staff_title = test_input($_POST['staff_title']);
                $staff_login = test_input($_POST['staff_login']);
                $staff_education = test_input($_POST['staff_education']);
                $staff_speciality = test_input($_POST['staff_speciality']);
                $url = test_input($_POST['url']);
                $staff_url = test_input($_POST['staff_url']);
                $user = test_input($_SESSION['user']);
                $reg_inst = test_input($_SESSION['cci']);

                if (empty($staff_login)){
                        $staff_login = '0';
                }

		if (empty($staff_photo)){
			$staff_photo = $url;
		}

                $sql = "UPDATE staff SET staff_lname = '$staff_lname',
			staff_phone = '$staff_phone',
			staff_email = '$staff_email',
			staff_title = '$staff_title',
			staff_education ='$staff_education',
			staff_speciality ='$staff_speciality',
			staff_nin ='$staff_nin',
			staff_login ='$staff_login',
			staff_photo='$staff_photo',
			staff_lastupdatedon = now(),
			staff_lastupdatedby = '$user'
			WHERE staff_url = '$staff_url'";
                $result = pg_query($conn,$sql);

		$tab = '1';
                if ($result){

                        $ip = ip_add();
                        $type = "-- normal --";
                        $GLOBALS['error'] = "User role has been modified by  " .$user. ".";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../staff/?tab_role='.$tab.'"</script>';
                }else{
                        $ip = ip_add();
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! Modifying user role has failed. ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../staff/?tab_role='.$tab.'"</script>';
                }
        }

	
 	require_once'../header.php';
 	require_once'../navigation.php';
 	include('body.php');
 	require_once'../footer.php';
?>

