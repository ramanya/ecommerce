
        <!-- Begin form for adding user -->
        <div class="modal fade" id="edit<?php echo $role_url_link; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
            	<div class="modal-header">
              	   <h4 class="modal-title">Editing user roles / profiles </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM user_roles WHERE role_url_link='".$role_url_link."'");
                        $erow=pg_fetch_array($edit);
           	?>


                <form method="POST" action="index.php">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Role name :</label>

                        <input  type="hidden" name="role_url_link" class="form-control" value="<?php echo $role_url_link; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                        <input 	type="text" name="rolename" class="form-control" value="<?php echo $erow['role_name']; ?>" 
				id="inputdefault" style="width: 100%;"  required>
                    </div>
                    <div class="form-group">
                    <label> Role Privileges :  </label>
		    <select class="select2" name="privilege[]"  multiple="multiple" data-placeholder="Select a State" 
			data-dropdown-css-class="select2-purple" style="width: 100%;">
                    <?php
			(string) $priv = $erow['role_access_rights'];

                        $ovcdArr = explode(",", $priv);
                        $sqll = "SELECT * FROM access_rights ORDER BY right_name ASC";
                        $results = pg_query($conn, $sqll);
                        if (pg_num_rows($results) > 0) {
                                while($roww = pg_fetch_assoc($results)) {
                                        $id = $roww['right_id'];
                                        $name = $roww['right_name'];
                                        ?>
                                        <option value="<?php echo $name; ?>" 
                                        <?php if (in_array($name, $ovcdArr) ){echo 'selected';}else{ echo " ";} ?> >
                                        <?php echo $name ?> </option>
                        <?php
                                }
                        } 
                     ?>
                    </select>

                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="update_role" class="btn btn-primary">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->
