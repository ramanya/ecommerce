

    <!-- Begin to view Religious -->
    <div class="modal fade" id="view<?php echo $land_code_link;?>" tabindex="-1" role="dialog" a-labelledby="myModalLabell" aria-hidden="true">
          <div class="modal-dialog modal-xl">
            <div class="modal-content bg-default">
            	<div class="modal-header bg-info disabled color-palette">
              	   <h4 class="modal-title"> Property Details </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              	    $edit=pg_query($conn,"SELECT * FROM land WHERE land_code_link ='".$land_code_link."'");
                    $urow=pg_fetch_array($edit);
                    (string) $GLOBALS['land_photo'] = $urow['land_photo'];
                    (string) $GLOBALS['status'] = $urow['land_availability'];
           	    ?>

                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                <table class="table table-bordered">
            	    <tr>
			            <td width="50%" align="center">
			                <!-- for the org logo -->
                            <?php   
				                if (!empty($land_photo)){
                                    echo '  <img src="images/'.$land_photo.'" alt="User Image"
                                        height="350" width="350" > <br /> ';
					                echo '<b> <i> Property photo </i> </b> <br />';
                                }
                            ?>


			            </td>
			            <td>
                    	    <label> Property Details   </label>
				            <hr />
                            <label> Code :  </label>
                            <?php echo $urow['land_code']; ?> 
                            <br />	
                            <label> Property Type :  </label>
                            <?php echo $urow['land_category']; ?>
                            <label> Property Usage :  </label>
                            <?php echo $urow['land_usage']; ?>
                            <br />
                    		<label> Location  :  </label>
                            <?php echo $urow['land_location']; ?>
                            <label > Status :</label>
                            <?php if ($status == 't'){echo "Available";}else{echo "Not Available";} ?> 
			                <br />
                            <label> Property Description :  </label>
                            <?php echo $urow['land_description']; ?>
                            <br />
                    		<label> Bedrooms : </label>
                    		<?php echo $urow['land_bedroom']; ?> 
                        	<label > Birthrooms  :</label>
                            <?php echo $urow['land_bathrooms']; ?> 
                        	<label > In a fense   :</label>
                        	<?php echo $urow['land_fense']; ?>
			                <br /> 
                            <label > Pricing  :</label>
                            <?php echo $urow['land_price']; ?>
                            <label > Inspection fee  :</label>
                            <?php echo $urow['land_inspection_fee']; ?>
                            <br />
                            <label > Published by :</label>
                            <?php echo $urow['land_createdby']; ?>
                            <label > Published on :</label>
                            <?php echo $urow['land_createdon']; ?>
                            <br /> 
                  	    </td>
                    </tr>
		            <tr>
                </table>
       	</div> <!-- end of the table -->

       	</div>
       		<div class="modal-footer justify-content-between">
       		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    <?php
        	echo '<button title="Click to print." class="btn btn-default" 
                      onclick="window.open('."'".'Printing.php?error='.$org_code_link."','_blank','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=400')".'" > <i class="fa fa-print"></i> Print </button>  ';
	    ?>

       	 	</div>
        </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



    <!-- Begin form for adding user -->
    <div class="modal fade" id="edit<?php echo $land_code_link;?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            	<div class="modal-header bg-success disabled color-palette">
              	   <h4 class="modal-title"> Editing Property Details </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
                <div class="modal-body">

                <?php
                    $edit=pg_query($conn,"SELECT * FROM land WHERE land_code_link ='".$land_code_link."'");
                    $urow=pg_fetch_array($edit);
			        (string) $GLOBALS['land_photo'] = $urow['land_photo'];
           	    ?>

                <form action="../lands/" method="post" enctype="multipart/form-data">
                <div class="box-body">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table table-bordered mb-0">
	                        <tr>
                                <td colspan="4">
                                    <div class="form-group">
                                        <input type="hidden" maxlength="50" name="land_code_link" class="form-control"
                                        value="<?php echo $urow['land_code_link']; ?>" required>
                                        <?php
                                            if (!empty($land_photo)){
                                                echo '  <img src="images/'.$land_photo.'" alt="User Image"
                                                    height="50" width="50" >
                                                    <font color="red"> Property photo </font>
                                                    <input name="photo_url" type="hidden" value='.$land_photo. '> <br />
                                                    <input name="land_photo" type="file" id="exampleInputFile" >';
                                            }else{
                                                echo '<input name="land_photo" type="file" id="exampleInputFile" >';
                                            }
                                        ?>

                                    </div>
                            </td>
                </tr>
                <tr>

                <td width="25%" >
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Property Category : </label>
                        <select name="land_category" class="form-control select2" style="width: 100%;" required>
                            <option value="" selected> Click to select </option>
                            <?php
                            $sql = "SELECT * FROM property_category ORDER BY name ASC";
                            $result = pg_query($conn, $sql);
                            if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                    $name = $row['name']; ?>
                                    <option value="<?php echo $name; ?>"
                                    <?php if ($urow['land_category'] == $name){ echo "selected";} ?>>
                                    <?php  echo $name; ?> </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>

                <td width="25%" >
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Property Usage : </label>
                        <select name="land_usage" class="form-control select2" style="width: 100%;" required>
                            <option value="" selected> Click to select </option>
                            <?php
                            $sql = "SELECT * FROM property_usage ORDER BY name ASC";
                            $result = pg_query($conn, $sql);
                            if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                    $name = $row['name']; ?>
                                    <option value="<?php echo $name; ?>"
                                    <?php if ($urow['land_usage'] == $name){ echo "selected";} ?> >
                                    <?php  echo $name; ?> </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>

                <td width="25%" >
                    <div class="form-group">
                        <label for="exampleInputPassword1"> District : </label>
                        <select name="land_district" class="form-control select2" style="width: 100%;" required>
                            <option value="" selected> Click to select </option>
                            <?php
                            $sql = "SELECT * FROM district ORDER BY district_name ASC";
                            $result = pg_query($conn, $sql);
                            if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                    $name = $row['district_name']; ?>
                                    <option value="<?php echo $name; ?>" 
                                    <?php if ($urow['land_district'] == $name){ echo "selected";} ?> >
                                    <?php  echo $name; ?> </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>

                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Property Location :</label>
                        <input type="text" maxlength="50" name="land_location" class="form-control"
                        placeholder="E.g. Kakoba " value="<?php echo $urow['land_location']; ?>" required>
                    </div>
                </td>

            </tr>
            <tr>
                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Property Description :</label>
                        <input type="text" maxlength="100" name="land_description" class="form-control"
                            value="<?php echo $urow['land_description']; ?>"  
                            placeholder="Enter property description here ... " required>
                    </div>
                </td>
                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Size  :</label>
                        <input type="text" maxlength="100" name="land_size" class="form-control"
                            placeholder="Please enter land size e.g. 12 Decimals or acres etc ..." 
                            value="<?php echo $urow['land_size']; ?>" 
                            title="Please enter land size e.g. 12 Decimals or acres etc ..." required>
                    </div>
                </td>
                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Property Pricing :</label>
                        <input type="text" maxlength="100" name="land_price" class="form-control"
                            value="<?php echo $urow['land_price']; ?>"
                            placeholder="Enter price here e.g. UGX.20,000,000 ... " required>
                    </div>
                </td>

                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Inspection Fees :</label>
                        <input type="text" maxlength="100" name="land_inspection" class="form-control"
                            value="<?php echo $urow['land_inspection']; ?>"
                            placeholder="Enter fee here e.g. UGX. 20000 " style="width: 100%;"  required>
                    </div>
                </td>

            </tr>
            <tr>

                <td colspan="4" >
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Property Contact Person : </label>
                        <select name="land_contact_person" class="form-control select2" style="width: 50%;" required>
                            <option value="" selected> Click to select </option>
                            <?php
                                $userIn = $_SESSION['login_user'];
                                if ($_SESSION['companyusertype'] == 'Agents'){
                                    $sql = "SELECT * FROM company
                                        WHERE companyusertype ='Agents' AND companyaccountname = '$userIn'
                                        ORDER BY companyfullname ASC";
                                }else{
                                    $sql = "SELECT * FROM company ORDER BY companyfullname ASC";
                                }
                                $result = pg_query($conn, $sql);
                                if (pg_num_rows($result) > 0) {
                                    while($row = pg_fetch_assoc($result)) {
                                        $email = $row['companyemail'];
                                        $name = $row['companyfullname']; ?>
                                        <option value="<?php echo $email; ?>"
                                        <?php if ($urow['land_contact_person'] == $name){ echo "selected";} ?> >
                                        <?php  echo $name; ?> </option>
                                <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </td>


            </tr>

		</table>

            </div>
                </div>
                <!-- /.box-body -->
              </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="update_land" class="btn btn-success"> Update </button>
              </form>
            </div>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->

<script type="text/javascript">
        $(document).ready(function(){

            $("#sel_district").change(function(){
                var district_code = $(this).val();

                $.ajax({
                    url: 'getCounty.php',
                    type: 'post',
                    data: {district_code:district_code},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_county").empty();
                        for( var i = 0; i<len; i++){
                            var county_code = response[i]['county_code'];
                            var county_name = response[i]['county_name'];

                            $("#sel_county").append("<option value=''>--- Select County ---</option>");
                            $("#sel_county").append("<option value='"+county_code+"'>"+county_name+"</option>");

                        }
                    }
                });
            });

        });
</script>


    <script type="text/javascript">
        $(document).ready(function(){

            $("#sel_county").change(function(){
                var county_code = $(this).val();

                $.ajax({
                    url: 'getsubCounty.php',
                    type: 'post',
                    data: {county_code:county_code},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_subcounty").empty();
                        for( var i = 0; i<len; i++){
                            var sub_county_code = response[i]['sub_county_code'];
                            var sub_county_name = response[i]['sub_county_name'];
                            
                            $("#sel_subcounty").append("<option value='"+sub_county_code+"'>"+sub_county_name+"</option>");

                        }
                    }
                });
            });

        });
    </script>


    <script type="text/javascript">
        $(document).ready(function(){

            $("#sel_subcounty").change(function(){
                var sub_county_code = $(this).val();

                $.ajax({
                    url: 'getParish.php',
                    type: 'post',
                    data: {sub_county_code:sub_county_code},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_parish").empty();
                        for( var i = 0; i<len; i++){
                            var parish_code = response[i]['parish_code'];
                            var parish_name = response[i]['parish_name'];

                            $("#sel_parish").append("<option value='"+parish_code+"'>"+parish_name+"</option>");

                        }
                    }
                });
            });

        });
    </script>


    <script type="text/javascript">
        $(document).ready(function(){

            $("#sel_parish").change(function(){
                var parish_code = $(this).val();

                $.ajax({
                    url: 'getVillage.php',
                    type: 'post',
                    data: {parish_code:parish_code},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#sel_village").empty();
                        for( var i = 0; i<len; i++){
                            var village_code = response[i]['village_code'];
                            var village_name = response[i]['village_name'];
                            $("#sel_village").append("<option value='"+village_code+"'>"+village_name+"</option>");

                        }
                    }
                });
            });

        });
    </script>


