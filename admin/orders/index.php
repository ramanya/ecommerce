<?php
	session_start();
	require_once'../library/config.php';
 	logout_session($conn); //Check wheather user logged in
	auto_logout($conn); //Logout iddle user
    $GLOBALS['app'] = "slider";
    $GLOBALS['nav'] = "lands";
    $GLOBALS['error'] = "";

    #Uploading symbol photo
    if(isset($_FILES['land_photo'])){
        $errors= array();
        $file_names = $_FILES['land_photo']['name'];
        $file_sizes =$_FILES['land_photo']['size'];
        $file_tmps =$_FILES['land_photo']['tmp_name'];
        $file_types=$_FILES['land_photo']['type'];
        //$file_ext=strtolower(end(explode('.',$_FILES['org_profile_symbol']['name'])));
        $file_exts = pathinfo($_FILES["land_photo"]["name"])['extension'];
        photo_upload($errors,$file_names,$file_sizes,$file_tmps,$file_types,$file_exts);
    }

    # Processing Religious Institute
    if(isset($_POST['land'])){
	$land_code = randomString();
	$land_photo = $_FILES['land_photo']['name'];
    $land_category = test_input($_POST['land_category']);
    $land_usage = test_input($_POST['land_usage']);
    $land_district = test_input($_POST['land_district']);
    $land_location = test_input($_POST['land_location']);
    $land_size = test_input($_POST['land_size']);
    $land_description = test_input($_POST['land_description']);
    $land_price = test_input($_POST['land_price']);
    $land_inspection = test_input($_POST['land_inspection']);
    $land_contact_person = test_input($_POST['land_contact_person']);
    $link = sha1($land_code);
    $user = test_input($_SESSION['user']);

    $sql = "SELECT * FROM company WHERE companyemail = '$land_contact_person'";
    $result = pg_query($conn,$sql);
    $row = pg_fetch_assoc($result);
    $count = pg_num_rows($result);
    // If result matched
    if($count > 0) {
        $GLOBALS['land_contact'] = $row['companyfullname'];
        $GLOBALS['land_contact_email'] = $row['companyemail'];
        $GLOBALS['land_contact_phone'] = $row['companyphone'];
    }


    $sql = "SELECT * FROM land WHERE  land_size= '$land_size'
        AND land_location = '$land_location'
        AND land_district = '$land_district'";
    $result = pg_query($conn,$sql);
    $row = pg_fetch_assoc($result);
    $count = pg_num_rows($result);
    // If result matched 
    if($count > 0) {
        $type = "-- warning --";
        $GLOBALS['error'] = "Invalid! This property  " .$land_location. " already exists.";
        $msg = $error;
        syslogs($conn,$ip,$msg,$user,$type);
        $GLOBALS['land'] = $row['land_code_link'];
    }else{
        $sql = "INSERT INTO land (land_code,land_category,land_usage,land_district,land_location,
                land_size,land_description,land_price,land_inspection,land_contact_person,
                land_contact_phone,land_contact_email,land_photo,land_createdby,land_code_link) 
                VALUES ('$land_code','$land_category','$land_usage','$land_district',
                '$land_location','$land_size','$land_description','$land_price','$land_inspection',
                '$land_contact','$land_contact_phone','$land_contact_email','$land_photo','$user','$link')";
         $result = pg_query($conn,$sql);

         if ($result){
            $ip = ip_add();
            $type = "-- normal --";
            $GLOBALS['error'] = "Land Property has been added by  " .$user. ".";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
            $GLOBALS['land'] = $link;
            #Tab activation
            $GLOBALS['tab'] = "1";
         }else{
            $ip = ip_add();
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! Your request has failed try again.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
            $GLOBALS['land'] = $link;
            #Tab activation
            $GLOBALS['tab'] = "1";
        }
    }
    }

    # Processing editing  
    if(isset($_POST['update_land'])){
        $link = test_input($_POST['land_code_link']);
        $land_photo = $_FILES['land_photo']['name'];
        $land_category = test_input($_POST['land_category']);
        $land_usage = test_input($_POST['land_usage']);
        $land_district = test_input($_POST['land_district']);
        $land_location = test_input($_POST['land_location']);
        $land_size = test_input($_POST['land_size']);
        $land_description = test_input($_POST['land_description']);
        $land_price = test_input($_POST['land_price']);
        $land_inspection = test_input($_POST['land_inspection']);
        $land_contact_person = test_input($_POST['land_contact_person']);
        $user = test_input($_SESSION['user']);
		$photo_url = test_input($_POST['photo_url']);
        $ip = ip_add();

        if (empty($land_photo)){
            $land_photo = $photo_url;
        }

        $sql = "SELECT * FROM company WHERE companyemail = '$land_contact_person'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched 
        if($count > 0) {
            $GLOBALS['land_contact'] = $row['companyfullname'];
            $GLOBALS['land_contact_email'] = $row['companyemail'];
            $GLOBALS['land_contact_phone'] = $row['companyphone'];
        }

        $sql = "UPDATE land SET land_category = '$land_category',
            land_photo = '$land_photo',
            land_usage = '$land_usage',
            land_district = '$land_district',
            land_location = '$land_location',
            land_description = '$land_description',
            land_size = '$land_size',
            land_price = '$land_price',
            land_inspection = '$land_inspection',
            land_contact_person = '$land_contact',
            land_contact_phone = '$land_contact_phone',
            land_contact_email = '$land_contact_email',
            land_lastupdatedon = now(),
            land_lastupdatedby = '$user'
            WHERE land_code_link = '$link'";
        $result = pg_query($conn,$sql);
        $tab = '1';
        $type = "-- Normal --";
        $GLOBALS['error'] = " Land Property in ".$land_location." has been updated.";
        $msg = $error;
        syslogs($conn,$ip,$msg,$user,$type);
        $GLOBALS['land'] = $link;
    }

 	require_once'../header.php';
 	require_once'../navigation.php';
 	include('body.php');
 	require_once'../footer.php';
?>

