<style>

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 7px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Order Content Manager </h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
	    <?php
		    //For alert messages
        	if(strlen($error) > 0){
			    echo '	<div class="alert alert-success" role="alert" data-auto-dismiss="3000">
  					        '.$error.'
				        </div>';
		    }
	    ?>

	<table id="cci" class="table table-bordered ">
        <tr>
	        <td>

		    <div class="tab">
                <button class="tablinks" onclick="openCity(event, 'order')" id="defaultOpen"> 
                    Ordered products and their details </button>
<!-- 
                <button class="tablinks" onclick="openCity(event, 'messages')" >
                    Messages </button> -->

		    </div>

		        <div id="order" class="tabcontent">
  			        <h4> Ordered products  
                    <?php
                        $cci = $_SESSION['cci'];
                        $access = $_SESSION['privilege'];
                        if  (strstr($access,'INSERT') && empty($cci)){
                            echo "<button class='button button2' data-toggle='modal' >
                                  <i class='fa fa-plus'></i> </button>";
                        }else{
                            echo "<button class='button button4' data-toggle='modal'  disabled>
                                <i class='fa fa-plus'></i> </button>";
                        }
                    ?>

			        </h4>

                	<table id="example1" class="table table-bordered ">
                        <thead>
                        <tr>
                            <th> Id</th>
                            <th> Code </th>
                            <th> Icon </th>
                            <th>Product Name </th>
                            <th>Customer Email </th>
                            <th> Customer Phone </th>
                            <th> Customer Name </th>
                            <th align='center'> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $cci = $_SESSION['cci'];
                            $access = $_SESSION['privilege'];
                            if (strstr($access,'READ')){
                                $root = "SELECT * FROM orders ORDER BY product_createdon DESC";
                            }
                            $res1 = pg_query($conn, $root);
                            if ($res1) {
                                // output data of each row
                                $num = 1;
                                while($row1 = pg_fetch_assoc($res1)) {
                                    (string) $id = $row1['product_code_link'];
                                    (string) $status = $row1['land_availability'];
                                    (string) $land_photo = $row1['product_photo'];
                                    (string) $land_code_link = $row1['product_code_link'];
                                    echo "<tr class='text-sm'>";
                                    echo "	<td width='4%'> " . $num . "  </td>";
                                    echo "	<td> " . $row1['product_code_link']. "  </td>";
                                    echo "<td>";
                                    if (!empty($land_photo)){
                                        echo '  <img src="../products/images/'
                                                .$land_photo.'" alt="User Image" width="30" height="30">';
                                    }else{
                                        echo '  <img src="images/img_default.png" alt="Avatar" 
                                               width="30" height="30">';
                                        }

                                    echo "</td>";
                                    echo "  <td> " . $row1['product_name'] . " </td>";
                                    echo "  <td> " . $row1['client_email'] . " </td>";
                                   
                                    echo "  <td> " . $row1['client_phone'] . " </td>";
                                    echo "  <td> " . $row1['product_createdby'] . " </td>";


                                    echo '  <td width="25%">';
                                   

                                    #Open scheme Details after adding record
                                    if (!empty($land)){
                                        echo "  <script type='text/javascript'>
                                                    $(document).ready(function(){
                                                        $('#view".$land."').modal('show');
                                                    });
                                                 </script>";
                                    }

                                  

                                    if  (!empty(strstr($access,'DELETE'))){
                                        echo '<a href="land_delete.php?error='. $id.'"
                                            onclick="return confirm(\'Are you sure to delete?\')">
                                            <button class="button button3" >
                                            <i class="fa fa-trash"> </i> Delete </button> </a>';
                                    }else{
                                        echo '<a href="#land_delete.php?error='. $id.'"
                                            onclick="return confirm(\'Are you sure to delete?\')">
                                            <button class="button button5" disabled>
                                            <i class="fa fa-trash"> </i> Delete </button> </a>';
                                    }

                                    include('edit_lands.php');
                                    echo '</td>';
                                    $num++;
                                    echo '</tr>';
                                }
                            } else {
                                echo "<tr>";
                                echo "<td colspan='8'> No Orders listed yet! </td>";
                                echo "</tr>";
                            }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th> Id</th>
                            <th> Code </th>
                            <th> Icon </th>
                               <th>Product Name </th>
                            <th>Customer Email </th>
                            <th> Customer Phone </th>
                            <th> Customer Name </th>
                            <th align='center'> Action </th>

                        </tr>
                        </tfoot>
                 	</table>

		        </div>
		
                <div id="messages" class="tabcontent">
                        <h3>Messages</h3>


                	<table id="message" class="table table-bordered ">
                        <thead>
                        <tr>
                            <th> Id</th>
                            <th> From </th>
                            <th> To </th>
                            <th> Subject </th>
                            <th> Message </th>
                            <th> SentOn </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $cci = $_SESSION['cci'];
                            $access = $_SESSION['privilege'];
                            if (strpos($access,'READ') && (empty($cci))){
                                $root = "SELECT * FROM  app_messages ORDER BY id DESC LIMIT 500";
                            }
                            $res1 = pg_query($conn, $root);
                            if ($res1) {
                                // output data of each row
                                $num = 1;
                                while($row1 = pg_fetch_assoc($res1)) {
                                    (string) $id = $row1['app_messages_code_link'];
                                    (string) $app_messages_code_link = $row1['app_messages_code_link'];
                                    echo "<tr class='text-sm'>";
                                    echo "	<td width='4%'> " . $num . "  </td>";
                                    echo "	<td> " . $row1['app_messages_from']. "  </td>";
                                    echo "	<td> " . $row1['app_messages_to']. "  </td>";
                                    echo "	<td> " . $row1['app_messages_subject']. "  </td>";
                                    echo "	<td> " . $row1['app_messages_message']. "  </td>";
                                    echo "	<td> " . $row1['app_messages_createdon']. "  </td>";
                                    echo '  <td width="10%">';
                                    echo '  <a class="button button2" href="#reply'.$app_messages_code_link.'" data-toggle="modal">
                                            <i class="fa fa-reply"></i> Reply</a>';

                                    include('reply_message.php');
                                    echo '</td>';
                                    $num++;
                                    echo '</tr>';
                                }
                            } else {
                                echo "<tr>";
                                echo "<td colspan='6'> No incoming message </td>";
                                echo "</tr>";
                                echo "<tr>";
                                echo "<td colspan='6' align='right'>";
                                echo "</td>";
                                echo "</tr>";
                            }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th> Id</th>
                            <th> From </th>
                            <th> To </th>
                            <th> Subject </th>
                            <th> Message </th>
                            <th> SentOn </th>
                            <th> Action </th>

                        </tr>
                        </tfoot>
                 	</table>

                </div>

            </div>

		</td>
    </tr>
    </table>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Land Content Manager 
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- For tab navigation   -->
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>





      <!-- for scheme -->
      <div class="modal fade" id="modal-land">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title"> Publish Land Property  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../lands/" method="post" enctype="multipart/form-data">
            <div class="modal-body">

            <table class="table table-bordered  mb-0">
		    <tr class="text-sm">
                <td colspan="4">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Land Photo ( Size 73 × 73 pixels) :</label>
                        <input name="land_photo" type="file" id="exampleInputFile" required>
                    </div>

                </td>
            </tr>
            <tr class="text-sm"> 

                <td width="25%" >
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Property Category : </label>
                        <select name="land_category" class="form-control select2" style="width: 100%;" required>
                            <option value="" selected> Click to select </option>
                            <?php
                            $sql = "SELECT * FROM property_category ORDER BY name ASC";
                            $result = pg_query($conn, $sql);
                            if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                    $name = $row['name']; ?>
                                    <option value="<?php echo $name; ?>">
                                    <?php  echo $name; ?> </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>

                <td width="25%" >
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Property Usage : </label>
                        <select name="land_usage" class="form-control select2" style="width: 100%;" required>
                            <option value="" selected> Click to select </option>
                            <?php
                            $sql = "SELECT * FROM property_usage ORDER BY name ASC";
                            $result = pg_query($conn, $sql);
                            if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                    $name = $row['name']; ?>
                                    <option value="<?php echo $name; ?>">
                                    <?php  echo $name; ?> </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>


                <td width="25%" >
                    <div class="form-group">
                        <label for="exampleInputPassword1"> District : </label>
                        <select name="land_district" class="form-control select2" style="width: 100%;" required>
                            <option value="" selected> Click to select </option>
                            <?php
                            $sql = "SELECT * FROM district ORDER BY district_name ASC";
                            $result = pg_query($conn, $sql);
                            if (pg_num_rows($result) > 0) {
                                while($row = pg_fetch_assoc($result)) {
                                    $name = $row['district_name']; ?>
                                    <option value="<?php echo $name; ?>">
                                    <?php  echo $name; ?> </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>

                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Property Location :</label>
                        <input type="text" maxlength="50" name="land_location" class="form-control" 
                            placeholder="E.g. Kakoba " required> 
                    </div>
                </td>

            </tr>
            <tr class="text-sm">
                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Property Description :</label>
                        <input type="text" minlength="35" maxlength="100" name="land_description" class="form-control" 
                            placeholder="Enter property description here ... " 
                            title="Please indicate the distance to the site from the nearest town center" required> 
                    </div>
                </td>
                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Size :</label>
                        <input type="text" maxlength="100" name="land_size" class="form-control"
                            placeholder="Enter land size e.g. 15 Decimals or acres etc ... " 
                            title="Please enter property size" required>
                    </div>
                </td>

                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Property Pricing :</label>
                        <input type="text" maxlength="100" name="land_price" class="form-control"
                            placeholder="Enter price here e.g. UGX.20,000,000 ... " required>
                    </div>
                </td>

                <td width="25%">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Inspection Fees :</label>
                        <input type="text" maxlength="100" name="land_inspection" class="form-control"
                            placeholder="Enter fee here e.g. UGX. 20000 " style="width: 100%;"  required>
                    </div>
                </td>


            </tr>
            <tr class="text-sm">

                <td colspan="4" >
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Property Contact Person : </label>
                        <select name="land_contact_person" class="form-control select2" style="width: 50%;" required>
                            <option value="" selected> Click to select </option>
                            <?php
                                $userIn = $_SESSION['login_user'];
                                if ($_SESSION['companyusertype'] == 'Agents'){
                                    $sql = "SELECT * FROM company
                                        WHERE companyusertype ='Agents' AND companyaccountname = '$userIn'
                                        ORDER BY companyfullname ASC";
                                }else{
                                    $sql = "SELECT * FROM company ORDER BY companyfullname ASC";
                                }
                                $result = pg_query($conn, $sql);
                                if (pg_num_rows($result) > 0) {
                                    while($row = pg_fetch_assoc($result)) {
                                        $email = $row['companyemail'];
                                        $name = $row['companyfullname']; ?>
                                        <option value="<?php echo $email; ?>">
                                        <?php  echo $name; ?> </option>
                                <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </td>

            </tr>
	        </table>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="land" class="btn btn-info"> Publish </button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



<!-- Java Scripts for tabs on enrollment   -->
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<!-- for table search and pagination -->
<script>
  $(function () {
    $("#message").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


