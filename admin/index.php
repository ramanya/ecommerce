<?php

    session_start();
    require_once'library/config.php';
    $GLOBALS['error'] = "";
	$phone="";
    # Get IP 
	$ip = ip_add();

    if(isset($_POST['change_passcode'])){
        $account = test_input($_POST['account']);
        $old_passcode = test_input($_POST['old_passcode']);
        $new_passcode = test_input($_POST['new_passcode']);
        $cfm_passcode = test_input($_POST['cfm_passcode']);
 
        if ($new_passcode != $cfm_passcode){
            $GLOBALS['error'] = "Invalid! New Password did not match";
            $msg = $error;
            $type = "-- warning --";
            syslogs($conn,$ip,$msg,$user,$type);
		
			# Login again	
			session_destroy();
         }else{
			$old_passcode = sha1($old_passcode);
                	$sql = "SELECT companyemail,company_domain FROM company 
                                WHERE companyaccountname = '$account' 
				AND passcode = '$old_passcode' ";
                        // If result matched $myusername and $mypassword, table row must be 1 row
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                        // output data of each row
                        	while($row = pg_fetch_assoc($result)) {
                                	$email = $row['companyemail'];
                                	$ln = $row['company_domain'];
                                        $msg = "Your new password is: ";
					
					#Update the password
					$code = sha1($new_passcode);
                    			$pwd = "UPDATE company SET passcode = '$code',
                        			companyneverloggedin = false,
                        			cfmpasscode = '$code',
                        			company_last_updated_on = now() 
						WHERE companyemail = '$email'";
					$res = pg_query($conn,$pwd);

                                        #Calling the email function
                                	$type = "-- normal --";
                                	$user = $email;
                                	syslogs($conn,$ip,$msg,$type,$user);

                                	$lnk = $ln;
                                	$sub = "Password Recovery: ";
                                	$to = $email;
                                	$phone = $phone;
                                	$new_pwd = $new_passcode;
                                	$msg = "Your new password is : ". $new_pwd ."\n The link is ".$lnk;
                                	//Sending email 
                                        $from = 'noreply@gemcs.biz';
                                        send_email($conn,$sub,$msg,$to,$from);
					$GLOBALS['error'] = "New Password has been sent to your email.";
					//Desstroy current sessionn	
					session_destroy();
                                }
                        }else{
                        	$GLOBALS['error'] = "Invalid! Old Password";
                        	$msg = $error;
                        	$type = "-- warning --";
                        	syslogs($conn,$ip,$msg,$user,$type);

				#Login again
				session_destroy();
			}
                }
        }

        // End of form for recovering password

        # Form for recovering password 
        if(isset($_POST['recover_pwd'])){
                $email = test_input($_POST['email']);
                $new_pwd = randomString();

                if (empty($email)){
                        $GLOBALS['error'] = "Invalid! Try again";
                        $msg = $error;
                        $type = "-- warning --";
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                	$sql = "SELECT companyemail,company_domain FROM company 
                                WHERE companyemail = '$email'";
                        // If result matched $myusername and $mypassword, table row must be 1 row
                        $result = pg_query($conn, $sql);
                        if (pg_num_rows($result) > 0) {
                        // output data of each row
                        	while($row = pg_fetch_assoc($result)) {
                                	$email = $row['companyemail'];
                                	$ln = $row['company_domain'];
                                        $msg = "Your new password is: ";
					
					#Update the password
					$code = sha1($new_pwd);
					$pwd = "UPDATE company SET passcode = '$code',
						cfmpasscode = '$code' 
						WHERE companyemail = '$email'";
					$res = pg_query($conn,$pwd);

                                        #Calling the email function
                                	$type = "-- normal --";
                                	$user = $email;
                                	syslogs($conn,$ip,$msg,$type,$user);

                                	$lnk = $ln;
                                	$sub = "Password Recovery";
                                	$to = $email;
                                	$phone = $phone;
                                	$new_pwd = $new_pwd;
                                	$msg = "Your new password is : ". $new_pwd ."\n The link is ".$lnk;
                                	//Sending email to the new staff added
                                        $from = 'noreply@gemcs.biz';
                                        send_email($conn,$sub,$msg,$to,$from);

                                	$GLOBALS['error'] = "Recovery token has been sent to your email.";
                                }
                        }
                }
        }

        // End of form for recovering password

    // Login processing 
    if(isset($_POST['login'])){
        // username and password sent from form 
        $myusername = $_POST['username'];
        $mypassword = sha1($_POST['password']);

        if (empty($myusername)){
            $GLOBALS['error'] = "Username or email is required";
            $msg = $error;
            $user = "anonymous";
            $type = "-- warning --";
            syslogs($conn,$ip,$msg,$user,$type);
        }

        if ((strlen($myusername > 100)) || (strlen($myusername) < 10)){
            $GLOBALS['error'] = "Invalid Username or Email! Try again.";
            $msg = $error;
            $type = "-- warning --";
            $user = "anonymous";
            syslogs($conn,$ip,$msg,$user,$type);
        }

        if (empty($mypassword)){
            $GLOBALS['error'] = "Password is required at login";
            $msg = $error;
            $type = "-- warning --";
            $user = "anonymous";
            syslogs($conn,$ip,$msg,$user,$type);
        }

        if (!empty($myusername) && !empty($mypassword)){
            $sql = "SELECT * FROM company 
                WHERE companyuserisactive = true 
                AND companyemail = '$myusername' AND passcode = '$mypassword'";
            $result = pg_query($conn, $sql);
            if (pg_num_rows($result) > 0) {
                // output data of each row
                while($row = pg_fetch_assoc($result)) {

                    $everloggedin = $row['companyneverloggedin'];
                    $account = $row['companyaccountname'];
                    $last_update = substr($row['company_last_updated_on'],0,10);
                    $today = date("Y-m-d");

                    if ($everloggedin == 't'){
                        $GLOBALS['changePass'] = $account;
                    }else{

                        #Check the last time the password was updated
                        $diff=date_diff(date_create($last_update),date_create($today));
                        #Convert date diff to int
                        $diff = intval($diff->format("%R%a"));
                        if ($diff > 45){
                            $query = "UPDATE company SET 
                                companyneverloggedin = TRUE
                                WHERE companyemail = '$myusername'";
                            $res = pg_query($conn,$query);
                        }
				        #Login
				        login($conn,$myusername,$mypassword);
                    }
                }
            }else{
                $GLOBALS['error'] = "Invalid! Check your username and password.";
                $GLOBALS['errors'] = "User trying to login with incorrect username and password";
                $msg = $errors;
                $type = "-- warning --";
                $user = "anonymous";
                syslogs($conn,$ip,$msg,$user,$type);
            }
        }
    }

        # Login ends here
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> CKil Staff Portal </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

  <!-- For Modal pop up -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<?php
    if (!empty($changePass)){ 
        echo "<script>
	            $(document).ready(function(){
		            $('#passcodeChange').modal('show');
	            });
            </script>";
    }
?>

</head>
<body class="hold-transition login-page" oncontextmenu="return false">
<center> <h2 class="text-muted">  CKil Staff Portal </h2>
</center>
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <!-- display error messages -->
      <center> <small> <font color="red"><?php echo $error; ?> </font> </small> </center>
      <form action="index.php" method="post">
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" name="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" name="login" class="btn btn-info btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mb-1">
	<a data-target="#modal-default" data-toggle="modal" class="MainNavText" id="MainNavHelp" 
        href="#myModal">I forgot my password </a>

      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

</body>
</html>

      <!-- forgotten password -->
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Please enter your email to recover your password! </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
	    
	    <form action="index.php" method="post">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
               </div>
                 <input type="email" class="form-control" name="email" placeholder="Email" maxlength="50" required>
             </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="recover_pwd" class="btn btn-primary"> Recover Password </button>

            </div>
	    </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->




      <!-- for changing password -->
        <div id="passcodeChange" class="modal fade">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">

              <h4 class="modal-title"> <i class="fa fa-lock"></i>  Change Password  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="index.php" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <input type="hidden" name="account" value="<?php echo $changePass; ?>"
                            class="form-control" >
                            <input minlength="4" maxlength="15" type="password" name="old_passcode" class="form-control" placeholder="Enter Old Password "  required>
                    </div>

                        <div class="form-group">
                            <input minlength="4" maxlength="15" type="password" name="new_passcode" class="form-control" 
                                placeholder="Enter New Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
                                title="Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters" required>
                        </div>

                        <div class="form-group">
                                <input minlength="4" maxlength="15" type="password" name="cfm_passcode" class="form-control"  
                                placeholder="Confirm New Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
                                title="Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters"  required>
                        </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="change_passcode" class="btn btn-info"> Continue </button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

