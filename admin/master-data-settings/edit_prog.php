
        <!-- Begin form for adding user -->
        <div class="modal fade" id="edit<?php echo $study_program_code_link; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
            	<div class="modal-header">
              	   <h4 class="modal-title">Editing Program / Course </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM study_program WHERE study_program_code_link ='".$study_program_code_link."'");
                        $erow=pg_fetch_array($edit);
           	?>


                <form method="POST" action="../master-data-settings/">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Name :</label>

                        <input  type="hidden" name="study_program_code_link" class="form-control" value="<?php echo $erow['study_program_code_link']; ?>" 
                                id="inputdefault" style="width: 100%;"  required>

                        <input 	type="text" name="study_program_name" class="form-control" value="<?php echo $erow['study_program_name']; ?>" 
				id="inputdefault" style="width: 100%;"  required>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="update_study_program" class="btn btn-primary">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->
