        <!-- Editing A level subjects -->
        <div class="modal fade" id="oedit<?php echo $olevel_subjects_code_link; ?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-success disabled color-palette bg-success disabled color-palette">
                   <h4 class="modal-title">Editing Subject </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

                <?php
                        $edit=pg_query($conn,"SELECT * FROM olevel_subjects WHERE olevel_subjects_code_link ='".$olevel_subjects_code_link."'");
                        $erow=pg_fetch_array($edit);
                ?>


                <form method="POST" action="../master-data-settings/">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Name :</label>

                        <input  type="hidden" name="olevel_subjects_code_link" class="form-control" value="<?php echo $erow['olevel_subjects_code_link']; ?>"
                                id="inputdefault" style="width: 100%;"  required>

                        <input  type="text" name="olevel_subjects_name" class="form-control" value="<?php echo $erow['olevel_subjects_name']; ?>"
                                id="inputdefault" style="width: 100%;"  required>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer justify-content-between justify-content-between">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="update_olevel_subjects" class="btn btn-success">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->
