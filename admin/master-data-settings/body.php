  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Master Data Settings </h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

                <?php
                        if(strlen($error) > 0){
                                echo '
                                        <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" 
                                        aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i> Alert!</h4>' .$error . '
                                        </div>';
                        }
                ?>



		<table id="cci" class="table table-bordered ">
              		<tr>
			<td>
			<?php
			   $tab_nation = $_GET["tab_nationality"];
			   $tab_tribe = $_GET["tab_tribe"];
			   $tab_religion = $_GET["tab_religion"];
			   $tab_disability = $_GET["tab_disability"];
			   $tab_department = $_GET["tab_department"];
			?>
            <button class="tablink" onclick="openPage('root', this, '#04812F')" 
                <?php if ($tab_nation == '1'){ echo 'id="defaultOpen"';} ?>> 
			   <i class="fas fa-circle nav-icon"></i>  Nationalities </button>
            <button class="tablink" onclick="openPage('Home', this, '#04812F')" 
                <?php if ($tab_tribe == '1'){ echo 'id="defaultOpen"';} ?>>
			   <i class="fas fa-circle nav-icon"></i> Ethnic Backbround </button>
            <button class="tablink" onclick="openPage('News', this, '#04812F')" 
                <?php if ($tab_religion == '1'){ echo 'id="defaultOpen"';} ?>> 
			   <i class="fas fa-circle nav-icon"></i> Religions </button>
            <button class="tablink" onclick="openPage('Contact', this, '#04812F')" 
                <?php if ($tab_department == '1'){ echo 'id="defaultOpen"';} ?>>
                <i class="fas fa-circle nav-icon"></i>  Disabilities </button>
			<button class="tablink" onclick="openPage('About', this, '#04812F')" 
			    <?php if ($tab_disability == '1'){ echo 'id="defaultOpen"';}else{echo 'id="defaultOpen"';} ?>> 
				<i class="nav-icon fas fa-cog"></i> More Settings </button>

           <div id="root" class="tabcontent">
                <h3>Nationality 
   	            <button class='button button2' data-toggle='modal' data-target='#modal-nation'>
                    <i class="fas fa-circle nav-icon"> </i> Add Nationality</button>
			    </h3>

              	    <table id="example1" class="table table-bordered table-striped">
 			            <thead>
                        <tr>
                	        <th>Id</th>
                		    <th>Country Name</th>
                		    <th>CreatedBy </th>
                		    <th>CreatedOn </th>
				            <th align='center'> Action </th>
                	    </tr>
                        </thead>
                	    <tbody>
  			            <?php	
			                $root = "SELECT * FROM nationality ORDER BY name ASC";
				            $res1 = pg_query($conn, $root);
				            if ($res1) {
    				            // output data of each row
				                $number = 1;
    				            while($row1 = pg_fetch_assoc($res1)) {
                                    (string) $id = $row1['id'];
                                    (string) $trash = $row1['nationality_trash'];
                                    (string) $is_active = $row1['id'];
                                    (string) $companyurl = $row1['id'];
       					            echo "<tr>";
       					            echo "<td width='5%'>" . $number . "</td>";
					                echo "<td>" . $row1['name'] . "</td>";
		               		        echo "<td>" . $row1['nationality_createdby'] . "</td>";
		               		        echo "<td>" . $row1['nationality_createdon'] . "</td>";
					                echo '<td>';
                                    echo ' <a class="button button" href="#edit'.$companyurl.'" data-toggle="modal">
                                           <i class="fa fa-edit"></i> Edit</a>';

					                if (!empty($name)){
					                    echo ' <button class="button button2" title="Disable account!" 
					 	                        onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						                        <i class="fa fa-user"> </i> Disable </button> ';
					                }else{
                                        echo ' <button class="button button2" title="Enable account!" 
                                               onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                               <i class="fa fa-user-times"> </i> Enable </button> ';
				                    }

                                    if (($trash == 'f')){
                                        echo '  <button class="button button5" title="Trash record!" 
                                                onclick="window.location.href ='."'".'nationality_trash.php?error='.$id."';".'"> 
                                                <i class="fa fa-trash"> </i> Trash </button> ';
                                    }else{
                            ?>
                                <a href="nationality_delete.php?error=<?php echo $id;?>" 
                                    onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                            <?php
                                    }

                                    include('edit_nationality.php');
                                    echo '</td>';
				                    $number++;
                                    echo '</tr>';
                                }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='3'> No Ecclesiastical Province configured in the system yet! </td>";
                                   echo "</tr>";
                  	       }
                	    ?>
                	        </tbody>
                	        <tfoot>
                	        <tr>
                                <th>Id</th>
                                <th>Country Name</th>
                                <th>CreatedBy </th>
                                <th>CreatedOn </th>
                                <th align='center'> Action </th>
			                </tr>
                	        </tfoot>
              		    </table>
                    </div>

			<div id="Home" class="tabcontent">
                           <h3> Ethnic Background
	                      <button class='button button2' data-toggle='modal' data-target='#modal-tribe'>
                	         <i class='fa fa-user-secret'> </i> Add Ethnic Background </button>
			   </h3>
              		   <table id="privilege" class="table table-bordered table-striped">
                	      <thead>
              		         <tr>
                		    <th>Id</th>
                		    <th> Ethnic Background </th>
                		    <th>Created By</th>
                		    <th>Created On</th>
				    <th align='center'> Action </th>
                		 </tr>
                		 </thead>
                		 <tbody>
  				 <?php	
				    $pr = "SELECT * FROM tribe ORDER BY tribe_name ASC";
				    $rs = pg_query($conn, $pr);
				    if ($rs) {
    				       // output data of each row
				       $number = 1;
    				       	while($rw = pg_fetch_assoc($rs)) {
                                           (string) $id = $rw['tribe_code_link'];
                                           (string) $tribe_code_link = $rw['tribe_code_link'];
                                           (string) $trash = $rw['tribe_trash'];
       					   echo "<tr>";
                                           echo "<td width='5%'>" . $number . "</td>";
					   echo "<td>" . $rw['tribe_name'] . "</td>";
					   echo "<td>" . $rw['tribe_createdby'] . "</td>";
					   echo "<td>" . $rw['tribe_createdon'] . "</td>";
					   echo "<td width='25%'>";
                                           echo ' <a class="button button" href="#edit'.$tribe_code_link.'" data-toggle="modal">
                                                  <i class="fa fa-edit"></i> Edit</a>';

					   if ($is_active == 't'){
                                              echo ' <button class="button button2" title="Disable account!" 
					             onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						     <i class="fa fa-user"> </i> Disable </button> ';
					   }else{
                                              echo ' <button class="button button4" title="Enable account!" 
                                                     onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                     <i class="fa fa-user-times"> </i> Enable </button> ';
					  }

                                          if ($trash == 'f'){
                                             echo ' <button class="button button5" title="Trash record!" 
                                                    onclick="window.location.href ='."'".'tribe_trash.php?error='.$id."';".'"> 
                                                    <i class="fa fa-trash"> </i> Trash </button> ';
                                          }else{
                                ?>
                                        <a href="tribe_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }
                                         include('edit_tribe.php');
                                         echo '</td>';

                                         $number++;
                                         echo '</tr>';
                                      }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> No tribes configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  		}
                		?>
                		</tbody>
                		<tfoot>
                		<tr>
                		<tr>
                        	   <th>Id</th>
                		   <th> Ethnic Background  </th>
                                   <th>Created By</th>
                                   <th>Created On</th>
                        	   <th align='center'> Action </th>
                                </tr>
                	        </tr>
                	        </tfoot>
              		     </table>
			</div>


			<div id="News" class="tabcontent">

                           <h3> Religion 
			      <button class='button button2' data-toggle='modal' data-target='#modal-religion'>
                	         <i class='fa fa-user-secret'> </i> Add Religion </button>
			   </h3>

			   <table id="religion" class="table table-bordered table-striped">
                	      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Religion</th>
                		 <th>Created By</th>
                		 <th>Created On</th>
				 <th align='center'> Action </th>
                	      </tr>
                	      </thead>
                	      <tbody>

			      <?php	
			         $roles = "SELECT * FROM religion ORDER BY religion_name ASC";
				 $result_role = pg_query($conn, $roles);
				 if ($result_role) {
    				   // output data of each row
				   $number = 1;
    				   while($row_role = pg_fetch_assoc($result_role)) {
                                      (string) $id = $row_role['religion_code_link'];
                                      (string) $religion_code_link = $row_role['religion_code_link'];
                                      (string) $trash = $row_role['religion_trash'];

                                      echo "<tr>";
                                      echo "<td width='5%'>" . $number . "</td>";
                                      echo "<td>" . $row_role['religion_name'] . "</td>";
				      echo "<td>" . $row_role['religion_createdby'] . "</td>";
				      echo "<td>" . $row_role['religion_createdon'] . "</td>";

				      echo "<td width='22%' >";
                                      echo  ' <a class="button button" href="#edit'.$religion_code_link.'" data-toggle="modal">
                                              <i class="fa fa-edit"></i> Edit</a>';

               			      if ($is_active == 't'){
                                         echo ' <button class="button button2" title="Disable account!" 
						onclick="window.location.href ='."'".'#disable.php?error='.$id."';".'"> 
						<i class="fa fa-user"> </i> Disable </button> ';
				      }else{
                                         echo ' <button class="button button4" title="Enable account!" 
                                                onclick="window.location.href ='."'".'#enable.php?error='.$id."';".'"> 
                                                <i class="fa fa-user-times"> </i> Enable </button> ';
				     }

                                     if ($trash == 'f'){
                                        echo ' <button class="button button5" title="Trash record!" 
                                               onclick="window.location.href ='."'".'religion_trash.php?error='.$id."';".'"> 
                                               <i class="fa fa-trash"> </i> Trash </button> ';
                                     }else{
                                ?>
                                        <a href="religion_delete.php?error=<?php echo $id;?>" 
                                                onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
                                <?php
                                      }
				    
                                     include('edit_religion.php');
				     echo '</td>';
				     $number++;
                                     echo '</tr>';
                                  }
                               } else {
                                  echo "<tr>";
                                  echo "<td colspan='6'> No religons configured in the system yet! </td>";
                                  echo "</tr>";
                                  echo "<tr>";
                                  echo "<td colspan='6' align='right'>";
                                  echo "</td>";
                                  echo "</tr>";
                  	       }
                	    ?>
                	    </tbody>
                	    <tfoot>
                	    <tr>
                	    <tr>
                               <th>Id</th>
                	       <th>Religion</th>
                               <th>Created By</th>
                               <th>Created On</th>
                               <th align='center'> Action </th>
                	    </tr>
                	    </tr>
                	    </tfoot>
              		 </table>
	              </div>
		
		      <div id="Contact" class="tabcontent">

                           <h3>Disabilities 
   	                      <button class='button button2' data-toggle='modal' data-target='#modal-disability'>
                	         <i class='fa fa-plus'> </i> Add Disability </button>
			   </h3>

              		   <table id="disability" class="table table-bordered table-striped">
 			      <thead>
                	      <tr>
                	         <th>Id</th>
                		 <th>Name </th>
                		 <th>Created By </th>
                		 <th>Created On </th>
				 <th align='center'> Action </th>
                	       </tr>
                	       </thead>
                	       <tbody>
  			       <?php	
			          $root = "SELECT * FROM disability ORDER BY disability_name ASC"; 
				  $res1 = pg_query($conn, $root);
				  if ($res1) {
    				     // output data of each row
				     $number = 1;
    				     while($row1 = pg_fetch_assoc($res1)) {
                                        (string) $id = $row1['disability_code_link'];
                                        (string) $trash = $row1['disability_trash'];
                                        (string) $disability_code_link = $row1['disability_code_link'];
       					echo "<tr>";
       					echo "<td width='5%'>" . $number . "</td>";

					echo "<td>" . $row1['disability_name'] . "</td>";
					echo "<td>" . $row1['disability_createdby'] . "</td>";
					echo "<td>" . $row1['disability_createdon'] . "</td>";
					echo '<td>';
                                        echo ' <a class="button button" href="#edit'.$disability_code_link.'" data-toggle="modal">
                                               <i class="fa fa-edit"></i> Edit</a>';

					if ($is_active == 't'){
					   echo ' <button class="button button2" title="Disable account!" 
					 	  onclick="window.location.href ='."'".'disable.php?error='.$id."';".'"> 
						  <i class="fa fa-user"> </i> Disable </button> ';
					}else{
                                           echo ' <button class="button button4" title="Enable account!" 
                                                  onclick="window.location.href ='."'".'enable.php?error='.$id."';".'"> 
                                                  <i class="fa fa-user-times"> </i> Enable </button> ';
				       }

                                      if ($trash == 'f'){
                                         echo ' <button class="button button5" title="Trash record!" 
                                                onclick="window.location.href ='."'".'dis_trash.php?error='.$id."';".'"> 
                                                <i class="fa fa-trash"> </i> Trash </button> ';
                                      }else{
				?>
                                        <a href="dis_delete.php?error=<?php echo $id;?>" 
						onclick="return confirm('Are you sure to delete?')"> <button class='button button3' > 
                                                <i class='fa fa-user-times'> Delete </i> </button> </a>
				<?php


                                      }

                                      include('edit_disability.php');
                                      echo '</td>';
				      $number++;
                                      echo '</tr>';
                                    }
                                } else {
                                   echo "<tr>";
                                   echo "<td colspan='6'> No Disabilities configured in the system yet! </td>";
                                   echo "</tr>";
                                   echo "<tr>";
                                   echo "<td colspan='6' align='right'>";
                                   echo "</td>";
                                   echo "</tr>";
                  	       }
                	      ?>
                	      </tbody>
                	      <tfoot>
                	      <tr>
              		      <tr>
                                 <th>Id</th>
                                 <th>Name </th>
                                 <th>Created By </th>
                                 <th>Created On </th>
                                 <th align='center'> Action </th>
			      </tr>
                	      </tr>
                	      </tfoot>
              		   </table>
		        </div>

		        <div id="About" class="tabcontent">
                    <h3>More Settings </h3>

	                <table id="" class="table table-bordered ">
        	            <tr>
                            <td width="50%">

  			                    <h4> Property Category
                                    <?php
                          	            echo "  <button class='button button2' data-toggle='modal' data-target='#modal-category'>
                                                <i class='fa fa-plus'></i> Add Property Category </button>";
                                    ?>
			                    </h4>

                	            <table id="category" class="table table-bordered table-striped">
                        	        <thead>
                        	            <tr>
                                	        <th> Id</th>
                               	 	        <th> Name </th>
                                	        <th align='center'> Action </th>
                        	            </tr>
                        	        </thead>
                        	        <tbody>
                        	            <?php
                                       	    $root = "SELECT * FROM property_category ORDER BY name ASC";
                                	        $res1 = pg_query($conn, $root);
                                	        if ($res1) {
                                        	    // output data of each row
                                        	    $numb = 1;
                                        	    while($row1 = pg_fetch_assoc($res1)) {
                                                    (string) $id = $row1['property_category_code'];
                                                    (string) $property_category_code = $row1['property_category_code'];
                                                    echo "<tr>";
                                                    echo "<td width='5%'>" . $numb . "</td>";

                                                    echo "<td>" . $row1['name'] . "</td>";
                                                    echo '<td width="20%">';

                                                    echo "<button class='button button2' data-toggle='modal' data-target='#modal-category'>
                                                            <i class='fa fa-plus'> </i> </button>";

                                                    echo ' <a class="button button" href="#edit'.$property_category_code.'" data-toggle="modal">
                                                            <i class="fa fa-edit"></i> </a>';
                                                    echo '  <a href="category_delete.php?error='. $id.'"
                                                            onclick="return confirm(\'Are you sure to delete?\')">
                                                            <button class="button button3" >
                                                            <i class="fa fa-trash"> </i> </button> </a>';

                                                	include('edit_category.php');
                                                	echo '</td>';
                                                	$numb++;
                                                	echo '</tr>';
                                        	    }
                                	        } 
                        	            ?>
                        	        </tbody>
                        	        <tfoot>
                        	            <tr>
                                            <th>Id</th>
                                            <th> Name </th>
					                        <th align='center'> Action </th>
                        	            </tr>
                        	        </tfoot>
                 	            </table>
			                </td>
			                <td>

  			                    <h4> Property Usage
                                    <?php
                          	            echo " <button class='button button2' data-toggle='modal' data-target='#modal-usage'>
                                                <i class='fa fa-plus'></i> Add Property Usage </button>";
                                    ?>
			                    </h4>

                	            <table id="diocese" class="table table-bordered table-striped">
                        	        <thead>
                        	            <tr>
                                	        <th> Id</th>
                               	 	        <th> Name </th>
                                	        <th align='center'> Action </th>
                        	            </tr>
                        	        </thead>
                        	        <tbody>
                        	            <?php
                                       	    $root = "SELECT * FROM property_usage ORDER BY name ASC";
                                	        $res1 = pg_query($conn, $root);
                                	        if ($res1) {
                                        	    // output data of each row
                                        	    $numb = 1;
                                                while($row1 = pg_fetch_assoc($res1)) {
                                                    (string) $id = $row1['property_usage_code'];
                                                    (string) $property_usage_code = $row1['property_usage_code'];

                                                	echo "<tr>";
                                                	echo "	<td width='5%'>" . $numb . "</td>";
                                                	echo "  <td>" . $row1['name'] . "</td>";
                                                    echo '  <td width="20%">';

                                                    echo "<button class='button button2' data-toggle='modal' data-target='#modal-usage'>
                                                            <i class='fa fa-plus'> </i> </button>";

                                                    echo '  <a class="button button" href="#edit'.$property_usage_code.'" data-toggle="modal">
                                                           	<i class="fa fa-edit"></i> </a>';
                                                    echo '  <a href="usage_delete.php?error='. $id.'"
                                                            onclick="return confirm(\'Are you sure to delete?\')">
                                                            <button class="button button3" >
                                                            <i class="fa fa-trash"> </i> </button> </a>';

                                                	include('edit_usage.php');
                                                	echo '</td>';
                                                	$numb++;
                                                	echo '</tr>';
                                        	    }
                                	        } 
                        	            ?>
                        	        </tbody>
                        	        <tfoot>
                        	            <tr>
                                            <th>Id</th>
                                            <th> Name </th>
					                        <th align='center'> Action </th>
                        	            </tr>
                        	        </tfoot>
                 	            </table>
			                </td>
		                </tr>

                </table>



                <table id="" class="table table-bordered ">
        	        <tr>
                        <td width="50%">

  			                <h4> Property Type
                                <?php
                          	        echo "  <button class='button button2' data-toggle='modal' data-target='#modal-type'>
                                        <i class='fa fa-plus'></i> Add Property Type </button>";
                                ?>
			                </h4>

                	            <table id="ecclesiastical" class="table table-bordered table-striped">
                        	        <thead>
                        	            <tr>
                                	        <th> Id</th>
                               	 	        <th> Name </th>
                                	        <th align='center'> Action </th>
                        	            </tr>
                        	        </thead>
                        	        <tbody>
                        	            <?php
                                       	    $root = "SELECT * FROM property_type ORDER BY name ASC";
                                	        $resp = pg_query($conn, $root);
                                	        if ($resp) {
                                        	    // output data of each row
                                        	    $numb = 1;
                                        	    while($rowp = pg_fetch_assoc($resp)) {
                                                    (string) $id = $rowp['property_type_code'];
                                                    (string) $property_type_code = $rowp['property_type_code'];
                                                    echo "<tr>";
                                                    echo "<td width='5%'>" . $numb . "</td>";

                                                    echo "<td>" . $rowp['name'] . "</td>";
                                                    echo '<td width="20%">';

                                                    echo "<button class='button button2' data-toggle='modal' data-target='#modal-type'>
                                                            <i class='fa fa-plus'> </i> </button>";

                                                    echo ' <a class="button button" href="#edit'.$property_type_code.'" data-toggle="modal">
                                                            <i class="fa fa-edit"></i> </a>';
                                                    echo '  <a href="type_delete.php?error='. $id.'"
                                                            onclick="return confirm(\'Are you sure to delete?\')">
                                                            <button class="button button3" >
                                                            <i class="fa fa-trash"> </i> </button> </a>';

                                                	include('edit_type.php');
                                                	echo '</td>';
                                                	$numb++;
                                                	echo '</tr>';
                                        	    }
                                	        } 
                        	            ?>
                        	        </tbody>
                        	        <tfoot>
                        	            <tr>
                                            <th>Id</th>
                                            <th> Name </th>
					                        <th align='center'> Action </th>
                        	            </tr>
                        	        </tfoot>
                 	            </table>
			                </td>
			                <td>

  			                    <h4> Property Location
                                    <?php
                          	            echo " <button class='button button2' data-toggle='modal' data-target='#modal-location'>
                                                <i class='fa fa-plus'></i> Add Property Location </button>";
                                    ?>
			                    </h4>

                	            <table id="plocation" class="table table-bordered table-striped">
                        	        <thead>
                        	            <tr>
                                	        <th> Id</th>
                               	 	        <th> Name </th>
                                	        <th align='center'> Action </th>
                        	            </tr>
                        	        </thead>
                        	        <tbody>
                        	            <?php
                                       	    $root = "SELECT * FROM property_location ORDER BY name ASC";
                                	        $resl = pg_query($conn, $root);
                                	        if ($resl) {
                                        	    // output data of each row
                                        	    $numb = 1;
                                                while($rowl = pg_fetch_assoc($resl)) {
                                                    (string) $id = $rowl['property_location_code'];
                                                    (string) $property_location_code = $rowl['property_location_code'];

                                                	echo "<tr>";
                                                	echo "	<td width='5%'>" . $numb . "</td>";
                                                	echo "  <td>" . $rowl['name'] . "</td>";
                                                    echo '  <td width="20%">';

                                                    echo "<button class='button button2' data-toggle='modal' data-target='#modal-location'>
                                                            <i class='fa fa-plus'> </i> </button>";

                                                    echo '  <a class="button button" href="#edit'.$property_location_code.'" data-toggle="modal">
                                                           	<i class="fa fa-edit"></i> </a>';
                                                    echo '  <a href="location_delete.php?error='. $id.'"
                                                            onclick="return confirm(\'Are you sure to delete?\')">
                                                            <button class="button button3" >
                                                            <i class="fa fa-trash"> </i> </button> </a>';

                                                	include('edit_location.php');
                                                	echo '</td>';
                                                	$numb++;
                                                	echo '</tr>';
                                        	    }
                                	        } 
                        	            ?>
                        	        </tbody>
                        	        <tfoot>
                        	            <tr>
                                            <th>Id</th>
                                            <th> Name </th>
					                        <th align='center'> Action </th>
                        	            </tr>
                        	        </tfoot>
                 	            </table>
			                </td>
		                </tr>

                </table>

	        </div>

         </table>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            Master Data Settings
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



      <!-- for property usage -->
      <div class="modal fade" id="modal-type">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-diocese">Add Property Type  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                <div class="form-group">
                   <label for="exampleInputPassword1"> Property Type :</label>
                        <input maxlength="100" type="text" name="name" 
                            class="form-control" placeholder="Enter property type ... " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="type" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <!-- for property usage -->
      <div class="modal fade" id="modal-location">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-diocese">Add Property Location  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                <div class="form-group">
                   <label for="exampleInputPassword1"> Property Location :</label>
                        <input maxlength="100" type="text" name="name" 
                            class="form-control" placeholder="Enter property location ... " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="location" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



      <!-- for Religious Inst Category -->
      <div class="modal fade" id="modal-rcategory">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-sector">Add Religious Institute Category  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Name : </label>
                        <input type="text" name="religious_inst_category_name" class="form-control" placeholder="Enter here ... " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="category" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <!-- for property usage -->
      <div class="modal fade" id="modal-usage">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-diocese">Add Property Usage  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              
            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                <div class="form-group">
                   <label for="exampleInputPassword1"> Property Usage :</label>
                        <input maxlength="100" type="text" name="name" 
                            class="form-control" placeholder="Enter property usage ... " required>
                    </div>
                        
            </div>  
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="usage" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <!-- for property category -->
      <div class="modal fade" id="modal-category">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-eprovince">Add Property Category  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Category Name : </label>
                        <input maxlength="100" type="text" name="name" class="form-control" 
                            placeholder="Enter category name here ... " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="category" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- for sectors -->
      <div class="modal fade" id="modal-sector">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-sector">Add Sectors  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Sector Name : </label>
                        <input type="text" name="sector_name" class="form-control" placeholder="Enter sector name E.g. Health " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="sector" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



      <!-- for titles -->
      <div class="modal fade" id="modal-title">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Add Title  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Title Name : </label>
                        <input type="text" name="title_name" class="form-control" placeholder="Enter title name " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="title" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- for titles -->
      <div class="modal fade" id="modal-position">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Add Position  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              
            <form action="../master-data-settings/" method="post">
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Position Name : </label>
                        <input type="text" name="position_name" class="form-control" placeholder="Enter position name " required>
                    </div>
                        
            </div>  
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="position" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



      <!-- for Education -->
      <div class="modal fade" id="modal-education">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Add Education Level  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Level of Education : </label>
                        <input type="text" name="education_name" class="form-control" placeholder="Enter level name e.g. Degree, Certification " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="education" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- for Speciality -->
      <div class="modal fade" id="modal-speciality">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Add Speciality  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">
              
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Speciality : </label>
                        <input type="text" name="speciality_name" class="form-control" placeholder="Enter speciality name e.g. Social Work" required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="speciality" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <!-- for department -->
      <div class="modal fade" id="modal-department">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Add Department  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Department Name : </label>
                        <input type="text" name="department_name" class="form-control" placeholder="Enter department name here " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="department" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



      <!-- for root -->
      <div class="modal fade" id="modal-disability">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Add Disability  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Disability Name : </label>
                        <input type="text" name="disability_name" class="form-control" placeholder="Enter disability here " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="disability" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <!-- for partner -->
      <div class="modal fade" id="modal-partner">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Add Partner  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Partner Name : </label>
                        <input type="text" name="partner_name" class="form-control" placeholder="Enter partner name " required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="partner" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <!-- for root -->
      <div class="modal fade" id="modal-nation">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-info disabled color-palette">
              <h4 class="modal-title">Add New Nationality  </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form action="../master-data-settings/" method="post">
            <div class="modal-body">
              
                    <div class="form-group">
                        <label for="exampleInputEmail1">Country Name : </label>
                        <input type="text" name="nationality" class="form-control" placeholder="Enter username" required>
                    </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" name="nation" class="btn btn-info">Save</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->




  <!-- /.content-wrapper -->

        <!-- Begin form for adding privilege -->
        <div class="modal fade" id="modal-tribe">
          <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-info disabled color-palette">
                   <h4 class="modal-title">Add Tribe </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

                <form action="../master-data-settings/" method="post">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tribe Name : </label>
                        <input type="text" name="tribe_name" class="form-control" placeholder="Enter tribe name e.g. Baganda " required>
                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="tribe" class="btn btn-info">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user privilege -->

        <!-- Begin form for adding religion -->
        <div class="modal fade" id="modal-religion">
          <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-info disabled color-palette">
                   <h4 class="modal-title">Add Religion </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

                <form action="../master-data-settings/" method="post">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Religion Name : </label>
                        <input type="text" name="religion_name" class="form-control" placeholder="Enter religion name here" required>
                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="religion" class="btn btn-info">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user privilege -->






        <!-- Add Institution -->
        <div class="modal fade" id="modal-uni">
          <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-info disabled color-palette">
                   <h4 class="modal-title">Add University / Institution </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

                <form action="../master-data-settings/" method="post">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Name : </label>
                        <input type="text" name="institute_name" class="form-control" placeholder="Enter name of the institution" required>
                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="institute" class="btn btn-info">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user privilege -->




        <!-- Add program -->
        <div class="modal fade" id="modal-prog">
          <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-info disabled color-palette">
                   <h4 class="modal-title">Add Program or Descpline </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

                <form action="../master-data-settings/" method="post">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Name : </label>
                        <input type="text" name="study_program_name" class="form-control" placeholder="Enter study program name here " required>
                    </div>

                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer justify-content-between">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="study_program" class="btn btn-info">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user privilege -->




<!-- Java Scripts for tabs on enrollment   -->
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- for table search and pagination -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#privilege").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#diocese").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#syslog").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#category").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#ecclesiastical").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#plocation").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#religion").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#disability").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#education").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#speciality").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#title").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#position").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#sector").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#rcategory").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


<script>
  $(function () {
    $("#institute").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#prog").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<script>
  $(function () {
    $("#salutation").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


