
    <!-- Begin form for adding user -->
    <div class="modal fade" id="edit<?php echo $property_category_code;?>" tabindex="-1" role="dialog" a-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
            	<div class="modal-header bg-success disabled color-palette">
              	   <h4 class="modal-title">Editing Property Category </h4>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                   </button>
                 </div>
              <div class="modal-body">

              	<?php
              		$edit=pg_query($conn,"SELECT * FROM property_category WHERE property_category_code ='".$property_category_code."'");
                    $erow=pg_fetch_array($edit);
           	    ?>

                <form method="POST" action="../master-data-settings/">
                <div class="box-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1"> Property Category :</label>

                        <input  type="hidden" name="property_category_code" class="form-control" 
                            value="<?php echo $erow['property_category_code']; ?>" 
                            id="inputdefault" style="width: 100%;"  required>

                        <input 	type="text" name="name" class="form-control" value="<?php echo $erow['name']; ?>" 
				            id="inputdefault" style="width: 100%;"  required>
                    </div>
                </div>
                <!-- /.box-body -->
              </div>

              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" name="update_category" class="btn btn-info">Save</button>
              </div>

              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- End form for adding user -->
