<?php
    session_start();
	require_once'../library/config.php';
 	logout_session($conn); //Check wheather user logged in
    auto_logout($conn); //Logout iddle user
    $GLOBALS['app'] = "settings";
    $GLOBALS['nav'] = "master";
    $GLOBALS['error'] = "";

    # Processing property location
    if(isset($_POST['location'])){
        $name = test_input($_POST['name']);
        $link = sha1($name.date("Y-m-d"));
        $user = test_input($_SESSION['user']);
        $ip = ip_add();

        $sql = "SELECT * FROM property_location WHERE name = '$name'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched
        if($count > 0) {
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! The property location " .$name. " already exists.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
        }else{
            $sql = "INSERT INTO property_location (name,createdby,property_location_code)
                VALUES ('$name','$user','$link')";
            $result = pg_query($conn,$sql);
            $tab = '1';
            $type = "-- Normal --";
            $GLOBALS['error'] = "The property location ".$name." has been added ";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
            echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
        }
    }

    # Processing editing property location
    if(isset($_POST['update_location'])){
        $name = test_input($_POST['name']);
        $link = test_input($_POST['property_location_code']);
        $user = test_input($_SESSION['user']);
        $ip = ip_add();

        $sql = "SELECT * FROM property_location
            WHERE name = '$name' AND property_location_code = '$link'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched
        if($count > 0) {
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! No changes made.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
        }else{
            $sql = "UPDATE property_location SET name = '$name'
                WHERE property_location_code = '$link'";
            $result = pg_query($conn,$sql);
            $tab = '1';
            $type = "-- Normal --";
            $GLOBALS['error'] = "The property location ".$name." has been updated.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
            echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
        }
    }

    # Processing diocese
    if(isset($_POST['usage'])){
        $name = test_input($_POST['name']);
        $link = sha1($name.date("Y-m-d"));
        $user = test_input($_SESSION['user']);
        $ip = ip_add();

        $sql = "SELECT * FROM property_usage 
            WHERE name = '$name'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched 
        if($count > 0) {
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! The property usage " .$name. " already exists.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
        }else{
            $sql = "INSERT INTO property_usage (name,createdby,property_usage_code)
                VALUES ('$name','$user','$link')";
            $result = pg_query($conn,$sql);
            $tab = '1';
            $type = "-- Normal --";
            $GLOBALS['error'] = "Property usage ".$name." has been added ";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
            echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
        }
    }

    # Processing editing diocesen 
    if(isset($_POST['update_usage'])){
        $name = test_input($_POST['name']);
        $link = test_input($_POST['property_usage_code']);
        $user = test_input($_SESSION['user']);
        $ip = ip_add();

        $sql = "SELECT * FROM property_usage
            WHERE name = '$name' 
            AND property_usage_code = '$link'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched 
        if($count > 0) {
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! No changes made.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
        }else{
            $sql = "UPDATE property_usage SET name = '$name',
            createdby = '$user'
            WHERE property_usage_code = '$link'";
            $result = pg_query($conn,$sql);
            $tab = '1';
            $type = "-- Normal --";
            $GLOBALS['error'] = "The property usage ".$name." has been updated.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
            echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
        }
    }


    # Processing property category
    if(isset($_POST['category'])){
        $name = test_input($_POST['name']);
        $link = sha1($name.date("Y-m-d"));
        $user = test_input($_SESSION['user']);
        $ip = ip_add();

        $sql = "SELECT * FROM property_category WHERE name = '$name'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched 
        if($count > 0) {
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! The property category " .$name. " already exists.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
        }else{
        $sql = "INSERT INTO property_category (name,property_category_code)
            VALUES ('$name','$link')";
        $result = pg_query($conn,$sql);
        $tab = '1';
        $type = "-- Normal --";
        $GLOBALS['error'] = "The property category ".$name." has been added ";
        $msg = $error;
        syslogs($conn,$ip,$msg,$user,$type);
        echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
        }
    }

    # Processing editing eprovincen 
    if(isset($_POST['update_category'])){
        $name = test_input($_POST['name']);
        $link = test_input($_POST['property_category_code']);
        $user = test_input($_SESSION['user']);
        $ip = ip_add();

        $sql = "SELECT * FROM property_category
            WHERE name = '$name' AND property_category_code = '$link'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched 
        if($count > 0) {
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! No changes made.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
        }else{
            $sql = "UPDATE property_category SET name = '$name'
            WHERE property_category_code = '$link'";
            $result = pg_query($conn,$sql);
            $tab = '1';
            $type = "-- Normal --";
            $GLOBALS['error'] = "The property category ".$name." has been updated.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
            echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
        }
    }

    # Processing property category
    if(isset($_POST['type'])){
        $name = test_input($_POST['name']);
        $link = sha1($name.date("Y-m-d"));
        $user = test_input($_SESSION['user']);
        $ip = ip_add();

        $sql = "SELECT * FROM property_type WHERE name = '$name'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched 
        if($count > 0) {
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! The property type " .$name. " already exists.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
        }else{
        $sql = "INSERT INTO property_type (name,property_type_code)
            VALUES ('$name','$link')";
        $result = pg_query($conn,$sql);
        $tab = '1';
        $type = "-- Normal --";
        $GLOBALS['error'] = "The property type ".$name." has been added ";
        $msg = $error;
        syslogs($conn,$ip,$msg,$user,$type);
        echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
        }
    }

    # Processing editing property type 
    if(isset($_POST['update_type'])){
        $name = test_input($_POST['name']);
        $link = test_input($_POST['property_type_code']);
        $user = test_input($_SESSION['user']);
        $ip = ip_add();

        $sql = "SELECT * FROM property_type
            WHERE name = '$name' 
            AND property_type_code = '$link'";
        $result = pg_query($conn,$sql);
        $row = pg_fetch_assoc($result);
        $count = pg_num_rows($result);
        // If result matched 
        if($count > 0) {
            $type = "-- warning --";
            $GLOBALS['error'] = "Invalid! No changes made.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
        }else{
            $sql = "UPDATE property_type SET name = '$name',
            createdby = '$user'
            WHERE property_type_code = '$link'";
            $result = pg_query($conn,$sql);
            $tab = '1';
            $type = "-- Normal --";
            $GLOBALS['error'] = "The property type ".$name." has been updated.";
            $msg = $error;
            syslogs($conn,$ip,$msg,$user,$type);
            echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
        }
    }


        # Processing disability
        if(isset($_POST['disability'])){
                $disability_name = test_input($_POST['disability_name']);
                $link = sha1($disability_name);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM diasability WHERE disability_name = '$disability_name'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The disability name " .$disability_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO disability (disability_name,disability_createdby,disability_code_link)
                                VALUES ('$disability_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The disability name ".$disability_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
                }
        }

        # Processing editing disability 
        if(isset($_POST['update_disability'])){
                $disability_name = test_input($_POST['disability_name']);
                $link = test_input($_POST['disability_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM disability
                        WHERE disability_name = '$disability_name' AND disability_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE disability SET disability_name = '$disability_name',
                                disability_lastupdatedon = now(),
                                disability_lastupdatedby = '$user'
                                WHERE disability_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The disability name ".$disability_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_disability='.$tab.'"</script>';
                }
        }

        # Processing Religion
        if(isset($_POST['religion'])){
                $religion_name = test_input($_POST['religion_name']);
                $link = sha1($religion_name);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM religion WHERE religion_name = '$religion_name'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The religion name " .$religion_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO religion (religion_name,religion_createdby,religion_code_link)
                                VALUES ('$religion_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The religion name ".$religion_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_religion='.$tab.'"</script>';
                }
        }

        # Processing editing religion 
        if(isset($_POST['update_religion'])){
                $religion_name = test_input($_POST['religion_name']);
                $link = test_input($_POST['religion_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM religion
                        WHERE religion_name = '$religion_name' AND religion_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE religion SET religion_name = '$religion_name',
                                religion_lastupdatedon = now(),
                                religion_lastupdatedby = '$user'
                                WHERE religion_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The religion name ".$religion_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_religion='.$tab.'"</script>';
                }
        }


        # Processing Tribe
        if(isset($_POST['tribe'])){
                $tribe_name = test_input($_POST['tribe_name']);
                $link = sha1($tribe_name);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM tribe WHERE tribe_name = '$tribe_name'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! The tribe name " .$tribe_name. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "INSERT INTO tribe (tribe_name,tribe_createdby,tribe_code_link)
                                VALUES ('$tribe_name','$user','$link')";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The tribe name ".$tribe_name." has been added ";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_tribe='.$tab.'"</script>';
                }
        }

        # Processing editing tribe 
        if(isset($_POST['update_tribe'])){
                $tribe_name = test_input($_POST['tribe_name']);
                $link = test_input($_POST['tribe_code_link']);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM tribe 
                        WHERE tribe_name = '$tribe_name' AND tribe_code_link = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE tribe SET tribe_name = '$tribe_name',
                                tribe_lastupdatedon = now(),
                                tribe_lastupdatedby = '$user'
                                WHERE tribe_code_link = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The tribe name ".$tribe_name." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_tribe='.$tab.'"</script>';
                }
        }

        # Processing Nationality
        if(isset($_POST['nation'])){
        	$nationality = test_input($_POST['nationality']);
                $link = sha1($nationality);
                $user = test_input($_SESSION['user']);
		$ip = ip_add();
		
                $sql = "SELECT * FROM nationality WHERE name = '$nationality'"; 
                $result = pg_query($conn,$sql);
		$row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
       			$type = "-- warning --";
                        $GLOBALS['error'] = "The nationality of " .$nationality. " already exists.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
		}else{
                	$sql = "INSERT INTO nationality (name,nationality_createdby,nationality_url)
                        	VALUES ('$nationality','$user','$link')";
                	$result = pg_query($conn,$sql);
			$tab = '1';
                       	$type = "-- Normal --";
                       	$GLOBALS['error'] = "The nationality for the Country ".$nationality." has been added ";
                       	$msg = $error;
                      	syslogs($conn,$ip,$msg,$user,$type);
                       	echo '<script>window.location="../master-data-settings/?tab_nationality='.$tab.'"</script>';
                }
        }

        # Processing editing nationality
        if(isset($_POST['update_nationality'])){
                $name = test_input($_POST['name']);
                $link = test_input($_POST['nationality_url']);
                $url = sha1($link);
                $user = test_input($_SESSION['user']);
                $ip = ip_add();

                $sql = "SELECT * FROM nationality 
			WHERE name = '$name' AND id = '$link'";
                $result = pg_query($conn,$sql);
                $row = pg_fetch_assoc($result);
                $count = pg_num_rows($result);
                // If result matched 
                if($count > 0) {
                        $type = "-- warning --";
                        $GLOBALS['error'] = "Invalid! No changes made.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                }else{
                        $sql = "UPDATE nationality SET name = '$name',
				nationality_lastupdateon = now(),
				nationality_lastupdatedby = '$user',
				nationality_url = '$url'
				WHERE id = '$link'";
                        $result = pg_query($conn,$sql);
                        $tab = '1';
                        $type = "-- Normal --";
                        $GLOBALS['error'] = "The nationality ".$nationality." has been updated.";
                        $msg = $error;
                        syslogs($conn,$ip,$msg,$user,$type);
                        echo '<script>window.location="../master-data-settings/?tab_nationality='.$tab.'"</script>';
                }
        }

 	require_once'../header.php';
 	require_once'../navigation.php';
 	include('body.php');
 	require_once'../footer.php';
?>

