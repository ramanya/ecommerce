<?php
	require_once'../library/config.php';
 	logout_session($conn); //Check wheather user logged in
	auto_logout($conn); //Logout iddle user
        $GLOBALS['error'] = "";
        require_once'../library/functions.php';
	
	# Processing region
        if(isset($_POST['usertype'])){
                $usertypecode = test_input($_POST['usertypecode']);
                $usertypename = test_input($_POST['usertypename']);
		$user = test_input($_SESSION['login_user']);
		$connecting_IP = $_SERVER['REMOTE_ADDR'];
		
                if (empty($usertypecode)){
                        $GLOBALS['error'] = "User Type code is required";
                        $msg = "User Type Code is required...please try again.";
                        syslogs($conn,$connecting_IP,$msg);
                }
                if (empty($usertypename)){
                        $GLOBALS['error'] = "User Type name is required";
                        $msg = "User Type Name is required...please try again.";
                        syslogs($conn,$connecting_IP,$msg);
                }	
		if (strlen($usertypecode) > 15){
                        $GLOBALS['error'] = "Invalid user code! try again";
                        $msg = "User Type Code can not be more than 15 characters.";
                        syslogs($conn,$connecting_IP,$msg);
		}
                if (strlen($usertypename) > 50){
                        $GLOBALS['error'] = "Invalid user type name! try again";
                        $msg = "User Type Name can not be more than 50 characters.";
                        syslogs($conn,$connecting_IP,$msg);
                }
		# Checking if the region is already created
		if (!empty($usertypecode) && !empty($usertypename)){
                        $sql = "SELECT * FROM userType  WHERE usertypeCode = '$usertypecode' AND usertypeName = '$usertypename'";
                        $result = pg_query($conn,$sql);
                        $row = pg_fetch_array($result,MYSQLI_ASSOC);
                        $count = pg_num_rows($result);
                        // If result matched $myusername and $mypassword, table row must be 1 row
                        if($count > 0) {
                        	$GLOBALS['error'] = $usertypecode ." " . $usertypename . " already exists.";
                        }else{		
                		usertype($conn,$usertypecode,$usertypename,$user);
                		$GLOBALS['error'] = "The user type " . $usertypecode . " " . $usertypename . " has been created";
                		$msg = "The user type " . $usertypecode . " " .  $usertypename . "has been created by " . $user;
                		syslogs($conn,$connecting_IP,$msg);
			}
		}
	}

        # Processing region changes
        if(isset($_POST['editusertype'])){
                $usertypecode = test_input($_POST['usertypecode']);
                $usertypename = test_input($_POST['usertypename']);
                $user = test_input($_SESSION['login_user']);
                
		if (empty($usertypename)){
                        $GLOBALS['error'] = "User type name is required";
                        $msg = "User type name is required...please try again.";
                        syslogs($conn,$connecting_IP,$msg);
                }
                if (strlen($usertypename) > 50){
                        $GLOBALS['error'] = "Invalid region name! try again";
                        $msg = "User type name can not be more than 50 characters.";
                        syslogs($conn,$connecting_IP,$msg);
                }
                # Checking if the region is already created
                if (!empty($usertypename)){
                        $sql = "SELECT * FROM userType  WHERE usertypeName  = '$usertypename'";
                        $result = pg_query($conn,$sql);
                        $row = pg_fetch_array($result,MYSQLI_ASSOC);
                        $count = pg_num_rows($result);
                        // If result matched $myusername and $mypassword, table row must be 1 row
                        if($count > 0) {
                                $GLOBALS['error'] = $usertypecode ." " . $usertypename . " already exists.";
                        }else{
                                editusertype($conn,$usertypecode,$usertypename,$user);
                                $GLOBALS['error'] = "The user type details are have been modified to " . $usertypename . ". ";
                                $msg = "The user type " .  $usertypename . "has been changed by " . $user;
                                syslogs($conn,$connecting_IP,$msg);
                        }
                }
        }



 	require_once'../header.php';
 	require_once'../navigation.php';
 	include('edit.php');
 	require_once'../footer.php';
?>
