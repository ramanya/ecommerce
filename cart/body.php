		
<!-- Preloader -->
<div class="preloader">
	<div class="preloader-inner">
		<div class="preloader-icon">
			<span></span>
			<span></span>
		</div>
	</div>
</div>
<!-- End Preloader -->


<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="bread-inner">
					<ul class="bread-list">
						<li><a href="#">Cart<i class="ti-arrow-right"></i></a></li>
						
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Breadcrumbs -->

<!-- Shopping Cart -->
<div class="shopping-cart section">
	<div class="container">
		<?php
			//For alert messages
		if (strlen($error) > 0) {
			echo '<div class="alert alert-success" role="alert" data-auto-dismiss="3000">
			' . $error . '
			</div>';
		}
		?>

		<div class="row">
			<div class="col-12">
				<!-- Shopping Summery -->

				<table class="table shopping-summery">
					<thead>
						<tr class="main-hading">
							<th>PRODUCT</th>
							<th>NAME</th>
							<th class="text-center">UNIT PRICE</th>
							<th class="text-center">QUANTITY</th>
							<th class="text-center">TOTAL</th> 
							<th class="text-center"><i class="ti-trash remove-icon"></i></th>
						</tr>
					</thead>
					<tbody>
						<?php

						require_once'../admin/library/config.php';
						if (empty($_SESSION['user'])) {


        //some thing is not okay
						}else{
							$user =  $_SESSION['user'];

							$root ="SELECT * FROM cart WHERE product_createdby ='$user'";
							$res1 = pg_query($conn, $root);

							if ($res1) {
            					// output data of each row
								$num = 1;
								while($row1 = pg_fetch_assoc($res1)) {
									(string) $id = $row1['product_code'];
									(string) $product_code = $row1['product_code'];
									(string) $product_photo = $row1['product_photo'];
									(string) $product_description = $row1['product_description'];
									(string) $product_name = $row1['product_name'];
									(string) $product_price = $row1['product_price'];
									(string) $product_quantity = $row1['product_quantity'];
									
									(string) $amount = $row1['product_amount'];

									(string) $product_createdon = $row1['product_createdon'];
									(string) $product_createdby = $row1['product_createdby'];
									(string) $product_code_link = $row1['product_code_link'];

									if (!empty($product_code)){

										?>

										<tr>
											<td class="image" data-title="No"><img src="../admin/products/images/<?php echo $product_photo; ?>" alt="#"></td>
											<td class="product-des" data-title="Description">
												<p class="product-name"><a href="#"><?php echo $product_name; ?></a></p>
												<p class="product-des"><?php echo $product_description; ?></p>
											</td>
											<td class="price" data-title="Price"><span>UGX <?php echo $product_price; ?> </span></td>
											<td class="qty" data-title="Qty"><!-- Input Order -->
												<div class="input-group">
													<div class="button minus">
														<form action="index.php" method="post">
												<?php echo "<input  type='hidden' name='product_code_link' id='product_code_link' value='$product_code_link' />"; ?>

													<!-- 	<button type="button" name="reduce_quantity" class="btn btn-primary btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
															<i class="ti-minus"></i>
														</button> -->
														<?php 	echo ' <button type="submit" name="reduce_quantity"<i class="ti-minus"> </i></button>'; ?>

													</form>
												</div>
												<input type="text" name="quant" class="input-number"  data-min="1" data-max="100" value="<?php echo $product_quantity; ?>">
												<div class="button plus">
													<form action="index.php" method="post">
												<?php echo "<input  type='hidden' name='product_code_link' id='product_code_link' value='$product_code_link' />"; ?>


												<!-- 	<button type="submit" name="increase_quantity" class="btn btn-primary btn-number" >
															<i class="ti-plus"></i>
														</button> --> 

														

														<?php 	echo ' <button type="submit" name="increase_quantity"<i class="ti-plus"> </i></button>'; ?>


													</form>
												</div>
											</div>
											<!--/ End Input Order -->
										</td>
										<td class="total-amount" data-title="Total"><span>$ <?php echo $amount; ?> </span></td>
										<td class="action" data-title="Remove">
											<form action="index.php" method="post">
												<input type="hidden" name="product-code-link" value="<?php echo $product_code_link ?>">
												<button name="remove_cart" ><i class="ti-trash remove-icon"></i></button>


											</form></td>
										</tr>

									<?php }
								}
							}
						}
						?>
					</tbody>
				</table>
				<!--/ End Shopping Summery -->
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<!-- Total Amount -->
				<div class="total-amount">
					<div class="row">
						<!-- 	<div class="col-lg-8 col-md-5 col-12">
								<div class="left">
									<div class="coupon">
										<form action="#" target="_blank">
											<input name="Coupon" placeholder="Enter Your Coupon">
											<button class="btn">Apply</button>
										</form>
									</div>
									<div class="checkbox">
										<label class="checkbox-inline" for="2"><input name="news" id="2" type="checkbox"> Shipping (+10$)</label>
									</div>
								</div>
							</div> -->
							<div class="col-lg-4 col-md-7 col-12">
								<div class="right">
									<ul>
										<li>Cart Subtotal<span>$ <?php 
										require_once'../admin/library/config.php';
										if (empty($_SESSION['user'])) {


       							 //some thing is okay
										}else{
											$user =  $_SESSION['user'];

											$query = "select sum(product_amount) from cart where product_createdby='$user'";
											$result=pg_query($conn,$query);
									//$rows= pg_num_rows($result);
											while($rows = pg_fetch_array($result)) {
												echo $rows['sum']; 
											}
										}
									?></span></li>
									<!-- 	<li>Shipping<span>Free</span></li>
										<li>You Save<span>$20.00</span></li> -->
										<li class="last">You Pay<span>$ <?php 
										require_once'../admin/library/config.php';
										if (empty($_SESSION['user'])) {


       							 //some thing is okay
										}else{
											$user =  $_SESSION['user'];

											$query = "select sum(product_amount) from cart where product_createdby='$user'";
											$result=pg_query($conn,$query);
									//$rows= pg_num_rows($result);
											while($rows = pg_fetch_array($result)) {
												echo $rows['sum']; 
												       $_SESSION['final_amount'] = $rows['sum'];

											}
										}
									?></span></li>
								</ul>
								<div class="button5">
									<form action="index.php" method="post">
												<input type="hidden" name="product-code-link" value="<?php if (empty($product_code_link)) {


       							 //some thing is okay
										}else{ echo $product_code_link; } ?>">
											<button name="order_now" class="btn">Order Now</button>

												
											</form>
									
									<a href="../" class="btn">Continue shopping</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ End Total Amount -->
			</div>
		</div>
	</div>
</div>
<!--/ End Shopping Cart -->


