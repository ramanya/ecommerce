	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->


	<!-- Start Product Area -->
	<div class="product-area section">
		<div class="container">

			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h2>On Sale</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="product-info">
						<div class="nav-main">
							<!-- Tab Nav -->
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item"><a class="nav-link active"  href="all-products/" >All Products</a></li>
								<li class="nav-item"><a class="nav-link" href="homeware/" >Homeware</a></li>
								<li class="nav-item"><a class="nav-link"  href="horn/" >Horns & Buttons</a></li>
								<li class="nav-item"><a class="nav-link"  href="accessories/" >Accessories</a></li>
								<li class="nav-item"><a class="nav-link"  href="jewellery/" >Jewellery</a></li>
								<li class="nav-item"><a class="nav-link"  href="culinary/" >Culinary Accessories</a></li>

								
							</ul>
							<!--/ End Tab Nav -->
						</div>
						<div class="tab-content" id="myTabContent">
							<!-- Start Single Tab -->
							<div class="tab-pane fade show active" id="man" role="tabpanel">
								<div class="tab-single">
								
									<div class="row">


										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-12  ">
											<div class="single-product">
												<div class="product-img">
													<a href="horn/">
														<img class="default-img" src="../images/horn.jpg" alt="#">
														<img class="hover-img" src="../images/horn.jpg" alt="#">
														<span style="color: #f6feff; position: absolute;font-size: 22px;
														top: 50%;
														left: 50%;
														transform: translate(-50%, -50%);" class=carousel-caption>Horns</span>
													</a>

												</div>


											</div>
										</div>

										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-12  ">
											<div class="single-product">
												<div class="product-img">
													<a href="homeware/">
														<img class="default-img" src="../images/xzada.jpg" alt="#">
														<img class="hover-img" src="../images/xzada.jpg" alt="#">
														<span style="color: #f6feff ; position: absolute;font-size: 22px;
														top: 50%;
														left: 50%;
														transform: translate(-50%, -50%);" class=carousel-caption>Home Ware</span>
													</a>

												</div>


											</div>
										</div>

										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-12  ">
											<div class="single-product">
												<div class="product-img">
													<a href="accessories/">
														<img class="default-img" src="../images/acc.jpg" alt="#">
														<img class="hover-img" src="../images/acc.jpg" alt="#">
														<span style="color: #f6feff ; position: absolute;font-size: 22px;
														top: 50%;
														left: 50%;
														transform: translate(-50%, -50%);" class=carousel-caption>Accessories</span>
													</a>

												</div>


											</div>
										</div>

										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-12 ">
											<div class="single-product">
												<div class="product-img">
													<a href="jewellery/">
														<img class="default-img" src="../images/jew.jpg" alt="#">
														<img class="hover-img" src="../images/jew.jpg" alt="#">
														<span style="color: #f6feff ; position: absolute;font-size: 22px;
														top: 50%;
														left: 50%;
														transform: translate(-50%, -50%);" class=carousel-caption>Jewellery</span>
													</a>
													
												</div>


											</div>
										</div>



										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-12 ">
											<div class="single-product">
												<div class="product-img">
													<a href="culinary/">
														<img class="default-img" src="../images/cup.jpg" alt="#">
														<img class="hover-img" src="../images/cup.jpg" alt="#">
														<span style="color: #f6feff ; position: absolute;font-size: 22px;
														top: 50%;
														left: 50%;
														transform: translate(-50%, -50%);" class=carousel-caption>Culinary Accessories</span>
													</a>

												</div>

											</div>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-12">
											<div class="single-product">
												<div class="product-img">
													<a href="all-products/">
														<img class="default-img" src="../images/all.jpg" alt="#">
														<img class="hover-img" src="../images/all.jpg" alt="#">
														<span style="color: #f6feff ; position: absolute;font-size: 22px;
														top: 50%;
														left: 50%;
														transform: translate(-50%, -50%);" class=carousel-caption>All Products</span>
													</a>

												</div>

											</div>
										</div>



									</div>
									
								</div>
							</div>
							<!--/ End Single Tab -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Product Area -->