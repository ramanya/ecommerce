


<!--start Header -->
<?php    require_once'../header.php'; ?>
<!--/ End Header -->

<div class="product-area section">
    <div class="container">


        <?php
        require_once'../../admin/library/config.php';

        $product_id = $_GET["error"];
        $root ="SELECT * FROM product  where product_code_link= '$product_id' ORDER BY product_createdon DESC LIMIT 500 ";
        $res1 = pg_query($conn, $root);

        if ($res1) {
            // output data of each row
            $num = 1;
            while($row1 = pg_fetch_assoc($res1)) {
                (string) $id = $row1['product_code'];
                (string) $product_code = $row1['product_code'];
                (string) $product_photo = $row1['product_photo'];
                (string) $product_description = $row1['product_description'];
                (string) $product_name = $row1['product_name'];
                (string) $product_price = $row1['product_price'];
                (string) $status = $row1['product_availability'];
                (string) $product_createdon = $row1['product_createdon'];
                (string) $product_createdby = $row1['product_createdby'];
                (string) $product_code_link = $row1['product_code_link'];
                (string) $housing_photo1 = $row1['housing_photo1'];
                (string) $housing_photo2 = $row1['housing_photo2'];
                (string) $housing_photo3 = $row1['housing_photo3'];
                (string) $housing_photo4 = $row1['housing_photo4'];
                (string) $housing_photo5 = $row1['housing_photo5'];
                (string) $housing_photo6 = $row1['housing_photo6'];
                (string) $housing_photo1_cap = $row1['housing_photo1_cap'];
                (string) $housing_photo2_cap = $row1['housing_photo2_cap'];
                (string) $housing_photo3_cap = $row1['housing_photo3_cap'];
                (string) $housing_photo4_cap = $row1['housing_photo4_cap'];
                (string) $housing_photo5_cap = $row1['housing_photo5_cap'];
                (string) $housing_photo6_cap = $row1['housing_photo6_cap'];
                if (!empty($product_code)){
                    ?>

                    <!-- Modal -->

                    <div class="modal-body">
                        <div class="row no-gutters">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <!-- Product Slider -->
                                <div class="product-gallery">
                                    <div class="quickview-slider-active">
                                        <div class="single-slider">
                                            <img src="../../admin/products/images/<?php echo $product_photo; ?>" alt="#">
                                        </div>
                                        <!-- product photo -->

                                        <div class="single-slider">
                                            <img src="../../admin/products/images/<?php echo $housing_photo1; ?>" alt="#">
                                        </div>

                                        <!-- product photo -->

                                        <div class="single-slider">
                                            <img src="../../admin/products/images/<?php echo $housing_photo2; ?>" alt="#">
                                        </div>

                                        <!-- product photo -->

                                        <div class="single-slider">
                                            <img src="../../admin/products/images/<?php echo $housing_photo3; ?>" alt="#">
                                        </div>

                                        <!-- product photo -->

                                        <div class="single-slider">
                                            <img src="../../admin/products/images/<?php echo $housing_photo4; ?>" alt="#">
                                        </div>

                                        <!-- product photo -->

                                        <div class="single-slider">
                                            <img src="../../admin/products/images/<?php echo $housing_photo5; ?>" alt="#">
                                        </div>

                                        <!-- product photo -->

                                        <div class="single-slider">
                                            <img src="../../admin/products/images/<?php echo $housing_photo6; ?>" alt="#">
                                        </div>

                                    </div>
                                </div>
                                <!-- End Product slider -->
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="quickview-content">
                                    <h2><?php echo $product_name; ?></h2>
                                    <div class="quickview-ratting-review">
                                        <div class="quickview-ratting-wrap">
                                            <div class="quickview-ratting">
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="yellow fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <a href="#"> (1 customer review)</a>
                                        </div>
                                        <div class="quickview-stock">
                                            <?php 
                                            if ($status == 't'){echo '<span><i class="fa fa-check-circle-o"></i> in stock</span>';}else{echo '<span><i class="fa fa-check-circle-o"></i> out of stock </span>';} ?>

                                        </div>
                                    </div>
                                    <h3>$ <?php echo $product_price; ?></h3>
                                    <div class="quickview-peragraph">
                                        <p><?php echo $product_description; ?></p>
                                    </div>


                                    <div class="add-to-cart">
                                        <form action="index.php" method="post">
                                            <input type="hidden" name="product-code-link" value="<?php echo $product_code_link ?>">
                                            <button title="Add to cart" class="btn" type="submit" name="cart">Add to cart</button>

                                        </form>

                                    </div>

                                    <div class="add-to-cart">
                                        <a href="./" class="btn">continue shopping</a>

                                    </div>
                                    <div class="default-social">
                                        <h4 class="share-now">Share:</h4>
                                        <ul>
                                            <li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
                                            <li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
                                            <li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                <?php 
            }
            }
        }
        ?>


        <!-- Modal end -->


    </div>
</div>

<!-- Start Footer Area -->
<?php 

require_once'../footer.php';
?>
<!-- /End Footer Area -->



