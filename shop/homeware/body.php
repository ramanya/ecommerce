	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
	
	
	<!-- Breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="bread-inner">
						<ul class="bread-list">
							<li><a href="#">shop/horns<i class="ti-arrow-right"></i></a></li>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Breadcrumbs -->


	<!-- Start Product Area -->
	<div class="product-area section">
		<div class="container">
			<div class="nav-main">
				<!-- Tab Nav -->
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item"><a class="nav-link active"  href="../all-products/" >All Products</a></li>
					<li class="nav-item"><a class="nav-link" href="../homeware/" >Homeware</a></li>
					<li class="nav-item"><a class="nav-link"  href="../horn/" >Horns & Buttons</a></li>
					<li class="nav-item"><a class="nav-link"  href="../accessories/" >Accessories</a></li>
					<li class="nav-item"><a class="nav-link"  href="../jewellery/" >Jewellery</a></li>
					<li class="nav-item"><a class="nav-link"  href="../culinary/" >Culinary Accessories</a></li>


				</ul>
				<!--/ End Tab Nav -->
			</div>

			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h2>Homeware products</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="product-info">

						<div class="tab-content" id="myTabContent">
							<!-- Start Single Tab -->
							<div class="tab-pane fade show active" id="man" role="tabpanel">
								<div class="tab-single">
									<div class="row">

										<?php

										$root ="SELECT * FROM product WHERE product_category='Homeware' ORDER BY product_createdon DESC LIMIT 500";
										$res1 = pg_query($conn, $root);

										if ($res1) {
            // output data of each row
											$num = 1;
											while($row1 = pg_fetch_assoc($res1)) {
												(string) $id = $row1['product_code'];
												(string) $product_code = $row1['product_code'];
												(string) $product_photo = $row1['product_photo'];
												(string) $product_description = $row1['product_description'];
												(string) $product_name = $row1['product_name'];
												(string) $product_price = $row1['product_price'];
												(string) $status = $row1['product_availability'];
												(string) $product_createdon = $row1['product_createdon'];
												(string) $product_createdby = $row1['product_createdby'];
												(string) $product_code_link = $row1['product_code_link'];
												if (!empty($product_code)){
													
													?>

													<div class="col-xl-3 col-lg-4 col-md-4 col-12">
														<div class="single-product">
															<div class="product-img">
																<a data-toggle="modal" data-target="#exampleModal">
																	<img class="default-img" src="../../admin/products/images/<?php echo $product_photo; ?>" alt="#">
																	<img class="hover-img" src="../../admin/products/images/<?php echo $product_photo; ?>" alt="#">
																</a>
																<div class="button-head">
																	<div class="product-action">
																		

																		<?php 	echo ' <a   title="Click to view details"
																		onclick="window.location.href ='."'".'product-detail.php?error='.$product_code_link."';".'" >
																		<i class="fa fa-eye"> </i> details </a>'; ?>
																		
																	</div>
																	<div class="product-action-2">
																		
																		<form action="index.php" method="post">
																			<input type="hidden" name="product-code-link" value="<?php echo $product_code_link ?>">
																			<button title="Add to cart" class="btn" type="submit" name="cart">Add to cart</button>
																			
																		</form>
																	</div>

																</div>
															</div>

															<div class="product-content">
																<h3><a href="product-details.html"><?php echo $product_name; ?></a></h3>
																<div class="product-price">
																	<span>$ <?php echo $product_price; ?></span>
																</div>
																
															</div>
															<div class="default-social">
																<h4 class="share-now">visit our pages:</h4>
																<ul>
																	<li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
																	<li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
																	<li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>
																	
																</ul>
															</div>
														</div>
													</div>

												<?php }

												
											}
										} 
										?>
									</div>
								</div>
							</div>
							<!--/ End Single Tab -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Product Area -->


	
	
	
	
	
