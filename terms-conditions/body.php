	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
	
	

	
	<!-- Start Blog Single -->
	<section class="blog-single section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h3>Terms and conditions</h3>
					</div>
				</div>
				<div class="col-12">
					<div class="blog-single-main">
						<div class="row">
							<div class="col-12">

								<div class="blog-detail">
									<h2 class="blog-title">Product Description</h2>

									<div class="content">
										<p>We do our best to describe our goods as accurately as possible, and please note that
											many of CKIL items are handmade and therefore very unique. Sizes and colors
											may vary slightly from the pictures on our website. Every effort is made to ensure
										that the details we give of our products is correct.</p>

										<h2 class="blog-title">Placing an order</h2>

										<p>Placing an order could not be easier. Simply browse our store and add any items
											you wish to buy to your shopping cart. When you have finished your selection,
											simply click ‘Checkout’ and you will be asked for the details we need to complete
											the order. If you make a mistake in your order and are unable to amend it in your
											shopping cart, please contact us and we will amend it for you.  When you place an
											order you will receive an automatic acknowledgement of receipt.  We will send
										acceptance of your order as soon as possible and confirm the details.</p>
										<p>All orders
											remain subject to both availability and the occurrence of events beyond our
											reasonable control, in which case we reserve the right to cancel your order without
											liability. In such unlikely circumstances we will inform you immediately.  All
											prices are shown in $ but do not include post and packaging charges. The total cost
											of each order will be shown before you agree to make a payment. Every effort is
											made to ensure that prices shown are correct. In the unlikely event of an error
										being made, we reserve the right to cancel the order at any time prior to delivery.</p>
										<h2 class="blog-title">Over seas orders</h2>
										
										<p>We can deliver to most overseas countries. Postage will be charged at actual cost,
											please contact us for more details. Customers will also be responsible for any
										duties charged on receipt in their Country.</p>
										<h2 class="blog-title">Payment</h2>
										
										<p>We use Shopify Payments and PayPal to process your payments. These both allow
											payment with all major Credit cards or Debit cards. Card information is always
											encrypted and not stored on our system. However, if you do not wish to pay online,
											simply contact Hannah  on +256772657985 or …… and we will be happy to make

											arrangements for direct bank payments. You will receive a confirmation e-mail
										once your payment has been received.</p>
										<h2 class="blog-title">Late or lost deliveries</h2>
										
										<p>If you experience a problem with your delivery please contact us and we will do
										our best to help you. We cannot be held responsible for late or lost parcels.</p>
										<h2 class="blog-title">Returns Policy</h2>

										<p>If for any reason you are not delighted with your purchase please let us know
										within 7 days of receipt and we will be happy to arrange an exchange or refund.</p>
										<p>We ask that you return any item in its original packaging. A refund is only possible
											if the item/s is/are returned unused and in its original condition.  Purchasers are
											required to pay the cost of returning items, unless they are found to be faulty.
										 Please obtain proof of posting as we cannot accept responsibility for lost parcels.</p>
										<h2 class="blog-title">Endemnity</h2>
										
										<p>You agree fully to indemnify, defend and hold us, CKIL and our officers,
											directors, employees, agents and suppliers, harmless immediately on demand, from
											and against all claims, liability, damages, losses, costs and expenses, including
											reasonable legal fees, arising out of any breach of these terms and conditions by
											you or any other liabilities arising out of your use of this website, or the use by any
											other person accessing the website using your shopping account and/or your
										personal information.</p>
										<h2 class="blog-title">Customer Services</h2>
										
										<p>If you have any queries regarding any of the above please don’t hesitate to contact
										us </p>
									</div>
								</div>
								
							</div>




						</div>
					</div>
				</div>



			</div>
		</div>
	</section>
	<!--/ End Blog Single -->
	
	
	
	
	
