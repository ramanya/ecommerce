<?php 
session_start();
require_once'../admin/library/config.php';
$GLOBALS['error'] = "";
$phone="";
   # Get IP 
$ip = ip_add();
  // Sign_up processing
if(isset($_POST['sign_up'])){
    $code = randomString();
    $app_login_surname_name = test_input($_POST['app_login_surname_name']);
    $app_login_other_names = test_input($_POST['app_login_other_names']);
    $app_login_email = test_input($_POST['app_login_email']);
    $app_login_phone = test_input($_POST['app_login_phone']);
    $pass_code = sha1($code.date('d-m-Y'));
    $cfm_pass_code = $pass_code;
    $link = sha1($code.date('Y-d-m'));
    $ip = ip_add();
    $user = $app_login_surname_name." ".$app_login_other_names;

    if (!empty($app_login_surname_name) && !empty($app_login_email)){
        $sql = "SELECT * FROM app_login 
        WHERE (app_login_email = '$app_login_email') OR (app_login_phone = '$app_login_phone')";
        $result = pg_query($conn, $sql);
        if (pg_num_rows($result) > 0) {
            // output data of each row
            while($row = pg_fetch_assoc($result)) {
                #Alert
             $GLOBALS['error'] = "Invalid! Email address or phone number already used.";
             $GLOBALS['errors'] = "Applicant using email or phone that has already been used.";
             $msg = $errors;
             $type = "-- warning --";
             $user = $app_login_surname_name." ".$app_login_other_names;
             syslogs($conn,$ip,$errors,$user,$type);
         }
     }else{
        $test_pass = sha1($app_login_phone);
        $sql_acc = "INSERT INTO app_login(app_login_code,app_login_surname_name,app_login_other_names,
            app_login_email,app_login_phone,app_login_ip,app_login_createdby,
            app_login_passcode,app_login_cfmpasscode,app_login_code_link)
        VALUES ('$code','$app_login_surname_name','$app_login_other_names',
            '$app_login_email','$app_login_phone','$ip','$user','$test_pass','$test_pass','$link')";
        $result = pg_query($conn, $sql_acc);                
        $GLOBALS['error'] = "Account Created! Check your email to get password.";
        $GLOBALS['errors'] = "Applicant's account ".$user." has been created using online portal";
        $msg = $errors;
        $type = "-- normal --";
        syslogs($conn,$ip,$msg,$user,$type);
        $lnk = "https://ckiluganda.com/";
        $sub = "Password Token: ";
        $to = $app_login_email;
        $phone = $app_login_phone;
        $new_pwd = $phone;
        $msg = "Your password is : ". $new_pwd ."\n The link is ".$lnk;
        $msg = wordwrap($msg, 70, "\r\n");
            //Sending email to the new applicant added
        $from = 'noreply@gemcs.biz';
        send_email($conn,$sub,$msg,$to,$from);
    }
}else{
    $GLOBALS['error'] = "Invalid! Please fill all the required fields on the form.";
    $GLOBALS['errors'] = "Invalid! Please fill all the required fields on the form.";
    $msg = $errors;
    $type = "-- warning --";
    $user = "anonymous";
    syslogs($conn,$ip,$msg,$user,$type);
}
}
   # Signup ends here

require_once'../include/header.php';

include('body.php');

require_once'../include/footer.php';

?>