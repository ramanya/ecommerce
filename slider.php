   <!DOCTYPE html>
   <html>
   <head>
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <title></title>
       <style>
        @media only screen and (max-width: 600px) {
            .carousel-caption p{
                display:none;
            }

        }
    </style>
   </head>
   <body>
   
  
    <!-- Preloader -->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!-- End Preloader -->

<!-- Slider Area -->
<section class="hero-slider">
    <!-- Single Slider -->
    <div class="container my-4">

        <!--Carousel Wrapper-->
        <div id="demo" class="carousel slide carousel-fade z-depth-1-half" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>  
            </ul>

            <!-- The slides -->               
            <div class="carousel-inner">

             <?php 
             require_once'admin/library/config.php';
             $root = "SELECT * FROM slider ORDER BY slider_createdon DESC LIMIT 3";
             $result3 = pg_query($conn, $root);
             $hasAddedActive = false;
             while ($row = pg_fetch_array($result3)) {      
                $id =($row['id']);                      
                $image =($row['slider_name']);
                $description = ($row['slider_description']);
                ?>

                <?php 
                $divClass = 'carousel-item bnw-filter';
                $divClass .= $hasAddedActive ? '' : ' active';
                $hasAddedActive = true;
                echo('<div class="'.$divClass.'">'); 
                ?>
                <?php 
        // echo '<a href="artykul.php?id='.$id.'">'; 
                echo '<img src="admin/slider/images/'.$image.'" class="img-fluid">';
                echo '</a>'; 
                ?>
                <?php echo('</div>'); ?>    
                <div class="mask rgba-black-light"></div>

            <?php } ?>            
        </div>
        <div class="carousel-caption">
            <p class="h6-responsive" style="font-family: Maiandra GD;font-size:20px; font-style: italic; color: #393433 " ><?php echo $description; ?></p>
         
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" role="button"  data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#demo" role="button"  data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>


        </a>
    </div>
    <!--/.Carousel Wrapper-->
</div>
<!--/ End Single Slider -->
</section>
  <!--/ End Slider Area -->
   </body>
   </html>