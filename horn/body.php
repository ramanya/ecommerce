  <!-- Preloader -->
  <div class="preloader">
    <div class="preloader-inner">
      <div class="preloader-icon">
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- End Preloader -->

  
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bread-inner">
            <ul class="bread-list">
              <li><a href="#">Horn<i class="ti-arrow-right"></i></a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Breadcrumbs -->

  <!-- Slider Area -->
  <section class="section free-version-banner">
    <div class="container">
      <div class="section18">
        <div class="row">
         <div class="col-12">
          <div class="section-title">
           <a href="../shop/">
             <h2>Horn & Bone</h2>
           </a>
         </div>
       </div>
       <div class="col-md-8 col-lg-8 wow fadeInUp">
        <div class="textcont">
          
          <p>Ankole Cow horn is one of the most sustainable and ethically sourced products that we carry. Our
            artisans collect the discarded Ankole horns and bones, then take them to our modest workshops/
            training stations where they heat the horns to transform them into beautiful bowls, servers, and
            jewelry components. Each horn is unique in color and texture, so you know that you are getting a
          one-of-a kind design where no two pieces are alike.</p>
        </div> 

      </div>
      <div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s" >
        <div class="section-18-img">
          <a href="../shop/">
            <img src="../images/horn-born.jpg"  class="img-responsive" alt=""/>
          </a>
          <div class="default-social">
            <h4 class="share-now">visit our pages:</h4>
            <ul>
              <li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
              <li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>
              
            </ul>
          </div>
        </div>
        
      </div>
    </div>

  </div>  
</div>
</section>
<!--/ End Slider Area -->

