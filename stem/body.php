  <!-- Preloader -->
  <div class="preloader">
    <div class="preloader-inner">
      <div class="preloader-icon">
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- End Preloader -->

 
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bread-inner">
            <ul class="bread-list">
              <li><a href="#">Stem<i class="ti-arrow-right"></i></a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Breadcrumbs -->


<!-- Slider Area -->
<section class="section free-version-banner">
  <div class="container">
    <div class="section18">
      <div class="row">
  <div class="col-12">
          <div class="section-title">
           <h2>STEM Education</h2>
         </div>
       </div>
        <div class="col-md-8 col-lg-8 wow fadeInUp">
          <div class="textcont">
      
            <p>Uganda largely dominated with unemployed yet educated youth, we discovered that the theoretical
              education plays a role in increasing the unemployment numbers high as the demand is too small
            compared to supply of skilled labor.</p>
            <br>
            <p>We partnered with African School of Innovations, science and technology to provide affordable
              weekend lessons to children from 8years into the field of STEM. STEM is an acronym for science,
              technology, engineering, and math. These four fields share an emphasis on innovation,
              problem-solving, and critical thinking. And together they make up a popular and fast-growing
            industry. Most STEM workers use computers and other technology in their day-to-day jobs.</p>

          </div>  
        </div>
        <div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s">
          <div class="section-18-img">
            <img src="../images/stem.jpg"  class="img-responsive" alt=""/>
             <div class="default-social">
          <h4 class="share-now">visit our pages:</h4>
          <ul>
            <li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
            <li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>
            
          </ul>
        </div>
          </div>
        </div>
      </div>

    </div>  
  </div>
</section>
  <!--/ End Slider Area -->