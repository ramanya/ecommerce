  <!-- Preloader -->
  <div class="preloader">
    <div class="preloader-inner">
      <div class="preloader-icon">
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- End Preloader -->


 
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bread-inner">
            <ul class="bread-list">
              <li><a href="#">Social Media<i class="ti-arrow-right"></i></a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Breadcrumbs -->

<!-- Slider Area -->
<section class="section free-version-banner">
  <div class="container">
    <div class="section18">
      <div class="row">
  <div class="col-12">
          <div class="section-title">
           <h2>Social Media marketing</h2>
         </div>
       </div>
        <div class="col-md-8 col-lg-8 wow fadeInUp">
          <div class="textcont">
          
            <p>Being a third world country, Uganda still lags behind in the field Social media marketing which is the
              use of social media platforms and websites to promote a product or service. We tailor our modules
            depending on the client’s needs.</p>
          

          </div>  
        </div>
        <div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s">
          <div class="section-18-img">
            <img src="../images/skill.jfif"  class="img-responsive" alt=""/>
             <div class="default-social">
          <h4 class="share-now">visit our pages:</h4>
          <ul>
            <li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
            <li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>
            
          </ul>
        </div>
          </div>
        </div>
      </div>

    </div>  
  </div>
</section>
  <!--/ End Slider Area -->