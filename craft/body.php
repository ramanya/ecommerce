  <!-- Preloader -->
  <div class="preloader">
    <div class="preloader-inner">
      <div class="preloader-icon">
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- End Preloader -->

 
  <!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bread-inner">
            <ul class="bread-list">
              <li><a href="#">Craft<i class="ti-arrow-right"></i></a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Breadcrumbs -->

  <!-- Slider Area -->
  <section class="section free-version-banner">
    <div class="container">
      <div class="section18">
        <div class="row">
         <div class="col-12">
          <div class="section-title">
           <a href="../shop/">
             <h2>Art& Craft | Back cloth</h2>
           </a>
         </div>
       </div>
       <div class="col-md-8 col-lg-8 wow fadeInUp">
        <div class="textcont">
          <p>We give young girls, women and youth access to affordable training opportunities to help in
            paving way towards economic justice and independence. There your order helps in making
            an impact and more power to train and reach more as they all strive to have a decent
          income.</p>



        </div>  
      </div>
      <div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s">
        <div class="section-18-img">
         <a href="../shop/">
          <img src="../images/craft.jpg"  class="img-responsive" alt=""/>
        </a>
        <div class="default-social">
          <h4 class="share-now">visit our pages:</h4>
          <ul>
            <li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
            <li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>
            
          </ul>
        </div>
      </div>
    </div>
  </div>

</div>  
</div>
</section>
<!--/ End Slider Area -->

