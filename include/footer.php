	<!-- Start Footer Area -->
	<footer class="footer">
		<!-- Footer Top -->
		<div class="footer-top section">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Quick links</h4>
							<ul>
								<li><a href="../terms-conditions/">Terms and Conditions</a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
						<!-- Single Widget -->
						<div class="single-footer about">

						
							<p class="call">Monday - Friday : 8AM - 6PM</p>
							<p class="call">saturday : 9AM - 4PM</p>
							<p class="call"><span><a href="../admin/">admin portal</a></span></p>
		
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
								<h4>Partners</h4>
							<ul>
								<li><a >Tony Elomelu Foundation</a></li>
								<li><a >Vibrant Communities Initiative</a></li>
								<li><a >Love Binti International</a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
						<!-- Single Widget -->
						<div class="single-footer about">

		<p class="call"><span><a href="https://webmail.ckiluganda.com/">web mail</a></span></p>
						</div>
						<!-- End Single Widget -->
					</div>

					<div class="col-lg-3 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Useful links</h4>
							<ul>
								<li><a href="../">Home</a></li>
								<li><a href="../impact/">Impact</a></li>
								<li><a href="../shop/">shop</a></li>
								<li><a href="../team/">Team</a></li>
								<li><a href="../contact/">contact us</a></li>
								
								
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer social">
							<h4>Get In Tuch</h4>
							<!-- Single Widget -->
							<div class="contact">
								<ul>
									<li>P.O.BOX 105323 Kampala</li>
									<li>Uganda</li>
									<li>info@ckiluganda.com</li>
									<li>+256 (772) 657-985</li>
								</ul>
							</div>
							<!-- End Single Widget -->
							<ul>
						<li><a href="https://m.facebook.com/CKenyaLtd/"><i class="ti-facebook"></i></a></li>
								<li><a href="https://twitter.com/ckiluganda"><i class="ti-twitter"></i></a></li>
<!-- 								<li><a href="#"><i class="ti-flickr"></i></a></li>
 -->								<li><a href="https://www.instagram.com/ckiluganda/"><i class="ti-instagram"></i></a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row">
						<div class="col-lg-6 col-12">
							<div class="left">
								<p>Powered By <a href="http://itexug.com/" target="_blank">ITEX COMPUTER SOLUTIONS (U) LTD</a>  -  All Rights Reserved.</p>
							</div>
						</div>
						<div class="col-lg-6 col-12">
							<div class="right">
								<img src="../images/payments.png" alt="#">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /End Footer Area -->


	<!-- Jquery -->
	<script src="../js/jquery.min.js"></script>
	<script src="../js/jquery-migrate-3.0.0.js"></script>
	<script src="../js/jquery-ui.min.js"></script>
	<!-- Popper JS -->
	<script src="../js/popper.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="../js/bootstrap.min.js"></script>
	<!-- Color JS -->
	<script src="../js/colors.js"></script>
	<!-- Slicknav JS -->
	<script src="../js/slicknav.min.js"></script>
	<!-- Owl Carousel JS -->
	<script src="../js/owl-carousel.js"></script>
	<!-- Magnific Popup JS -->
	<script src="../js/magnific-popup.js"></script>
	<!-- Waypoints JS -->
	<script src="../js/waypoints.min.js"></script>
	<!-- Countdown JS -->
	<script src="../js/finalcountdown.min.js"></script>
	<!-- Nice Select JS -->
	<script src="../js/nicesellect.js"></script>
	<!-- Flex Slider JS -->
	<script src="../js/flex-slider.js"></script>
	<!-- ScrollUp JS -->
	<script src="../js/scrollup.js"></script>
	<!-- Onepage Nav JS -->
	<script src="../js/onepage-nav.min.js"></script>
	<!-- Easing JS -->
	<script src="../js/easing.js"></script>
	<!-- Active JS -->
	<script src="../js/active.js"></script>
		<!-- Google Map JS -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnhgNBg6jrSuqhTeKKEFDWI0_5fZLx0vM"></script>	
	<script src="../js/gmap.min.js"></script>
	<script src="../js/map-script.js"></script>
	<!-- Ytplayer JS -->
	<script src="../js/nicesellect.js"></script>
	<!-- Jquery Counterup JS -->
	<script src="../js/jquery-counterup.min.js"></script>
	<!-- Fancybox JS -->
	<script src="js/facnybox.min.js"></script>
	
</body>
</html>