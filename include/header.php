
<!DOCTYPE html>
<html lang="zxx">
<head>
	<!-- Meta Tag -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Christ Kenya Investments Ltd empowers a girl child with 21st century skills.CKIL has sales arm where value addition on cow horn & detergent production." />
	<meta name="robots" content="noindex,nofollow" />
	<meta name="googlebot" content="notranslate" />

	<!-- Title Tag  -->
	<title>Christ Kenya Investments </title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="../images/logo.jpg">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	
	<!-- StyleSheet -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="../css/bootstrap.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="../css/magnific-popup.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="../css/font-awesome.css">
	<!-- Fancybox -->
	<link rel="stylesheet" href="../css/jquery.fancybox.min.css">
	<!-- Themify Icons -->
	<link rel="stylesheet" href="../css/themify-icons.css">
	<!-- Nice Select CSS -->
	<link rel="stylesheet" href="../css/niceselect.css">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="../css/animate.css">
	<!-- Flex Slider CSS -->
	<link rel="stylesheet" href="../css/flex-slider.min.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="../css/owl-carousel.css">
	<!-- Slicknav -->
	<link rel="stylesheet" href="../css/slicknav.min.css">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../style.css">
	<link rel="stylesheet" href="../css/responsive.css">
	<style>
		@media only screen and (max-width: 600px) {
			.logo {
				display:none;	
			}
		}
	</style>

	
	
</head>
<body class="js">

	<!-- Header -->
	<header class="header shop">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-12">
						<!-- Top Left -->
						<div class="top-left">
							<ul class="list-main">
								<!-- 	<li><i class="ti-headphone-alt"></i> +060 (800) 801-582</li> -->
								<li><i class="ti-email"></i>info@ckiluganda.com</li>
							</ul>
						</div>
						<!--/ End Top Left -->
					</div>
					<div class="col-lg-8 col-md-12 col-12">
						<!-- Top Right -->
						<div class="right-content">
							<ul class="list-main">
								<li><i class="ti-location-pin"></i> Kampala </li>
								<li><i class="ti-headphone"></i> <a href="#">+256 (772) 657-985</a></li>
								<li><div class="sinlge-bar shopping">
									<a href="../register/"  class="single-icon"><i class="ti-user"></i> <span class="total-count">Register</span></a>
									
								</div></li>
								<li>	<div class="sinlge-bar shopping">
									<a href="../login/"  class="single-icon"><i class="ti-power-off"></i> <span class="total-count">Login</span></a>
									
								</div>
							</li>
							<li><div class="sinlge-bar shopping">
								<a href="#" class="single-icon"><i class="ti-bag"></i> <span class="total-count">Cart <?php 
								require_once'../admin/library/config.php';
								if (empty($_SESSION['user'])) {


       							 //some thing is okay
								}else{
									$user =  $_SESSION['user'];

									$query = "select * from cart where product_createdby='$user'";
									$result=pg_query($conn,$query);
									$rows= pg_num_rows($result);
									echo "$rows";
								}
							?></span></a>
							<!-- Shopping Item -->
							<div class="shopping-item">
								<div class="dropdown-cart-header">
									<span><?php 
									require_once'../admin/library/config.php';
									if (empty($_SESSION['user'])) {


       								 //some thing is okay
									}else{
										$user =  $_SESSION['user'];

										$query = "select * from cart where product_createdby='$user'";
										$result=pg_query($conn,$query);
										$rows= pg_num_rows($result);
										echo "$rows";
									}
								?> Items</span>
								<a href="../cart/">View Cart</a>
							</div>
							<ul class="shopping-list">
								<?php

								require_once'../admin/library/config.php';
								if (empty($_SESSION['user'])) {


        //some thing is not okay
								}else{
									$user =  $_SESSION['user'];
									$final_amount =  $_SESSION['final_amount'] ;

									$root ="SELECT * FROM cart WHERE product_createdby ='$user'";
									$res1 = pg_query($conn, $root);

									if ($res1) {
            					// output data of each row
										$num = 1;
										while($row1 = pg_fetch_assoc($res1)) {
											(string) $id = $row1['product_code'];
											(string) $product_code = $row1['product_code'];
											(string) $product_photo = $row1['product_photo'];
											(string) $product_description = $row1['product_description'];
											(string) $product_name = $row1['product_name'];
											(string) $product_price = $row1['product_price'];
											(string) $quantity = $row1['product_quantity'];
											(string) $amount = $row1['product_amount'];
											(string) $product_createdon = $row1['product_createdon'];
											(string) $product_createdby = $row1['product_createdby'];
											(string) $product_code_link = $row1['product_code_link'];
											if (!empty($product_code)){

												?>

												<li>
													
													<a class="cart-img" href="#"><img src="../admin/products/images/<?php echo $product_photo; ?>" alt="#"></a>
													<h4><a href="#"><?php echo $product_name; ?></a></h4>
													<p class="quantity"><span class="amount">$ <?php echo $product_price; ?> </span></p>
												</li>
												
											<?php }
										}
									}
								}
								?>
							</ul>
							<div class="bottom">
								<div class="total">
									<span>Total</span>
									<span class="total-amount">$ <?php 	if (empty($_SESSION['user'])) {


        //some thing is not okay
									}else{ echo $final_amount; }?> </span>
								</div>
								<a href="#" class="btn animate">Order Now</a>
							</div>
						</div>
						<!--/ End Shopping Item -->
					</div></li>


				</ul>
			</div>
			<!-- End Top Right -->
		</div>
	</div>
</div>
</div>
<!-- End Topbar -->
<div class="middle-inner">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-12">
				<!-- Logo -->
				<div class="logo"  style="margin-top: -15px;">
					<a href="index.php"><img src="../images/logo1.png" alt="logo"></a>
				</div>
				<!--/ End Logo -->

				<div class="mobile-nav"></div>
			</div>
			<div class="col-lg-8 col-md-7 col-12">
				<div class="search-bar-top" >
					<h2 style="color: #88042c; font-family: Maiandra GD;">Christ Kenya Investments 
					</h2>

				</div>
			</div>

		</div>
	</div>
</div>
<!-- Header Inner -->
<div class="header-inner">
	<div class="container">
		<div class="cat-nav-head">
			<div class="row">

				<div class="col-lg-9 col-12">
					<div class="menu-area">
						<!-- Main Menu -->
						<nav class="navbar navbar-expand-lg">
							<div class="navbar-collapse">	
								<div class="nav-inner">	
									<ul class="nav main-menu menu navbar-nav">
										<li class="active"><a href="../">Home</a></li>
										<li><a href="../shop/">Shop</a></li>
										<li><a href="../impact/">Impact</a></li>
										<li><a href="#">Skilling<i class="ti-angle-down"></i></a>
											<ul class="dropdown">
												<li><a href="../horn/">Horn & Bone</a></li>
												<li><a href="../craft/">Art& Craft | Back cloth</a></li>
												<li><a href="../media/">Social Media marketing</a></li>
												<li><a href="../stem/">STEM Education</a></li>
											</ul>
										</li>

										<li><a href="../team/">Team</a></li>

										<li><a href="../contact/">Contact Us</a></li>
<!-- 													<li><a href="blog.php">Blog</a></li>
-->
</ul>
</div>
</div>
</nav>
<!--/ End Main Menu -->	
</div>
</div>
</div>
</div>
</div>
</div>
<!--/ End Header Inner -->
</header>








