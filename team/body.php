	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
	
	
	<!-- Breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="bread-inner">
						<ul class="bread-list">
							<li><a href="#">Team<i class="ti-arrow-right"></i></a></li>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Breadcrumbs -->

<!-- Start Shop Blog  -->
	<section class="shop-blog section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h2>Team</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
						<img src="../images/img3a.jpg" alt="#">
						<div class="content">
							<p  class="title">Hannah Mellon Kenyangi</p>
							<p class="more-btn">Managing Director.</p>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
						<img src="../images/img1.jpg" alt="#">
						<div class="content">
							<p  class="title">Ms Kabasonga Sandra</p>
							<p class="more-btn">Design and creative Manager.</p>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
						<img src="../images/img2.jpg" alt="#">
						<div class="content">
							<p  class="title">Odeth Kakaire.</p>
							<p class="more-btn">Fundraising & Grants mobilization Manager.</p>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>
				
			</div>

			<div class="row">
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
						<img src="../images/img4.jpg" alt="#">
						<div class="content">
							<p  class="title">Alice Ndyomugyenyi</p>
							<p class="more-btn">Training Response Manager.</p>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
						<img src="../images/dr.jpg" alt="#">
						<div class="content">
							<p  class="title">Dr Hillary Emmanuel Musoke Kisanja</p>
							<p class="more-btn">Brand Ambassador.</p>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
						<img src="../images/img6.jpg" alt="#">
						<div class="content">
							<p  class="title">Nanyonjo Allen </p>
							<p class="more-btn">STEM educator and Ambassador.</p>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>
				
			</div>
		</div>
	</section>
	<!-- End Shop Blog  -->
	
	
	
	
	
