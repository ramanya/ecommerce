	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
	
	
	<!-- Breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="bread-inner">
						<ul class="bread-list">
							<li><a href="#">Impact<i class="ti-arrow-right"></i></a></li>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Breadcrumbs -->
	<section id="about-us">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="about-content" style="text-align: justify;text-justify: inter-word;">
						<h2>Operation</h2>
						<p>We have a HYBRID model of profit and social enterprise. We provide debt free
							access to skills and capital for the entrepreneurs, under the poverty line, to start
							their own businesses through joint contribution. 
							While on the program, the likeminded entrepreneurs work together creating our
							Collection of banana fibre, baskets, bags and Ankole cow Horn wares ranging from
							décor, kitchen and bathroom, jewelry among others, sold locally, Nigeria and the
							UK. The profits are reinvested and used to employ more entrepreneurs and
						facilitate volunteers.</p>
					</div>
					<div class="about-content" style="text-align: justify;text-justify: inter-word;">
						<h2>support our impact in Ggaba</h2>
						<p>Our products are handcrafted by female entrepreneurs in Ggaba, Namayuba,
							Rwampara counties in Uganda who were previously living below the poverty
							line. Through short and midterm trainings and mentorship creating our
							products, the ladies and young men earn a living wage to support themselves
						and their families.</p>
					</div>
					<div  class="form-group button">
						<a href='https://www.thezenalaunchpad.com/donate'>
							<button  class="btn">Donate Now </button>
						</a>

					</div>
				</div>
				<div class="col-md-6">
					<div class="about-img" >
						<img   src="../images/imp.jpg" alt="#">
<!-- 						<img   src="../images/cup2.jpg" alt="#">
 -->

						<div class="default-social">
							<h4 class="share-now">visit our pages:</h4>
							<ul>
								<li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
								<li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
								<li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<!-- Start Shop Blog  -->
	<section class="shop-blog section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h2>Impact</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
						<img src="../images/try1.jpeg" alt="#">
						<div class="default-social">
							<h4 class="share-now">visit our pages:</h4>
							<ul>
								<li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
								<li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
								<li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>

							</ul>
						</div>
						<div class="content" style="text-align: justify;text-justify: inter-word;">
							
							<a >At CKil it’s our custom to celebrate big dream to realize a radical social mission
								every purchase supporting female entrepreneurs on a radical social mission. 
								Empowering women is essential to the health and social development of families,
								communities and countries. When women are living safe, fulfilled and productive
								lives, they can reach their full potential. Contributing their skills to the workforce
								and can raise happier and healthier children Therefore investing in young women
								entrepreneurs has the power to change the global stories of gender inequality, and
								extreme poverty. To note, we are male inclusive especially the youth in bridging the
								unemployment gap.
								Creating a new dimension CKIL decided that 20% of your purchase should be
							reinvested into this cause.</a>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>
				<div class="col-lg-6 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
						<img  src="../images/try2.jpeg" alt="#">
						<div class="default-social">
							<h4 class="share-now">visit our pages:</h4>
							<ul>
								<li><a class="facebook" href="https://m.facebook.com/CKenyaLtd/"><i class="fa fa-facebook"></i></a></li>
								<li><a class="twitter" href="https://twitter.com/ckiluganda"><i class="fa fa-twitter"></i></a></li>
								<li><a class="instagram" href="https://www.instagram.com/ckiluganda/"><i class="fa fa-instagram"></i></a></li>

							</ul>
						</div>
						<div class="content" style="text-align: justify;text-justify: inter-word;">

							<a >At CKIL, we agree that investing in a young entrepreneurs especially female
								has a great impact on extreme poverty and gender inequality. Female
								entrepreneurs living in semi urban and rural poverty lack access to business
								opportunity because of little or no education and capital.
								We are changing the narrative by using the Ankole Horns that are waste
								products from the meat industry in the central and west Uganda.
								We choose to work with the Ankole Cow Horn in order to preserve and honor
								our Heritage. The Long Horned Ankole Cow,that has long and distinctive horn
								commonly known as cattle of the Kings. These magnificent horns have great
								length and circumferences with awesome colors making them exceptional and
							valued décor artifacts</a>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>

			</div>
		</div>
	</section>
	<!-- End Shop Blog  -->


<!-- 

	<section class="section free-version-banner"  >
		<div class="container"  >
			<div class="row align-items-center">
				<div class="col-md-10 offset-md-2 col-xs-12">
					<div class="section-title mb-60">
						<h2  >support our impact in Ggaba</h2>
						
						<p  >Our products are handcrafted by female entrepreneurs in Ggaba, Namayuba,
							Rwampara counties in Uganda who were previously living below the poverty
							line. Through short and midterm trainings and mentorship creating our
							products, the ladies and young men earn a living wage to support themselves
						and their families.</p>

						<div  class="form-group button">
							<a href='https://www.thezenalaunchpad.com/donate'>
								<button  class="btn">Donate Now </button>
							</a>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->

	<!-- Start Shop Services Area  -->
	<section class="shop-services section home">
		<div class="container">
			<div class="section-title">
				<h2> Business Partners</h2>
				<p >We are proudly associated with Tony Elomelu Foundation as Alumni therefore
					share business knowledge from the best. CKIL program members receive weekly
				entrepreneurship training, literacy tutoring and 1-1 leadership mentoring.</p>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Service -->
					<div class="single-service">
						<img height="10px" src="../images/tonny.png" >
						<h4>Tony Elomelu Foundation</h4>
						<!-- 	<p>Orders over $100</p> -->
					</div>
					<!-- End Single Service -->
				</div>
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Service -->
					<div class="single-service">
						<img src="../images/vciug_logo.png">
						<h4>Vibrant Communities Initiative</h4>
						<!-- 	<p>Within 30 days returns</p> -->
					</div>
					<!-- End Single Service -->
				</div>
				<div class="col-lg-4 col-md-6 col-12">
					<!-- Start Single Service -->
					<div class="single-service">
						<img src="../images/logo-black.png">
						<h4>Love Binti International</h4>
						<!-- 	<p>100% secure payment</p> -->
					</div>
					<!-- End Single Service -->
				</div>
				

			</div>
		</div>
	</section>
	<!-- End Shop Services -->


