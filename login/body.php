

<!-- Preloader -->
<div class="preloader">
	<div class="preloader-inner">
		<div class="preloader-icon">
			<span></span>
			<span></span>
		</div>
	</div>
</div>
<!-- End Preloader -->





<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="bread-inner">
					<ul class="bread-list">
						<li><a href="#">Login<i class="ti-arrow-right"></i></a></li>
						
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Breadcrumbs -->

<!-- Start Contact -->
<section id="contact-us" class="contact-us section">
	<div class="container">
		
		<?php
//For alert messages
		if (strlen($error) > 0) {
			echo '<div class="alert alert-success" role="alert" data-auto-dismiss="3000">
			' . $error . '
			</div>';
		}
		?>
		<div class="contact-head">
			<div class="row">
				<div class="col-lg-8 col-12">
					<div class="form-main">
						<div class="title">
							<h4>Login</h4>
							<h3>Enter login details here</h3>
						</div>
						<form class="form" method="post" action="#">
							<div class="row">
								<div class="col-lg-6 col-12">
									<div class="form-group">
										<label>User Name<span>*</span></label>
										<input  name="email" type="email" placeholder="" required>
									</div>
								</div>
								
								
								<div class="col-lg-6 col-12">
									<div class="form-group">
										<label>Your Password<span>*</span></label>
										<input name="password" type="password" placeholder="" required> 
									</div>	
								</div>
								
								<div class="col-12">
									<div class="form-group button">
										<button type="submit" name="login" class="btn">Login</button>
											<a href="../register/"><button type="button" class="btn">Register</button></a>
									</div>
									
								</div>
							</div>
						</form>
					</div>
				</div>
					<!-- 	<div class="col-lg-4 col-12">
							<div class="single-head">
								<div class="single-info">
									<i class="fa fa-phone"></i>
									<h4 class="title">Call us Now:</h4>
									<ul>
										<li>+123 456-789-1120</li>
										<li>+522 672-452-1120</li>
									</ul>
								</div>
								<div class="single-info">
									<i class="fa fa-envelope-open"></i>
									<h4 class="title">Email:</h4>
									<ul>
										<li><a href="mailto:info@yourwebsite.com">info@yourwebsite.com</a></li>
										<li><a href="mailto:info@yourwebsite.com">support@yourwebsite.com</a></li>
									</ul>
								</div>
								<div class="single-info">
									<i class="fa fa-location-arrow"></i>
									<h4 class="title">Our Address:</h4>
									<ul>
										<li>KA-62/1, Travel Agency, 45 Grand Central Terminal, New York.</li>
									</ul>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</section>
		<!--/ End Contact -->
		

		


		
