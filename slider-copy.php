<!-- Slider Area -->
	<section class="hero-slider">
		<!-- Single Slider -->
		  <div class="container my-4">

    <!--Carousel Wrapper-->
    <div id="carousel-example-2" class="carousel slide carousel-fade z-depth-1-half" data-ride="carousel">
      <!--Indicators-->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-2" data-slide-to="1"></li>
        <li data-target="#carousel-example-2" data-slide-to="2"></li>
      </ol>
      <!--/.Indicators-->
      <!--Slides-->
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <div class="view">
          	  <?php
                    require_once'admin/library/config.php';
                    $root = "SELECT * FROM slider ORDER BY slider_createdon DESC LIMIT 3";
                    $res1 = pg_query($conn, $root);
                    if ($res1) {
                        // output data of each row
                        $num = 1;
                        while($row1 = pg_fetch_assoc($res1)) {
                        (string) $name = $row1['slider_name'];
                        (string) $description = $row1['slider_description'];
                ?>
            <img class="d-block w-100" src="admin/slider/images/<?php echo $name; ?>" alt="First slide">

            <div class="mask rgba-black-light"></div>
          </div>
          <div class="carousel-caption">
            <h3 class="h3-responsive"><?php echo $description; ?></h3>
            <p>CKil</p>
          </div>
           <?php
           $num++;
                        }
                }
                ?>

        </div>
       
       
      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->

  </div>
		<!--/ End Single Slider -->
	</section>
	<!--/ End Slider Area -->